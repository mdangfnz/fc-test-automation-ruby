require_relative '../../../../../components/clients/http_json_services'

class PostStateUpdate < HttpJsonServices

  def initialize
    @token = Config['chain_link_api']['token']
  end

  def send(chain_id, state)
    HtmlReporter.add_step_header("POST STATE UPDATE - #{state}")
    @chain_id = chain_id
    @state = state
    @uri = Config['chain_link_api']['url'] + "/api/transaction/#{@chain_id}/state"

    unless is_valid_state? @state
      HtmlReporter.add_step_failed_no_ss('Invalid State', @state)
      raise Exception.new 'Invalid post update state input'
    end

    payload = JSON.parse(get_payload)
    payload['state'] = @state
    if payload['state'] == 'TRANSACTION_TERMINATED'
      payload['reason'] = 'Smoke Test trade'
    end

    post(__FILE__, payload, 'Y', '1.1')
  end

  def is_valid_state?(state)
    case state.upcase
    when 'TRANSACTION_TERMINATED'
      return true
    when 'TRANSACTION_CONFIRMATION_RECEIVED_BY_DISTRIBUTOR'
      return true
    when 'REJECTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR'
      return true
    when 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR'
      return true
    when 'TRANSACTION_RECEIVED_BY_TA'
      return true
    else
      return false
    end
  end

  def get_payload
    '
    {
        "state": "{{state}}"
    }
    '
  end

  def validate_202
    validate(__FILE__, 202)
    response = JSON.parse(@response.to_s)
    if response['chainId'] == @chain_id
      HtmlReporter.add_step_passed_no_ss('State Updated', @state )
    else
      HtmlReporter.add_step_failed_no_ss('Chain ID Returned', "Expected: #{@chain_id} | Actual #{response['chainId']}")
      raise Exception.new 'Post State Update returned different Chain ID'
    end
  end
end