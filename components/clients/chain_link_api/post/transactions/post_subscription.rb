require_relative '../../../../../components/clients/http_json_services'

class PostSubscription < HttpXmlServices

  def initialize
    @uri = Config['chain_link_api']['url'] + '/api/transaction'
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Post Subscription')
  end

  def send

    if DataPrep.hash.has_key? 'units'
      @payload = get_payload_units
      @payload.gsub!('{{units}}', DataPrep['units'])
    elsif DataPrep.hash.has_key? 'amt'
      @payload = get_payload_amt
      @payload.gsub!('{{amt}}', DataPrep['amt'])
      #TODO: Maybe add this in? Error is catch later at 400
      #
    elsif DataPrep.hash.has_key? 'default'
      @payload = get_payload
    # #   HtmlReporter.add_step_failed_no_ss('Data Setup Error', 'Please check code and set units or amt to post subscription')
    # #   raise Exception.new 'Please set units or amt for Posting a subscription'
    end


    @payload.gsub!('{{cre_dt_tm}}', Time.now.utc.strftime('%Y-%m-%dT%H:%M:%SZ'))

    unless DataPrep.hash.has_key? 'fund_provider'
      @payload.gsub!('{{fund_provider}}', 'SAMUK')
    else
      @payload.gsub!('{{fund_provider}}', DataPrep['fund_provider'])
    end

    unless DataPrep.hash.has_key? 'acct_id'
      @payload.gsub!('{{acct_id}}', 'TC1000771-001')
    else
      @payload.gsub!('{{acct_id}}', DataPrep['acct_id'])
    end

    unless DataPrep.hash.has_key? 'order_ref'
      @payload.gsub!('{{ordr_ref}}', 'API_T_' + StringsUtil.generate_code(5))
    else
      @payload.gsub!('{{ordr_ref}}', DataPrep['order_ref'])
    end

    unless DataPrep.hash.has_key? 'isin'
      @payload.gsub!('{{isin}}', 'GB00B2NLM962')
    else
      @payload.gsub!('{{isin}}', DataPrep['isin'])
    end

    #TODO: add XML parser
    # xml_parser = Nori.new
    # xml_hash = xml_parser.parse(@payload)
    # puts 'deleteme'
    post(__FILE__, @payload)

  end

  def validate_400
    validate(__FILE__, 400)
    response = JSON.parse(@response.to_s)
    if response['message'].nil?
      HtmlReporter.add_step_failed_no_ss('No error message in response', 'Failed')
      raise Exception.new 'No error message in response'
    else
      HtmlReporter.add_step_passed_no_ss('Error Message', response['message'])
    end
  end

  def validate_202
    validate(__FILE__, 202)
    response = JSON.parse(@response.to_s)
    if response['chainId'].nil?
      HtmlReporter.add_step_failed_no_ss('No Chain ID created', 'Fail')
      raise Exception.new 'No Chain ID created'
    end
    HtmlReporter.add_step_passed_no_ss('Chain ID', response['chainId'])
    @chain_id = response['chainId']
  end

  def get_chain_id
    @chain_id
  end

  def get_payload
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.010.001.03">
	<SbcptOrdrV03>
		<MsgId>
			<Id>13415153155</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>{{acct_id}}</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Cofunds</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>{{isin}}</ISIN>
					</Id>
					<Nm>TC Share Centre Multi Manager Adventurous Fund</Nm>
				</FinInstrmDtls>
				<UnitsNb>
        			<Unit>1</Unit>
    			</UnitsNb>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
					<Role>FMCO</Role>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</SbcptOrdrV03>
</Document>
    '
  end

  def get_payload_units
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.010.001.03">
	<SbcptOrdrV03>
		<MsgId>
			<Id>13415153155</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>{{acct_id}}</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Cofunds</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>{{isin}}</ISIN>
					</Id>
					<Nm>TC Share Centre Multi Manager Adventurous Fund</Nm>
				</FinInstrmDtls>
				<UnitsNb>
        			<Unit>{{units}}</Unit>
    			</UnitsNb>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
					<Role>FMCO</Role>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</SbcptOrdrV03>
</Document>
    '
  end


  def get_payload_amt
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.010.001.03">
	<SbcptOrdrV03>
		<MsgId>
			<Id>13415153155</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>{{acct_id}}</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Cofunds</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>{{isin}}</ISIN>
					</Id>
					<Nm>TC Share Centre Multi Manager Adventurous Fund</Nm>
				</FinInstrmDtls>
				<GrssAmt Ccy="GBP">{{amt}}</GrssAmt>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
					<Role>FMCO</Role>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</SbcptOrdrV03>
</Document>
    '
  end
end