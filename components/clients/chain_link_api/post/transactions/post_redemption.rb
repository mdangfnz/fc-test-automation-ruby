require_relative '../../../../../components/clients/http_xml_services'


class PostRedemption < HttpXmlServices

  def initialize
    @uri = Config['chain_link_api']['url'] + '/api/transaction'
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Post Redemption')
  end

  def send

    if DataPrep.hash.has_key? 'units'
      payload = get_payload_units
      payload.gsub!('{{units}}', DataPrep['units'])
    elsif DataPrep.hash.has_key? 'amt'
      payload = get_payload_amt
      payload.gsub!('{{amt}}', DataPrep['amt'])
    elsif DataPrep.hash.has_key? 'hldgs_red_rate'
      payload = get_payload_holdings_red_rate
      payload.gsub!('{{hldgs_red_rate}}', DataPrep['hldgs_red_rate'])
    elsif DataPrep.hash.has_key? 'default'
      payload = get_payload
    else
      puts 'Using payload from special method'
    end


    payload.gsub!('{{cre_dt_tm}}', Time.now.utc.strftime('%Y-%m-%dT%H:%M:%SZ'))


    if DataPrep.hash.has_key? 'fund_provider'
      payload.gsub!('{{fund_provider}}', DataPrep['fund_provider'])
    else
      payload.gsub!('{{fund_provider}}', 'MERIANIM')
    end

    if DataPrep.hash.has_key? 'order_ref'
      payload.gsub!('{{ordr_ref}}', DataPrep['order_ref'])
    else
      payload.gsub!('{{ordr_ref}}', 'API_T_' + StringsUtil.generate_code(5))
    end


    if DataPrep.hash.has_key? 'fund_nm'
      payload.gsub!('{{fund_nm}}', DataPrep['fund_nm'])
    else
      payload.gsub!('{{fund_nm}}', 'MERIAN TEST FUND')
    end

    if DataPrep.hash.has_key? 'acct_id'
      payload.gsub!('{{acct_id}}', DataPrep['acct_id'])
    else
      payload.gsub!('{{acct_id}}', 'CU1000001-001')
    end

    if DataPrep.hash.has_key? 'isin'
      payload.gsub!('{{isin}}', DataPrep['isin'])
    else
      payload.gsub!('{{isin}}', 'GB00B1XG8F22')
    end


    post(__FILE__,payload)
  end

  def validate_202
    validate(__FILE__,202)
    response = JSON.parse(@response.to_s)
    if response['chainId'].nil?
      HtmlReporter.add_step_failed_no_ss('No Chain ID created', 'Fail')
      raise Exception.new 'No Chain ID created'
    end
    HtmlReporter.add_step_passed_no_ss('Chain ID', response['chainId'])
    @chain_id = response['chainId']
  end

  def validate_400
    validate(__FILE__,400)
    response = JSON.parse(@response.to_s)
    if response['message'].nil?
      HtmlReporter.add_step_failed_no_ss('No error message in response', 'Failed')
      raise Exception.new 'No error message in response'
    else
      HtmlReporter.add_step_passed_no_ss('Error Message', response['message'])
    end
  end

  def get_chain_id
    @chain_id
  end

  def get_payload
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.004.001.03">
	<RedOrdrV03>
		<MsgId>
			<Id>CHAIN-9</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>{{acct_id}}</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Elevate B</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>{{isin}}</ISIN>
					</Id>
					<Nm>{{fund_nm}}</Nm>
				</FinInstrmDtls>
				<GrssAmt Ccy="GBP">104.50</GrssAmt>
				<Rndg>RDUP</Rndg>
				<Grp1Or2Units>GRP1</Grp1Or2Units>
				<ChrgDtls>
					<Tp>INIT</Tp>
					<ChrgBsis>FLAT</ChrgBsis>
					<Rate>0.5</Rate>
				</ChrgDtls>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</RedOrdrV03>
</Document>
    '
  end

  def get_payload_units
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.004.001.03">
	<RedOrdrV03>
		<MsgId>
			<Id>CHAIN-9</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>{{acct_id}}</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Elevate B</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>{{isin}}</ISIN>
					</Id>
					<Nm>{{fund_nm}}</Nm>
				</FinInstrmDtls>
				<UnitsNb>
        		<Unit>{{units}}</Unit>
    		</UnitsNb>
				<Rndg>RDUP</Rndg>
				<Grp1Or2Units>GRP1</Grp1Or2Units>
				<ChrgDtls>
					<Tp>INIT</Tp>
					<ChrgBsis>FLAT</ChrgBsis>
					<Rate>0.5</Rate>
				</ChrgDtls>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</RedOrdrV03>
</Document>
    '
  end

  def get_payload_amt
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.004.001.03">
	<RedOrdrV03>
		<MsgId>
			<Id>CHAIN-9</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>{{acct_id}}</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Elevate B</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>{{isin}}</ISIN>
					</Id>
					<Nm>{{fund_nm}}</Nm>
				</FinInstrmDtls>
				<GrssAmt Ccy="GBP">{{amt}}</GrssAmt>
				<Rndg>RDUP</Rndg>
				<Grp1Or2Units>GRP1</Grp1Or2Units>
				<ChrgDtls>
					<Tp>INIT</Tp>
					<ChrgBsis>FLAT</ChrgBsis>
					<Rate>0.5</Rate>
				</ChrgDtls>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</RedOrdrV03>
</Document>
    '
  end

  def get_payload_holdings_red_rate
    '
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:setr.004.001.03">
	<RedOrdrV03>
		<MsgId>
			<Id>CHAIN-9</Id>
			<CreDtTm>{{cre_dt_tm}}</CreDtTm>
		</MsgId>
		<MltplOrdrDtls>
			<InvstmtAcctDtls>
				<AcctId>
					<Prtry>
						<Id>TC1000771-001</Id>
					</Prtry>
				</AcctId>
				<AcctDsgnt>Elevate B</AcctDsgnt>
				<AcctSvcr>
					<PrtryId>
						<Id>{{fund_provider}}</Id>
					</PrtryId>
				</AcctSvcr>
			</InvstmtAcctDtls>
			<IndvOrdrDtls>
				<OrdrRef>{{ordr_ref}}</OrdrRef>
				<FinInstrmDtls>
					<Id>
						<ISIN>GB00B3KKYH59</ISIN>
					</Id>
				</FinInstrmDtls>
				<HldgsRedRate>{{hldgs_red_rate}}</HldgsRedRate>
				<Rndg>RDUP</Rndg>
				<Grp1Or2Units>GRP1</Grp1Or2Units>
				<ChrgDtls>
					<Tp>INIT</Tp>
					<ChrgBsis>FLAT</ChrgBsis>
					<Rate>0.5</Rate>
				</ChrgDtls>
				<ComssnDtls>
					<Tp>FEND</Tp>
					<Bsis>FLAT</Bsis>
					<Rate>0.5</Rate>
					<WvgDtls>
						<InstrBsis>WIUN</InstrBsis>
						<WvdRate>0.5</WvdRate>
					</WvgDtls>
				</ComssnDtls>
				<PhysDlvryInd>true</PhysDlvryInd>
				<ReqdSttlmCcy>GBP</ReqdSttlmCcy>
				<ReqdNAVCcy>GBP</ReqdNAVCcy>
				<RltdPtyDtls>
					<Id>
						<PrtryId>
							<Id>{{fund_provider}}</Id>
						</PrtryId>
					</Id>
				</RltdPtyDtls>
			</IndvOrdrDtls>
		</MltplOrdrDtls>
	</RedOrdrV03>
</Document>
    '
  end

end