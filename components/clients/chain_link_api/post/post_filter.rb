require_relative '../../../../components/clients/http_json_services'

class PostFilter < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + '/api/transactions/filter'
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('POST /api/transactions/filter')
  end

  def send(from_date, to_date)
    from_date = DateTime.parse(from_date)
    to_date = DateTime.parse(to_date)
    @from_date = from_date.strftime("%Y-%m-%dT%H:%M%SZ")
    @to_date = to_date.strftime("%Y-%m-%dT%H:%M%SZ")
    payload = JSON.parse(get_payload)
    payload['creationDateFrom'] = @from_date
    payload['creationDateTo'] = @to_date

    post(__FILE__, payload)
  end

  def validate_count
    validate(__FILE__, 200)

    response = JSON.parse(@response)


    response.each do |chain_id|
      HtmlReporter.add_step_passed_no_ss("CreationDate Validation", "Key: #{chain_id['key']} | Date: #{chain_id['summary']['creationDate']}")
    end
    HtmlReporter.add_step_passed_no_ss('Validate date range', 'Successful')
    HtmlReporter.add_step_passed_no_ss('API Order Count', response.size.to_s)
    return response.size

    # Code to validate the API dates are within range
    # response.each do |order|
    #   if order['summary']['creationDate'] >= @from_date and order['summary']['creationDate'] <= @to_date
    #
    #   end
    # end
  end


  def validate_all_sources_appear
    validate(__FILE__, 200)
    emx_passed = 'N'
    clear_passed = 'N'
    opbank_passed = 'N'

    response = JSON.parse(@response)
    response.each do |transaction|
      if emx_passed == 'Y' and clear_passed == 'Y' and opbank_passed == 'Y'
        HtmlReporter.add_step_passed_no_ss('All sources found', 'Test Passed')
        return
      else
        if transaction['transactionSource'] == 'EMX' and emx_passed == 'N'
          HtmlReporter.add_step_header('EMX Found')
          HtmlReporter.add_step_passed_no_ss('Chain ID', transaction['key'])
          @emx_chain = transaction['key']
          emx_passed = 'Y'
        elsif transaction['transactionSource'] == 'FNZ_CLEAR' and clear_passed == 'N'
          HtmlReporter.add_step_header('FNZ CLEAR found')
          HtmlReporter.add_step_passed_no_ss('Chain ID', transaction['key'])
          @clear_chain = transaction['key']
          clear_passed = 'Y'
        elsif transaction['transactionSource'] == 'FNZ_ONE_OPBANK' and opbank_passed == 'N'
          HtmlReporter.add_step_header('FNZ ONE OPBANK found')
          HtmlReporter.add_step_passed_no_ss('Chain ID', transaction['key'])
          @opbank_chain = transaction['key']
          opbank_passed = 'Y'
        end
      end
    end
    HtmlReporter.add_step_failed_no_ss('All sources NOT found', 'Test Failed')
    raise Exception.new 'Chain ADMIN could not view all sources'
  end

  def get_emx_chain
    return @emx_chain
  end

  def get_clear_chain
    return @clear_chain
  end

  def get_opbank_chain
    return @opbank_chain
  end


  def get_payload
    '{
    "creationDateFrom": "{{from_date}}Z",
    "creationDateTo": "{{to_date}}Z",
    "limit": 500
    }'
  end

end