require_relative '../../../../components/clients/http_json_services'

class PostUserLogin < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + '/api/user/login'
    HtmlReporter.add_step_header('POST /api/user/login')
  end

  def send
    payload = JSON.parse(get_payload)


    payload['enrollmentId'] = Config['chain_link_api']['active_user']
    payload['enrollmentSecret'] = Config['chain_link_api']['active_pw']
    HtmlReporter.add_step_passed_no_ss('User', Config['chain_link_api']['active_user'])
    post(__FILE__,payload, 'Y')
  end

  def validate
    super(__FILE__, 200)

    if @response['token'].nil?
      raise Exception.new "Error - No Token generated"
    end

    puts "Verifying TOKEN: " + @response['token']

    if Config['chain_link_api']['token'] == @response['token']
      HtmlReporter.add_step_failed_no_ss('Old Token', Config['chain_link_api']['token'])
      HtmlReporter.add_step_failed_no_ss('New Token', @response['token'])
      raise Exception.new 'Same token created'
    else
      Config['chain_link_api']['token'] = @response['token']
      HtmlReporter.add_step_passed_no_ss('New Token Created', Config['chain_link_api']['token'])
    end
  end

  private

  def get_payload
    '{
    "enrollmentId": "{{username}}",
    "enrollmentSecret": "{{password}}"
    }'
  end

end