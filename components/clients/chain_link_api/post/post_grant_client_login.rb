require_relative '../../../../components/clients/x_client_services'

class PostGrantClientLogin < XClientServices

  def initialize
    @uri = Config['keycloak']['url'] + '/auth/realms/FNZ-Chainclear/protocol/openid-connect/token'
    HtmlReporter.add_step_header('Get Client Grant Token')
  end

  def send
    post __FILE__, get_payload
  end

  def get_payload
    {
        "grant_type" => "client_credentials"
    }
  end

  def validate
    super(__FILE__, 200)

    if @response['access_token'].nil?
      raise Exception.new "Error - No Token generated"
    end

    puts "Verifying TOKEN: " + @response['access_token']

    if Config['chain_link_api']['token'] == @response['access_token']
      HtmlReporter.add_step_failed_no_ss('Old Token', Config['chain_link_api']['token'])
      HtmlReporter.add_step_failed_no_ss('New Token', @response['access_token'])
      raise Exception.new 'Same token created'
    else
      Config['chain_link_api']['token'] = @response['access_token']
      HtmlReporter.add_step_passed_no_ss('New Token Created', Config['chain_link_api']['token'])
    end
  end
end