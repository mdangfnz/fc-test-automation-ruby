require_relative '../../../../components/clients/http_json_services'

class PostRegenerateCert < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + '/api/user/regenerate-cert'
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('POST Regenerate Cert')
  end

  def send
    payload = JSON.parse(get_payload)


    payload['enrollmentId'] = 'paddyRegenerateCertTest'
    payload['enrollmentSecret'] = 'P@ssw0rd!'
    payload['mspid'] = 'fnztaMSP'
    post_client(__FILE__,payload, 'Y')
  end

  def validate
    super(__FILE__, 200)

    if @response['type'] == 'SUCCESS' and @response['message'] == 'Successfully regenerated'
      HtmlReporter.add_step_passed_no_ss('Regenerate Cert', 'Successful')
    else
      HtmlReporter.add_step_failed_no_ss('Regenerate Cert', 'Failed - Check response payload')
      raise Exception.new "Error - No cert generated"
    end
  end

  def get_payload
    '{
    "enrollmentId": "{{enrollmentId}}",
    "enrollmentSecret": "{{enrollmentSecret}}",
    "mspid": "{{mspid}}"
    }'
  end
end