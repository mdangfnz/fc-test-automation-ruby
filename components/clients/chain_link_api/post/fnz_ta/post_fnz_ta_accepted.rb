require_relative '../../../../../components/clients/http_json_services'

class PostFnzTaAccepted < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + "/api/transaction/fnzta/status"
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Accept trades via FNZ TA endpoint')
  end

  def send(chain_id, order_ref, report_flag = 'Y')
    trade_date = Time.now.utc
    settlement_date = trade_date + (24*60*60) #add 1day
    trade_date = trade_date.strftime("%Y%m%d %H:%M:%S")
    settlement_date = settlement_date.strftime("%Y%m%d")
    @chain_id = chain_id
    @order_ref = order_ref

    payload = JSON.parse(get_payload)
    payload['ctnId'] = chain_id
    payload['ordrRef'] = order_ref
    payload['xpctdTradDtTm'] = trade_date
    payload['xpctdCshSttlmDt'] = settlement_date

    post(__FILE__, payload, report_flag)
    if report_flag == 'N'
      validate_emx
    elsif report_flag == 'PERF'
      puts 'created report for ' + @chain_id
      #don't validate to increase speed
    else
      validate_202
    end
  end

  def validate_emx
    if @response.code == 202
      HtmlReporter.add_step_passed_no_ss 'Accepted', "Chain ID: #{@chain_id} | Order Ref: #{@order_ref}"
    else
      HtmlReporter.add_step_failed_no_ss 'Failed', "Chain ID: #{@chain_id} | Order Ref: #{@order_ref}"
      HtmlReporter.add_step_failed_no_ss 'Response', @response.to_s
    end
  end

  def validate_202
    validate(__FILE__, 202)
    response = JSON.parse(@response.to_s)
    HtmlReporter.add_step_passed_no_ss('Chain ID', response['chainId'])
  end

  #Only the parameterized elements are important, the other elements can remain static - no impact
  def get_payload
    '{
      "ctnId": "{{chain_id}}",
      "ordrRef": "{{order_ref}}",
      "dealRef": "DEALREF",
      "sts": "PASS",
      "xpctdTradDtTm": "{{trade_date}}",
      "xpctdCshSttlmDt": "{{settlement_date}}"
     }'
  end
end