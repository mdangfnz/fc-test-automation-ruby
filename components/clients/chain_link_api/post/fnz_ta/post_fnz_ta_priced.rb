require_relative '../../../../../components/clients/http_json_services'


class PostFnzTAPriced < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + "/api/transaction/fnzta/price"
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('PRICE trades via FNZ TA endpoint')
  end

  def send(chain_id, order_ref, order_type, report_flag = 'Y')
    trade_date = Time.now.utc
    settlement_date = trade_date + (24 * 60 * 60) #add 1day
    trade_date = trade_date.strftime("%Y%m%d %H:%M:%S")
    settlement_date = settlement_date.strftime("%d/%m/%Y")
    @chain_id = chain_id
    @order_ref = order_ref

    payload = JSON.parse(get_payload)
    payload['ctnId'] = chain_id
    payload['ordrRef'] = order_ref
    payload['tradDtTm'] = trade_date
    payload['cshSttlmDt'] = settlement_date
    payload['ordrTyp'] = order_type

    if DataPrep.hash.has_key? 'fund_id'
      payload['fndId'] = DataPrep['fund_id']
    end

    if DataPrep.hash.has_key? 'deal_ref'
      payload['dealRef'] = DataPrep['deal_ref']
    end

    if DataPrep.hash.has_key? 'acct_id'
      payload['acctId'] = DataPrep['acct_id']
    end

    if DataPrep.hash.has_key? 'net_amt'
      payload['netAmt'] = DataPrep['net_amt']
    end

    if DataPrep.hash.has_key? 'sttlm_amt'
      payload['sttlmAmt'] = DataPrep['sttlm_amt']
    end

    if DataPrep.hash.has_key? 'price_amt'
      payload['pricAmt'] = DataPrep['price_amt']
    end

    if DataPrep.hash.has_key? 'grss_amt'
      payload['grssAmt'] = DataPrep['grss_amt']
    end

    if DataPrep.hash.has_key? 'unit'
      payload['unit'] = DataPrep['unit']
    end

    if DataPrep.hash.has_key? 'trad_dt_tm'
      payload['tradDtTm'] = DataPrep['trad_dt_tm']
    end

    if DataPrep.hash.has_key? 'csh_sttlm_dt'
      payload['cshSttlmDt'] = DataPrep['csh_sttlm_dt']
    end

    if DataPrep.hash.has_key? 'ordr_typ'
      payload['ordrTyp'] = DataPrep['ordr_typ']
    end

    if DataPrep.hash.has_key? 'net_amt_ccy'
      payload['netAmtCcy'] = DataPrep['net_amt_ccy']
    end

    if DataPrep.hash.has_key? 'grss_amt_ccy'
      payload['grssAmtCcy'] = DataPrep['grss_amt_ccy']
    end

    if DataPrep.hash.has_key? 'sttlm_amt_ccy'
      payload['sttlmAmtCcy'] = DataPrep['sttlm_amt_ccy']
    end

    if DataPrep.hash.has_key? 'price_amt_ccy'
      payload['pricAmtCcy'] = DataPrep['price_amt_ccy']
    end


    post(__FILE__, payload, report_flag)
    if report_flag == 'N'
      validate_emx
    elsif report_flag == '400'
      #call 'my confirmation receives a 400 error' step
    else
      validate_202
    end
  end

  def validate_emx
    if @response.code == 202
      HtmlReporter.add_step_passed_no_ss 'Priced', "Chain ID: #{@chain_id} | Order Ref: #{@order_ref}"
    else
      HtmlReporter.add_step_failed_no_ss 'Failed', "Chain ID: #{@chain_id} | Order Ref: #{@order_ref}"
      HtmlReporter.add_step_failed_no_ss 'Response', @response.to_s
    end
  end

  def validate_202
    validate(__FILE__,202)
    response = JSON.parse(@response.to_s)
    HtmlReporter.add_step_passed_no_ss('Chain ID', response['chainId'])
  end

  def validate_400
    validate(__FILE__, 400)
    response = JSON.parse(@response.to_s)
    HtmlReporter.add_step_passed_no_ss 'Message', response['message']
  end

  #Only the parameterized elements are important, the other elements can remain static - no impact
  def get_payload
    '{
      "ctnId": "{{chain_id}}",
      "ordrTyp": "{{order_type}}",
      "acctId": "SA1000350-001",
      "ordrRef": "{{order_ref}}",
      "dealRef": "ABCDEF1234",
      "fndIdType": "ISIN",
      "fndId": "GB0000252341",
      "fndNm": "Test fund",
      "unit": "43.122035",
      "netAmt": "100",
      "netAmtCcy": "GBP",
      "grssAmt": "100",
      "grssAmtCcy": "GBP",
      "tradDtTm": "{{trade_date}}",
      "pricTp": "ACTU",
      "pricAmt": "2.319",
      "pricAmtCcy": "GBP",
      "sttlmAmt": "100",
      "sttlmAmtCcy": "GBP",
      "cshSttlmDt": "{{settlement_date}}"
    }'
  end
end