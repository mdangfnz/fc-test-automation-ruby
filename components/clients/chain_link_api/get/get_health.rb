require_relative '../../../../components/clients/http_json_services'

class GetHealth < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + "/health"
    @token = Config['chain_link_api']['token']
  end

  def send
    get
  end

  def validate
    raise Exception.new "Status is not healthy" unless @response['status'] == 'API is healthy'
  end
end