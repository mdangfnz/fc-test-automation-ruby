require_relative '../../../../components/clients/http_json_services'

class GetTransactionStatusReport < HttpJsonServices

  def initialize
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Get Transaction Status Report by Chain ID')
  end

  def send(chain_id)
    @chain_id = chain_id
    @uri = Config['chain_link_api']['url'] + "/api/transaction/#{@chain_id}/statusReport"
    get
  end

  def status_report_received?
    expected_response = 200
    puts "***** GetTransactionStatusReport RESPONSE ***** "


    if @response.code == expected_response
      HtmlReporter.add_step_passed_no_ss("Expected: #{expected_response}", "Actual: #{@response.code.to_s}")
      return true
    else
      HtmlReporter.add_step_failed_no_ss("Expected: #{expected_response}", "Actual: #{@response.code.to_s}")
      return false
    end
  end


end