require_relative '../../../../components/clients/http_json_services'

class GetErrors < HttpJsonServices

  def initialize
    @uri = Config['chain_link_api']['url'] + '/api/errors'
    @token = Config['chain_link_api']['token']
  end

  def send
    HtmlReporter.add_step_header('Get Errors')
    get
  end

  def update_error_state(errors)
    HtmlReporter.add_step_header('Update Error State')
    payload = '['
    errors.each do |error_id|
      payload += "{\"id\":\"#{error_id}\"},"
    end
    payload = payload[0..-2]
    payload = payload + ']'
    payload = JSON.parse(payload.to_s)
    put(__FILE__, payload)
    validate(__FILE__,200)
  end

  def get_error_count
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)
    HtmlReporter.add_step_passed_no_ss('Error Count', response.size.to_s)
    return response.size.to_s
  end

  def get_all_error_ids
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)
    error_arr = []
    response.each do |error|
      error_arr << error['id']
    end
    HtmlReporter.add_step_passed_no_ss('Error IDs obtained', "Count: #{response.size.to_s}")
    return error_arr
  end

  def query_error(chain_id)
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)
    response = response.reverse

    response.each_with_index do |transaction, count|
      if transaction['payload']['chainId'] == chain_id
        HtmlReporter.add_step_passed_no_ss('Error Found', transaction['message'])
        HtmlReporter.add_step_passed_no_ss('Error ID', transaction['id'])
        HtmlReporter.add_step_passed_no_ss('Chain ID', transaction['payload']['chainId'])
        HtmlReporter.add_step_passed_no_ss('Trace ID', transaction['payload']['traceId'])
        @trace_id = transaction['payload']['traceId']
        @error_id = transaction['id']
        return
      end
    end

    HtmlReporter.add_step_failed_no_ss('Error not found', 'Fail')
    raise Exception.new 'Chain ID did not create expected error'

  end

  def get_trace_id
    return @trace_id
  end

  def get_error_id
    return @error_id
  end
end