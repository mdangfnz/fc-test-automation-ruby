require_relative '../../../../components/clients/http_json_services'

class GetMessageByOrderReference < HttpJsonServices

  def initialize(order_ref)
    @uri = Config['chain_link_api']['url'] + "/api/messages/#{order_ref}/orderReference"
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Wait for order to be Accepted')
  end

  def send
    get
  end

  def validate_listener(status)
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)
    if response[0]['listenerStatus'] == status
      HtmlReporter.add_step_passed_no_ss("Expected: #{status}", "Actual: #{response[0]['listenerStatus']}")
      return true
    else
      return false
    end
  end
end