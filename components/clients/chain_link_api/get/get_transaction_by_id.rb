require_relative '../../../../components/clients/http_json_services'

class GetTransactionByID < HttpJsonServices

  def initialize
    @chain_id = ''
    @uri = Config['chain_link_api']['url'] + "/api/transaction/#{@chain_id}"
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Get Transaction by Chain ID')
  end

  def send(chain_id)
    @chain_id = chain_id
    @uri = Config['chain_link_api']['url'] + "/api/transaction/#{@chain_id}"
    get
  end

  def validate_transaction_is_priced(chain_id)
    send chain_id
  end

  def validate_response_details
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)

    HtmlReporter.add_step_passed_no_ss('Chain ID', response['key'])
    HtmlReporter.add_step_passed_no_ss('Transaction Source', response['transactionSource'])
    HtmlReporter.add_step_passed_no_ss('Created DateTime', response['summary']['creationDate'])
    HtmlReporter.add_step_passed_no_ss('Last Updated Time', response['lastUpdatedTime'])
  end

  def get_received_trade_details
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)

    response['audit'].each do |audit|
      if audit['status'] == 'TRANSACTION_RECEIVED_BY_CHAIN'
        transaction_arr = []
        transaction_data = {}
        transaction_data['chain_id'] = response['key']
        transaction_data['order_ref'] = response['summary']['orderReference']
        transaction_data['order_type'] = response['orderType']
        transaction_data['creation_date'] = response['summary']['creationDate']
        transaction_arr << transaction_data
        return transaction_arr
      end
    end

    HtmlReporter.add_step_failed_no_ss('Chain not received', 'Fail Test')
  end
end
