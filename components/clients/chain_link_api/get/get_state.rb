require_relative '../../../../components/clients/http_json_services'

class GetState < HttpJsonServices

  def initialize(state)
    @state = state
    @uri = Config['chain_link_api']['url'] + "/api/transactions/#{@state}/state"
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header("Get state with parameter - #{@state}")
  end

  def send
    get
  end

  def validate_state
    validate(__FILE__, 200)

    # @state needs to be initialized in the child class
    response = JSON.parse(@response)
    HtmlReporter.add_step_passed_no_ss("Report Info", "Only first 10 shown, to reduce report size")
    response.each_with_index do |chain_id, count|
      unless chain_id['status'] == @state
        HtmlReporter.add_step_failed_no_ss("Chain output", "Key: #{chain_id['key']} | Status: #{chain_id['status']}")
        raise Exception.new 'Incorrect state found'
      end
      if count < 10
        HtmlReporter.add_step_passed_no_ss("Chain output", "Key: #{chain_id['key']} | Status: #{chain_id['status']}")
      end
    end
    HtmlReporter.add_step_passed_no_ss("Validated #{response.size.to_s} chain_ids", 'Successful')

  end

  def get_orders_by_order_ref(order_ref)
    get
    validate(__FILE__, 200)
    response = JSON.parse(@response)
    transaction_arr = []

    response.each do |transaction|
      transaction_data = {}
      if transaction['summary']['orderReference'].include? order_ref
        transaction_data['chain_id'] = transaction['key']
        transaction_data['order_ref'] = transaction['summary']['orderReference']
        transaction_data['order_type'] = transaction['orderType']
        transaction_data['creation_date'] = transaction['summary']['creationDate']
        transaction_arr << transaction_data
      end
    end
    return transaction_arr
  end

  def get_order_by_chain_id(chain_id)
    get
    validate(__FILE__, 200)
    response = JSON.parse(@response)
    transaction_arr = []

    response.each do |transaction|
      transaction_data = {}
      if transaction['key'].include? chain_id
        transaction_data['chain_id'] = transaction['key']
        transaction_data['order_ref'] = transaction['summary']['orderReference']
        transaction_data['order_type'] = transaction['orderType']
        transaction_data['creation_date'] = transaction['summary']['creationDate']
        transaction_arr << transaction_data
      end
    end
    return transaction_arr
  end


  def get_emx_file_orders
    get
    validate(__FILE__, 200, 'N')


    response = JSON.parse(@response)
    transaction_arr = []
    emx_order_string = File.read(Config['emx_e2e']['base'] + Config['emx_e2e']['active_file'])

    #Get the following:
    # Chain ID, ORDER REF, ORDER TYPE, FUND, FUND NAME, and
    response.each do |transaction|
      transaction_data = {}
      if transaction['summary']['orderReference'].include? emx_order_string
        transaction_data['chain_id'] = transaction['key']
        transaction_data['order_ref'] = transaction['summary']['orderReference']
        transaction_data['order_type'] = transaction['orderType']
        transaction_data['isin'] = transaction['summary']['isin']
        transaction_arr << transaction_data
      end
    end
    HtmlReporter.add_step_passed_no_ss "Find orders matching #{emx_order_string}", "#{transaction_arr.size} found"
    return transaction_arr
  end

  def get_all_emx_perf_orders
    get
    validate(__FILE__, 200, 'N')


    response = JSON.parse(@response)
    transaction_arr = []

    #Get the following:
    # Chain ID, ORDER REF, ORDER TYPE, FUND, FUND NAME, and
    response.each do |transaction|
      transaction_data = {}
      if transaction['transactionSource'] == 'EMX'
        transaction_data['chain_id'] = transaction['key']
        transaction_data['order_ref'] = transaction['summary']['orderReference']
        transaction_data['order_type'] = transaction['orderType']
        transaction_data['isin'] = transaction['summary']['isin']
        transaction_arr << transaction_data
      end
    end
    return transaction_arr
  end

  def get_latest_by_source(source)
    get
    validate(__FILE__, 200, 'N')

    response = JSON.parse(@response)
    transaction_arr = []
    #Get the following:
    # Chain ID, ORDER REF, ORDER TYPE, FUND, FUND NAME, and
    response.each do |transaction|
      if transaction['transactionSource'] == source
        transaction_data = {}
        transaction_data['chain_id'] = transaction['key']
        transaction_data['order_ref'] = transaction['summary']['orderReference']
        transaction_data['order_type'] = transaction['orderType']
        transaction_data['isin'] = transaction['summary']['isin']
        transaction_data['creation_date'] = transaction['summary']['creationDate']
        transaction_arr << transaction_data
        HtmlReporter.add_step_passed_no_ss "Order found", transaction['key']
        return transaction_arr
      end
    end
    HtmlReporter.add_step_failed_no_ss('Order not found', 'Check Script')
    raise Exception.new 'Order not found'
    return transaction_arr
  end

  def validate_opbank_source
    validate(__FILE__, 200)

    # @state needs to be initialized in the child class
    response = JSON.parse(@response)
    response.each_with_index do |chain_id, count|
      unless chain_id['transactionSource'] == 'FNZ_ONE_OPBANK'
        HtmlReporter.add_step_failed_no_ss("Chain output", "Key: #{chain_id['key']} | Source: #{chain_id['transactionSource']}")
        raise Exception.new 'Incorrect state found'
      end
    end
    HtmlReporter.add_step_passed_no_ss("Validated #{response.size.to_s} chain_ids", 'All sources are FNZ_ONE_OPBANK')
  end

  def validate_source(transaction_source)
    validate(__FILE__, 200)

    response = JSON.parse(@response)
    response.each_with_index do |chain_id, count|
      unless chain_id['transactionSource'] == transaction_source
        HtmlReporter.add_step_failed_no_ss("Chain output", "Key: #{chain_id['key']} | Source: #{chain_id['transactionSource']}")
        raise Exception.new 'Incorrect transaction source found'
      end
    end
    HtmlReporter.add_step_passed_no_ss("Validated #{response.size.to_s} chain_ids", "All orders sourced from #{transaction_source}")
  end

  def validate_source_none(transaction_source)
    validate(__FILE__, 200)

    response = JSON.parse(@response)
    response.each_with_index do |chain_id, count|
      if chain_id['transactionSource'] == transaction_source
        HtmlReporter.add_step_failed_no_ss("Chain output", "Key: #{chain_id['key']} | Source: #{chain_id['transactionSource']}")
        raise Exception.new 'Incorrect transaction source found'
      end
    end
    HtmlReporter.add_step_passed_no_ss("Validated #{response.size.to_s} chain_ids", "No orders sourced from #{transaction_source}")
  end

  def validate_401_no_token
    validate(__FILE__, 401)

    response = JSON.parse(@response.to_s)
    if response['message'] == 'No token provided' or response['message'] == 'Invalid Token or Key'
      HtmlReporter.add_step_passed_no_ss('No Token Error Found', response['message'])
    else
      HtmlReporter.add_step_failed_no_ss('No Token Error NOT FOUND', 'Test Failed')
      raise Exception.new '401 no token error was not found'
    end
  end

end