require_relative '../../../../components/clients/http_json_services'

class GetRequestsById < HttpJsonServices


  def initialize
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Get Requests by Chain ID')
  end

  def send(chain_id)
    @chain_id = chain_id
    @uri = Config['chain_link_api']['url'] + "/api/requests?chainId=#{@chain_id}"
    get
  end

  def validate_response_details
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)

    response.each_with_index do |request,index|
      HtmlReporter.add_step_header("Request #{index+1}")
      HtmlReporter.add_step_passed_no_ss('Request ID', request['id'].to_s)
      HtmlReporter.add_step_passed_no_ss('Chain ID', request['chainId'])
      HtmlReporter.add_step_passed_no_ss('Created At', request['createdAt'])
      HtmlReporter.add_step_passed_no_ss('Updated At', request['updatedAt'])
      @request_id = request['id']
    end
  end

  def get_state_update_request(state)
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)

    response.each do |request|
      if request['request']['payload']['body'].has_key? 'state'
        if request['request']['payload']['body']['state'] == state.upcase
          HtmlReporter.add_step_header("#{state} Request Details")
          HtmlReporter.add_step_passed_no_ss('Request ID', request['id'].to_s)
          HtmlReporter.add_step_passed_no_ss('State', request['request']['payload']['body']['state'])
          HtmlReporter.add_step_passed_no_ss('Chain ID', request['chainId'])
          HtmlReporter.add_step_passed_no_ss('Created At', request['createdAt'])
          HtmlReporter.add_step_passed_no_ss('Updated At', request['updatedAt'])
          @request_id = request['id']
          return @request_id
        end
      end

    end
    HtmlReporter.add_step_failed_no_ss('Find State Update in requests', 'Failed')
    raise Exception.new 'Unable to find State update in transaction requests'
  end

  def get_outbound_status_report_request
    validate(__FILE__, 200)
    response = JSON.parse(@response.to_s)

    response.each do |request|
      if request['request'].has_key? 'queueName'
        if request['request']['queueName'].include? 'outbound-statusreport'
          HtmlReporter.add_step_header("Outbound Status Report Found")
          HtmlReporter.add_step_passed_no_ss('Request ID', request['id'].to_s)
          HtmlReporter.add_step_passed_no_ss('Queue', request['request']['queueName'])
          HtmlReporter.add_step_passed_no_ss('Chain ID', request['chainId'])
          HtmlReporter.add_step_passed_no_ss('Created At', request['createdAt'])
          HtmlReporter.add_step_passed_no_ss('Updated At', request['updatedAt'])
          @request_id = request['id']
          return @request_id
        end
      end

    end
    HtmlReporter.add_step_failed_no_ss('Find State Update in requests', 'Failed')
    raise Exception.new 'Unable to find State update in transaction requests'
  end

  def get_request_id
    return @request_id
  end
end