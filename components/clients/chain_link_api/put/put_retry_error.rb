require_relative '../../../../components/clients/http_json_services'

class PutRetryError < HttpJsonServices

  def initialize
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Retry Error')
  end

  def send(trace_id)
    @uri = Config['chain_link_api']['url'] + "/api/error/#{trace_id}"
    put
  end

  def validate_chain_id(chain_id)
    validate(__FILE__, 202)
    response = JSON.parse(@response.to_s)
    if response['chainId'] == chain_id
      HtmlReporter.add_step_passed_no_ss('ChainID matched', "Expected: #{chain_id} | Actual: #{response['chainId']}")
    else
      HtmlReporter.add_step_failed_no_ss('ChainID failed to match', "Expected: #{chain_id} | Actual: #{response['chainId']}")
      raise Exception.new 'Retry error returned different CHAIN ID'
    end
  end


end