require_relative '../../../../components/clients/http_json_services'

class PutRetryRequest < HttpJsonServices

  def initialize
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Retry Request')
  end

  def send(request_id)
    HtmlReporter.add_step_passed_no_ss('Request ID', request_id.to_s)
    @uri = Config['chain_link_api']['url'] + "/api/request/#{request_id}"
    put
  end

  def retry_state_update(state)

  end

  def validate
    super(__FILE__, 202)
    response = JSON.parse(@response.to_s)
    if response['message'] == 'Request retry processing'
      HtmlReporter.add_step_passed_no_ss('Success', response['message'])
    else
      HtmlReporter.add_step_failed_no_ss('Failure', response['message'])
      raise Exception.new 'Retry Request failed'
    end
  end


end