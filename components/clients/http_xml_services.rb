require 'httparty'
require 'json'
require 'fileutils'
require 'nokogiri'
require_relative '../../utilities/strings_util'
require 'nori'

#TODO: Investigate how to handle apis
#
# This class is used to test RESTful JSON clients.
class HttpXmlServices
  include HTTParty
  #include ReportHelper

  def initialize(uri='', uid=nil, pass=nil)
    @uri = uri
  end

  def post(service, request, report = 'Y')
    service = service.split('/').pop[0..-4] #get class name without the .rb
    puts "***** POST REQUEST: " + @uri
    puts "***** PAYLOAD: " + request.to_s
    if report == 'Y'
      HtmlReporter.add_step_passed_no_ss'URL', @uri
      file_name = "#{service}_request.xml"
      file = File.open(Config['file_path']['archives'] + "/#{file_name}",'w')
      file.write(request)
      HtmlReporter.add_step_with_file("Request", file_name)
    end


    if @token.nil?
      @response = self.class.post(@uri,
                                  body: request,
                                  headers: {'Content-Type' => 'application/xml',
                                            Accept: 'application/vnd.fnzchain.v1.0+json'})
    else
      @response = self.class.post(@uri,
                                  body: request,
                                  headers: {'Content-Type' => 'application/xml',
                                            Accept: 'application/vnd.fnzchain.v1.0+json',
                                            Authorization: 'Bearer ' + @token})
    end
  end

  def get()
    puts "***** GET REQUEST: " + @uri
    HtmlReporter.add_step_passed_no_ss'URL', @uri

    if @token.nil?
      @response = self.class.get(@uri,
                                 headers: {'Content-Type' => 'application/xml',
                                           Accept: 'application/vnd.fnzchain.v1.0+json'})
    else
      @response = self.class.get(@uri,
                                 headers: {'Content-Type' => 'application/xml',
                                           Accept: 'application/vnd.fnzchain.v1.0+json',
                                           Authorization: 'Bearer ' + @token})
    end
  end

  def put(request, version='1.0')
    puts "***** PUT REQUEST: " + request.to_s
    HtmlReporter.add_step_passed_no_ss'URL', @uri
    HtmlReporter.add_step_passed_no_ss'POST Request', request.to_s
    #ReportHelper.report_step_passed_with_screenshot 'JSON Message', "#{request.to_s}"

    if @token.nil?
      @response = self.class.put(@uri,
                                 body: request,
                                 headers: {'Content-Type' => 'application/xml',
                                           Accept: 'application/vnd.fnzchain.v1.0+json'})
    else
      @response = self.class.put(@uri,
                                 body: request,
                                 headers: {'Content-Type' => 'application/xml',
                                           Accept: 'application/vnd.fnzchain.v1.0+json',
                                           Authorization: 'Bearer ' + @token})
    end
  end

  def validate(service, expected_response, report = 'Y')
    service = service.split('/').pop[0..-4] #get class name without the .rb
    puts "***** #{service} RESPONSE ***** "

    response = JSON.parse(@response.to_s)
    response = JSON.pretty_generate(response)

    unless @response.code == expected_response
      HtmlReporter.add_step_failed_no_ss("Expected: #{expected_response}", "Actual: #{@response.code.to_s}")
      raise Exception.new "Response Error - #{expected_response} not received"
    else
      HtmlReporter.add_step_passed_no_ss("Expected: #{expected_response}", "Actual: #{@response.code.to_s}")
    end

    if report == 'Y'
      file_name = "#{service}_response.json"
      file = File.open(Config['file_path']['archives'] + "/#{file_name}",'w')
      file.write(response)
      HtmlReporter.add_step_with_file("Response", file_name)
    end
  end
end