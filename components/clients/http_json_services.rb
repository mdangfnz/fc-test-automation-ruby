require 'httparty'
require 'json'
require 'fileutils'
require 'nokogiri'

#TODO: Investigate how to handle apis
#
# This class is used to test RESTful JSON clients.
class HttpJsonServices
  include HTTParty
  #include ReportHelper

  def initialize(uri='', uid=nil, pass=nil)
    @uri = uri
  end

  def post(service, request, report = 'Y', version = '1.0')
    service = service.split('/').pop[0..-4] #get class name without the .rb



    if @token.nil?
      @response = self.class.post(@uri,
                                  body: request.to_json,
                                  headers: {'Content-Type' => 'application/json',
                                            Accept: "application/vnd.fnzchain.v#{version}+json"})
    else
      @response = self.class.post(@uri, 
                                  body: request.to_json,
                                  headers: {'Content-Type' => 'application/json',
                                            Accept: "application/vnd.fnzchain.v#{version}+json",
                                            Authorization: 'Bearer ' + @token})
    end

    request = JSON.pretty_generate(request)
    puts "***** POST REQUEST: " + @uri
    puts "***** PAYLOAD: " + request.to_json.to_s
    if report == 'Y'
      HtmlReporter.add_step_passed_no_ss'URL', @uri
      file_name = "#{service}_request_#{Time.now.strftime('%H%M%S')}.json"
      file = File.open(Config['file_path']['archives'] + "/#{file_name}",'w')
      file.write(request)
      HtmlReporter.add_step_with_file("Request", file_name)
    end
  end

  def post_client(service, request, report = 'Y', version = '1.0')
    service = service.split('/').pop[0..-4] #get class name without the .rb



    if @token.nil?
      @response = self.class.post(@uri,
                                  body: request.to_json,
                                  headers: {'Content-Type' => 'application/json',
                                            Accept: "application/vnd.fnzchain.v#{version}+json"})
    else
      @response = self.class.post(@uri,
                                  body: request.to_json,
                                  headers: {'Content-Type' => 'application/json',
                                            Accept: "application/vnd.fnzchain.v#{version}+json",
                                            Authorization: 'Bearer ' + @token,
                                            clientid: '#0udSYUp@W0E'})
    end

    request = JSON.pretty_generate(request)
    puts "***** POST REQUEST: " + @uri
    puts "***** PAYLOAD: " + request.to_json.to_s
    if report == 'Y'
      HtmlReporter.add_step_passed_no_ss'URL', @uri
      file_name = "#{service}_request_#{Time.now.strftime('%H%M%S')}.json"
      file = File.open(Config['file_path']['archives'] + "/#{file_name}",'w')
      file.write(request)
      HtmlReporter.add_step_with_file("Request", file_name)
    end
  end

  def get(version = '1.0')
    puts "***** GET REQUEST: " + @uri
    HtmlReporter.add_step_passed_no_ss'URL', @uri

    if @token.nil?
      @response = self.class.get(@uri,
                                 headers: {'Content-Type' => 'application/json',
                                           Accept: "application/vnd.fnzchain.v#{version}+json"})
    else
      @response = self.class.get(@uri,
                                 headers: {'Content-Type' => 'application/json',
                                           Accept: "application/vnd.fnzchain.v#{version}+json",
                                           Authorization: 'Bearer ' + @token})
    end
  end

  def put(service = ' ', request = ' ', version='1.0', report = 'Y')

    if @token.nil?
      if request == ' '
        @response = self.class.put(@uri,
                                   headers: {'Content-Type' => 'application/json'})
      else
        @response = self.class.put(@uri,
                                    body: request.to_json,
                                    headers: {'Content-Type' => 'application/json'})
      end

    else
      if request == ' '
        @response = self.class.put(@uri,
                                   headers: {'Content-Type' => 'application/json',
                                             Authorization: 'Bearer ' + @token})
      else
        @response = self.class.put(@uri,
                                   body: request.to_json,
                                   headers: {'Content-Type' => 'application/json',
                                             Authorization: 'Bearer ' + @token})
      end

    end

    #Store Payload if passed
    unless service == ' '
      service = service.split('/').pop[0..-4] #get class name without the .rb
      request = JSON.pretty_generate(request)
      puts "***** PUT REQUEST: " + @uri
      puts "***** PAYLOAD: " + request.to_json.to_s
      if report == 'Y'
        HtmlReporter.add_step_passed_no_ss'URL', @uri
        file_name = "#{service}_request_#{Time.now.strftime('%H%M%S')}.json"
        file = File.open(Config['file_path']['archives'] + "/#{file_name}",'w')
        file.write(request)
        HtmlReporter.add_step_with_file("Request", file_name)
      end
    end

  end

  def validate(service, expected_response, report = 'Y')
    service = service.split('/').pop[0..-4] #get class name without the .rb
    puts "***** #{service} RESPONSE ***** "

    response = JSON.parse(@response.to_s)
    response = JSON.pretty_generate(response)


    if report == 'Y'
      file_name = "#{service}_response_#{Time.now.strftime('%H%M%S')}.json"
      file = File.open(Config['file_path']['archives'] + "/#{file_name}",'w')
      file.write(response)
      HtmlReporter.add_step_with_file("Response", file_name)
    end

    unless @response.code == expected_response
      HtmlReporter.add_step_failed_no_ss("Expected: #{expected_response}", "Actual: #{@response.code.to_s}")
      raise Exception.new "Response Error - #{expected_response} not received"
    else
      HtmlReporter.add_step_passed_no_ss("Expected: #{expected_response}", "Actual: #{@response.code.to_s}")
    end
  end
end