require_relative '../../../../components/clients/http_json_services'

class GetGatewayMessageById < HttpJsonServices

  def initialize
    @token = Config['chain_link_api']['token']
    HtmlReporter.add_step_header('Get WS Message By Chain ID')
  end

  def send(chain_id)
    @chain_id = chain_id
    @uri = Config['ws_gateway_api']['url'] + "/api/messages/chainId/#{@chain_id}"
    get
  end

  def validate_reconsumed
    validate(__FILE__, 200)

    response = JSON.parse(@response.to_s)
    response.each do |request|
      if request['messageStatus'] == 'RECONSUMED'
        if request['requestTime'].include? Time.now.utc.strftime('%Y-%m-%d')
          HtmlReporter.add_step_passed_no_ss('Request Time', request['requestTime'])
          HtmlReporter.add_step_passed_no_ss('Message Type', request['messageType'])
          HtmlReporter.add_step_passed_no_ss('Chain ID', request['chainId'])
          HtmlReporter.add_step_passed_no_ss('Order Ref', request['orderReference'])
          HtmlReporter.add_step_passed_no_ss('Message Status', request['messageStatus'])
          HtmlReporter.add_step_passed_no_ss('Sequence', request['sequence'].to_s)
          return
        end
      end
    end
    HtmlReporter.add_step_failed_no_ss('Retry RECONSUMED message not found', 'Fail Test')
    raise Exception.new 'Retry RECONSUMED message not found'
  end

  def get(version = '1.0')
    puts "***** GET REQUEST: " + @uri
    HtmlReporter.add_step_passed_no_ss 'URL', @uri
    @response = self.class.get(@uri, headers: {Authorization: 'Bearer ' + @token})
  end

end