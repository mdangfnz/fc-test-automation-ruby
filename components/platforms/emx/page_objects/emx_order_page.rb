require_relative '../../../../components/platforms/page_object'

class EmxOrderPage < PageObject

  def mailbox_link
    BrowserObject.obj.a(text: 'Mailbox')
  end

  def mailbox_orders_link
    BrowserObject.obj.a(text: 'Orders')
  end

  def from_date_field
    BrowserObject.obj.text_field(name: 'fromDate')
  end

  def to_date_field
    BrowserObject.obj.text_field(name: 'toDate')
  end

  def search_button
    BrowserObject.obj.button(id: 'btnSearch')
  end

  def merrd_all_checkbox
    BrowserObject.obj.checkbox(name: 'merred')
  end

  def merrd_unread_checkbox
    BrowserObject.obj.checkbox(name: 'merredUnread')
  end

  def booked_checkbox
    BrowserObject.obj.checkbox(name: 'booked')
  end

  def complete_checkbox
    BrowserObject.obj.checkbox(name: 'complete')
  end

  def not_booked_checkbox
    BrowserObject.obj.checkbox(name: 'notBooked')
  end

  def records_displayed_option
    BrowserObject.obj.select(id: 'ddlPageSize')
  end

  def expected_orders(count)
    BrowserObject.obj.strong(text: /#{count}/).exists?
  end

  def order_reference_field
    BrowserObject.obj.text_field(name: 'orderReference')
  end

  def failed_order_rows
    BrowserObject.obj.trs(class: 'sep')
  end

  def failed_order_text
    BrowserObject.obj.tr(text: /Text/).td(colspan: '3')
  end
end