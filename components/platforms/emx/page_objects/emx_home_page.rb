require_relative '../../../../components/platforms/page_object'

class EmxHomePage < PageObject

  def upload_link
    BrowserObject.obj.a(text: 'Upload')
  end

  def message_type_option
    BrowserObject.obj.select(id: 'ddlmessageTypes')
  end

  def choose_file_path(upload_file_name)

    file_path_arr = Dir[Config['file_path']['archives'] + '/' + upload_file_name]

    #Error Handling
    if file_path_arr.empty?
      HtmlReporter.add_step_failed_no_ss('File Error', "No file found matching 'Emx Order'")
      raise Exception.new 'EMX FILE error'
    end
    BrowserObject.obj.file_field.set file_path_arr[0]
    HtmlReporter.add_step_passed_with_ss'File selected', file_path_arr[0].split('/').pop
  end

  def upload_button
    BrowserObject.obj.button(id: 'btnUpload')
  end

  def upload_error
    BrowserObject.obj.div(id: 'messageError', text: 'Incorrect number of column headings')
  end

  def upload_confirmation_button
    BrowserObject.obj.a(class: 'ZebraDialog_Button0')
  end




end