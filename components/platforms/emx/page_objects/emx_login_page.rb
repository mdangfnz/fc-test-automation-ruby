require_relative '../../../../components/platforms/page_object'

class EmxLoginPage < PageObject

  def login_button
    BrowserObject.obj.button(id: 'btnLogin')
  end

  def company_id_field
    BrowserObject.obj.text_field(id: 'companyIDId')
  end

  def username_field
    BrowserObject.obj.text_field(id: 'userNameId')
  end

  def password_field
    BrowserObject.obj.text_field(id: 'passwordId')
  end
end