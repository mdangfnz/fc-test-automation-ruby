require_relative '../../../../utilities/html_reporter'
require_relative '../../../../utilities/data_prep'
require_relative '../../../../utilities/strings_util'

class EmxFileWriter

  def initialize

  end

  def write_archive_file
    #Generate EMX String
    order_string = 'ATEST_' + StringsUtil.generate_code(5)

    #Get emx storage file based on EMX Scenario
    emx_storage_file = Config['emx_e2e']['active_file']
    storage_file_name = emx_storage_file.gsub('/', '')

    #Open emx storage file and write new EMX string - this will be used later parts of the e2e scenario
    file_obj = File.open(storage_file_name, 'w')
    file_obj.write(order_string)
    archive_emx_string(storage_file_name)
    file_obj.close
    HtmlReporter.add_step_passed_no_ss 'EMX Order String', order_string


    #Open Emx Order Upload file with new order string
    upload_template = get_emx_template(Config['emx_e2e']['active_file'])
    text = File.read(File.join(File.dirname(__FILE__), upload_template))
    @upload_file_name = "#{order_string}_#{rand(10).to_s}.txt"

    #Get number of orders in upload template and replace them
    order_count = text.scan(/ORDER_REF/).count
    order_count.times do |index|
      text.gsub!("{{ORDER_REF_#{((index + 1).to_s)}}}", order_string + (index + 1).to_s)
    end

    #Write and archive the updated upload template
    file = File.open(@upload_file_name, 'w')
    file.write(text)
    file.close
    archive_file @upload_file_name
    HtmlReporter.add_step_with_file('Emx Order Upload File', @upload_file_name)
  end

  def get_upload_file_name
    return @upload_file_name
  end

  def generate_code(number)
    charset = Array('A'..'Z')
    Array.new(number) { charset.sample }.join
  end

  def archive_file(file_name)
    FileUtils.move(file_name, "#{Config['file_path']['archives']}/#{file_name}")
  end

  def archive_emx_string(file_name)
    FileUtils.move(file_name, "#{Config['emx_e2e']['base']}/#{file_name}")
  end

  def get_emx_template(scenario)
    case scenario
    when Config['emx_e2e']['complete_e2e']
      template = '/emx_complete_e2e_template.txt'
    when Config['emx_e2e']['before_cutoff']
      template = '/emx_before_cutoff_e2e_template.txt'
    when Config['emx_e2e']['after_cutoff']
      template = '/emx_after_cutoff_e2e_template.txt'
    when Config['emx_e2e']['before_valuation']
      template = '/emx_before_valuation_e2e_template.txt'
    when Config['emx_e2e']['after_valuation']
      template = '/emx_after_valuation_e2e_template.txt'
    when Config['emx_e2e']['santandar']
      template = '/emx_santandar_e2e_template.txt'
    when Config['emx_e2e']['rejected']
      template = '/emx_rejected_e2e_template.txt'
    when Config['emx_e2e']['close_to_valuation']
      template = '/emx_close_to_valuation_e2e_template.txt'
    else
      raise Exception.new 'GET EMX TEMPLATE FAILED, please check CONFIG'
    end
    return template
  end

end