class KeycloakLoginPage < PageObject

  def admin_console_link
    BrowserObjectTwo.obj.a(href: 'admin/')
  end

  def username_field
    BrowserObjectTwo.obj.text_field(id: 'username')
  end

  def password_field
    BrowserObjectTwo.obj.text_field(id: 'password')
  end

  def login_button
    BrowserObjectTwo.obj.button(id: 'kc-login')
  end

end