require_relative '../../../components/platforms/page_object'

class SpeedTestPage < PageObject


  def header
    BrowserObject.obj.h3(text: /Speedtest by Ookla/)
  end
end

