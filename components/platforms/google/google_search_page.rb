require_relative '../../../components/platforms/page_object'

class GoogleSearchPage < PageObject

  def search_field
    BrowserObject.obj.text_field(title: 'Pesquisar')
  end
end