require_relative '../../../../utilities/csv_reader'

class TaBcpFormatReader < CsvReader

  # This method needs to be used immediately after downloading the intended CSV file
  def read_file_from_downloads
    # Get TA BCP csv format - 'TransactionDetailsTaBcpFormat_YYYYMMDD_HHMMSS.csv'
    # Only matches up to the Hour mark, to account for latency
    #BST OFFSET add 1 hour - 03/31/2020
    ta_bcp_file_format = 'TransactionDetailsTaBcpFormat_' + (Time.now.utc + (1 * 60 * 60)).strftime("%Y%m%d_%H") + "*.csv"

    #Search Downloads directory for TA BCP file format and parses the first file (should only be one)
    csv_file_path_arr = Dir[Config['file_path']['downloads'] + '/' + ta_bcp_file_format]

    #Error Handling
    if csv_file_path_arr.empty?
      HtmlReporter.add_step_details_no_ss('File Error', "No file found matching 'TransactionDetailsTaBcpFormat'", 'red')
      raise Exception.new 'TA BCP FILE error'
    end

    #Output CSV file name and parse it
    @csv_file_name = csv_file_path_arr[0].split('/').pop
    HtmlReporter.add_step_details_no_ss('File Name', @csv_file_name, 'green')
    @data_map = CSV.parse(File.read(csv_file_path_arr[0]), headers: true)
  end


  def validate_against(table_data)
    @data_map.each_with_index do |csv_row_data, index|
      HtmlReporter.add_step_header("Validating row #{index + 1}")
      validate csv_row_data['Message ID'], table_data[index]['trade_id'], 'Trade ID'
      validate_time_offset csv_row_data['Creation Date Time'], table_data[index]['chain_received_time'], 'Chain Received Time'
      validate csv_row_data['Agent Number'], table_data[index]['fund_manager'], 'Fund Manager'
      validate csv_row_data['Account Number'], table_data[index]['account_id'], 'Account ID'
      validate csv_row_data['Order Reference'], table_data[index]['order_ref'], 'Order Reference'
      validate csv_row_data['Instrument ISIN'], table_data[index]['isin'], 'ISIN'
      validate csv_row_data['Transaction Type'].upcase, table_data[index]['trade_type'].upcase, 'Trade Type'

      #CSV only shows UNIT/VALUE based on selection, respectively. If PRICED, check the "PRICED" columns
      if table_data[index]['value/unit'].upcase == 'UNIT'
        validate_units csv_row_data['Units'], table_data[index]['units'], 'Units' #Units is Nil?
      elsif table_data[index]['value/unit'].upcase == 'VALUE'
        if table_data[index]['status'] == 'Priced'
          #TODO: Validate the priced amount - need to grab from API or update pricing to match original amount
        else
          validate_amount csv_row_data['Amount'], table_data[index]['amount'], 'Amount'
          validate csv_row_data['Amount Currency'], table_data[index]['amount_currency'], 'Amount Currency'
        end

      else
        #do not validate
      end

      #Settlement Currency Conversion: OPBank - EUR | Others - GBP
      # if table_data[index]['fund_manager'] == 'OPFMC'
      #   validate csv_row_data['Settlement Currency'], 'EUR', 'Settlement Currency'
      # else
      #   validate csv_row_data['Settlement Currency'], 'GBP', 'Settlement Currency'
      # end
    end

    if @fail_flag == 'Y'
      raise Exception.new 'TaBcp file not aligned with Chain UI'
    end
  end

  def validate_opbank_euros
    HtmlReporter.add_step_header('Validating TABCP Euros')
    @data_map.each_with_index do |csv_row_data, index|
      HtmlReporter.add_step_header("TABCP Row #{index+1}")
      is_euros? csv_row_data['Amount Currency'], 'Amount Currency'
      is_euros? csv_row_data['Settlement Currency'], 'Settlement Currency'
    end
  end

  private

  def is_euros?(hash, key)
    if hash == 'EUR'
      HtmlReporter.add_step_passed_no_ss(key, hash)
      return
    else
      HtmlReporter.add_step_failed_no_ss(key, hash)
      raise Exception.new 'OPBANK has NON EUR currency'
    end

  end

end