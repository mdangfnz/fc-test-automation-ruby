require_relative '../../../../utilities/csv_reader'

class ChainClearDefaultReader < CsvReader

  # This method needs to be used immediately after downloading the intended CSV file
  def read_file_from_downloads
    # Get Chain Clear csv format. Only matches up to the Hour mark - to account for latency
    # 'TransactionDetailsChainClearDefault_YYYYMMDD_HHMMSS.csv'
    #BST OFFSET add 1 hour - 03/31/2020
    chain_clear_file_format = '/TransactionDetailsChainClearDefault_' + (Time.now.utc + (1 * 60 * 60)).strftime("%Y%m%d_%H") + "*.csv"

    #Search Downloads directory for TA BCP file format and parses the first file (should only be one)
    csv_file_path_arr = Dir[Config['file_path']['downloads'] + chain_clear_file_format]

    #Error Handling
    if csv_file_path_arr.empty?
      HtmlReporter.add_step_failed_no_ss('File Error', "No file found matching 'TransactionDetailsChainClearDefault'")
      raise Exception.new 'ChainClear FILE error'
    end

    #Output CSV file name and parse it
    @csv_file_name = csv_file_path_arr[0].split('/').pop
    HtmlReporter.add_step_details_no_ss('File Name', @csv_file_name, 'green')
    @data_map = CSV.parse(File.read(csv_file_path_arr[0]), headers: true)
  end


  def validate_against(table_data)
    @data_map.each_with_index do |csv_row_data, index|
      HtmlReporter.add_step_header("Validating row #{index + 1}")
      validate_status csv_row_data['Summary Status'], table_data[index]['status'], 'Status'
      #validate_time_offset csv_row_data['Processed Date Time'], table_row_data['ta_accepted_time']
      validate csv_row_data['Orders Chain Link ID'], table_data[index]['trade_id'], 'Trade ID'
      validate_time_offset csv_row_data['Order File Received by Chain'], table_data[index]['chain_received_time'], 'Chain Received Time'

      #if EMX - Agent Code, other sources - Fund Provider
      if csv_row_data['Transaction source'] == 'EMX'
        validate csv_row_data['Agent Code'], table_data[index]['fund_manager'], 'Fund Manager'
      else
        validate csv_row_data['Fund Provider'], table_data[index]['fund_manager'], 'Fund Manager'
      end

      validate csv_row_data['Orders Order Reference'], table_data[index]['order_ref'], 'Order Reference'
      validate csv_row_data['Orders Fund ID'], table_data[index]['isin'], 'ISIN'
      validate csv_row_data['Account Number'], table_data[index]['account_id'], 'Account ID'


      #CSV only shows UNIT/VALUE based on selection, respectively.
      #If trade is PRICED then check "PRICED" columns for BOTH UNIT and AMOUNT
      if csv_row_data['Summary Status'].include? 'CONFIRMATION'
        validate_units csv_row_data['Units'], table_data[index]['units'], 'Units'
        validate_amount csv_row_data['Gross Amount'], table_data[index]['amount'], 'Amount'
        validate csv_row_data['Gross Amount Currency'], table_data[index]['amount_currency'], 'Amount Currency'

        #Settlement Currency Conversion: OPBank - EUR | Others - GBP - ONLY CHECK IF PRICED
        if table_data[index]['fund_manager'] == 'OPFMC'
          validate csv_row_data['Settlement Amount Currency'], 'EUR', 'Settlement Currency'
        else
          validate csv_row_data['Settlement Amount Currency'], 'GBP', 'Settlement Currency'
        end
      else
        #Validate UNITS/AMOUNT depending on the initial Trade - DO NOT validate settlement columns
        if table_data[index]['value/unit'].upcase == 'UNIT'
          validate_units csv_row_data['Ordered Units'], table_data[index]['units'], 'Units'
        elsif table_data[index]['value/unit'].upcase == 'VALUE'
          validate_amount csv_row_data['Ordered Amount'], table_data[index]['amount'], 'Amount'
          validate csv_row_data['Ordered Amount Currency'], table_data[index]['amount_currency'], 'Amount Currency'
        else
          #do not validate
        end
      end

    end

    if @fail_flag == 'Y'
      raise Exception.new 'ChainClear file not aligned with Chain UI'
    end
  end

  def validate_rejected_reasons
    HtmlReporter.add_step_header('Validating Rejected Reasons')
    @data_map.each_with_index do |csv_row_data, index|
      unless csv_row_data['Rejected Reason'].nil?
        HtmlReporter.add_step_passed_no_ss("Rejected Reason #{index + 1}", csv_row_data['Rejected Reason'])
      else
        HtmlReporter.add_step_failed_no_ss("Rejected Reason #{index + 1}", 'blank')
        raise Exception.new "Rejected order has no rejected reason. Index: #{index + 1}"
      end
    end
  end

  def validate_opbank_euros
    HtmlReporter.add_step_header('Validating ChainClear CSV Euros')
    @data_map.each_with_index do |csv_row_data, index|
      HtmlReporter.add_step_header("ChainClear Row #{index+1}")
      HtmlReporter.add_step_passed_no_ss('Status', csv_row_data['Summary Status'])
      unless csv_row_data['Ordered Amount'].nil? or csv_row_data['Ordered Amount'] == ''
        is_euros? csv_row_data['Ordered Amount Currency'], 'Ordered Amount Currency'
      end
      is_euros? csv_row_data['Requested Settlement Currency'], 'Requested Settlement Currency'
      #TODO: OPBANK currently is not sending this as expected, issue on OPBANK side
      #is_euros? csv_row_data['Requested Pricing Currency'], 'Requested Pricing Currency'
      if csv_row_data['Summary Status'].include? 'CONFIRMATION'
        is_euros? csv_row_data['Net Amount Currency'], 'Requested Settlement Currency'
        is_euros? csv_row_data['Gross Amount Currency'], 'Gross Amount Currency'
        is_euros? csv_row_data['Price Amount Currency'], 'Price Amount Currency'
        is_euros? csv_row_data['Settlement Amount Currency'], 'Settlement Amount Currency'
      end
    end
  end

  def validate_rejected_reasons_emx(emx_order_string)
    HtmlReporter.add_step_header('Validating Rejected Reasons')
    @data_map.each_with_index do |csv_row_data, index|
      if csv_row_data['Orders Order Reference'].include? emx_order_string
        unless csv_row_data['Rejected Reason'].nil?
          HtmlReporter.add_step_passed_no_ss(csv_row_data['Orders Order Reference'], csv_row_data['Rejected Reason'])
        else
          HtmlReporter.add_step_failed_no_ss(csv_row_data['Orders Order Reference'], 'blank')
          raise Exception.new "Rejected order has no rejected reason. #{csv_row_data['Orders Order Reference']}"
        end
      end
    end
  end


  def validate_redemption_holding(order_string)
    HtmlReporter.add_step_header('Validating Holding Redemption')
    @data_map.each_with_index do |csv_row_data, index|
      if csv_row_data['Orders Order Reference'].include? order_string
        unless csv_row_data['Percentage Of Holdings To Be Redeemed'].nil?
          HtmlReporter.add_step_passed_no_ss("Redemption Holding #{index + 1}", csv_row_data['Percentage Of Holdings To Be Redeemed'])
        else
          HtmlReporter.add_step_failed_no_ss("Redemption Holding #{index + 1}", 'blank')
          raise Exception.new "Order has no Redemption Holding . Index: #{index + 1}"
        end
      end
    end
  end

  private

  def is_euros?(hash, key)
    if hash == 'EUR'
      HtmlReporter.add_step_passed_no_ss(key, hash)
      return
    else
      HtmlReporter.add_step_failed_no_ss(key, hash)
      raise Exception.new 'OPBANK has NON EUR currency'
    end

  end


end
