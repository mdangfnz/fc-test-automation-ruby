require_relative '../../../../components/platforms/page_object'

class FnzChainErrorPage < PageObject

  def error_rows
    BrowserObject.obj.divs(role: 'row')
  end

end
