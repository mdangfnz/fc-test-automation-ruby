require_relative '../../../../components/platforms/page_object'

class FnzChainSearchGrid < PageObject

  #click somewhere that won't impact the UI, to clear the dropbox checkboxes
  def clear_drop_box
    BrowserObject.obj.img(alt: 'Logo').click
    sleep 1
  end

  def fnz_logo_img
    BrowserObject.obj.img(alt: 'Logo')
  end


  def from_date_field
    BrowserObject.obj.text_field(placeholder: 'From date')
  end

  def to_date_field
    BrowserObject.obj.text_field(placeholder: 'To date')
  end

  def clear_button
    BrowserObject.obj.a(text: 'Clear')
  end

  def search_icon
    BrowserObject.obj.i(class: 'fa fa-search')
  end

  def top_left_section
    BrowserObject.obj.divs(class: 'panel graph_panel')
  end

  def bottom_left_section
    BrowserObject.obj.div(class: 'error_block')
  end

  def top_right_section
    BrowserObject.obj.divs(class: 'ReactTable')[0]
  end

  def bottom_right_section
    BrowserObject.obj.div(class: 'filter_block')
  end

  def select_ta_bcp_format_option
    #BrowserObject.obj.div(class: 'Dropdown-control').scroll.to
    BrowserObject.obj.div(class: 'Dropdown-control').click
    BrowserObject.obj.div(role: 'option', text: 'TA BCP format').click
    sleep 1
    HtmlReporter.add_step_passed_with_ss('CSV Format', 'TA BCP format')
  end

  def select_chain_clear_default_option
    #BrowserObject.obj.div(class: 'Dropdown-control').scroll.to
    sleep 1
    BrowserObject.obj.div(class: 'Dropdown-control').click
    sleep 1
    BrowserObject.obj.div(role: 'option', text: 'ChainClear default').click
    sleep 1
    HtmlReporter.add_step_passed_with_ss('CSV Format', 'ChainClear default')
  end

  def download_csv_button
    BrowserObject.obj.button(text: /Download CSV/)
  end

  def select_status_filter_option(status)
    BrowserObject.obj.span(text: 'Status').click
    sleep 1
    BrowserObject.obj.span(text: /#{status}/).click
    # HtmlReporter.add_step_passed_with_ss('Set Filter', status)
    self.clear_drop_box
    HtmlReporter.add_step_passed_with_ss('Set Status', status)
    sleep 2
  end

  def select_fund_manager_filter_option(fund_manager)
    BrowserObject.obj.span(text: 'Fund manager').click
    sleep 1
    BrowserObject.obj.span(text: /#{fund_manager}/).click
    self.clear_drop_box
    HtmlReporter.add_step_passed_with_ss('Set Fund Manager', fund_manager)
    sleep 2
  end

  def select_channel_filter_option(channel)
    case channel.upcase
    when 'EMX'
      channel = 'Emx'
    when 'FNZ ONE OPBANK'
      channel = 'Fnz one opbank'
    when 'FNZ CLEAR'
      channel = 'Fnz clear'
    else
      raise Exception.new 'Channel provided is invalid, check cucumber feature'
    end
    BrowserObject.obj.span(text: 'Channel').click
    sleep 1
    BrowserObject.obj.span(text: /#{channel}/).click
    self.clear_drop_box
    HtmlReporter.add_step_passed_with_ss('Set Channel', channel)
    sleep 2
  end

  def select_trade_type_filter_option(trade_type)
    BrowserObject.obj.span(text: 'Trade type').click
    sleep 1
    BrowserObject.obj.span(text: /#{trade_type}/).click
    self.clear_drop_box
    HtmlReporter.add_step_passed_with_ss('Set Trade Type', trade_type)
    sleep 2
  end

  def select_processing_system_filter_option(processing_system)
    BrowserObject.obj.span(text: 'Processing system').click
    sleep 1
    BrowserObject.obj.span(text: /#{processing_system}/).click
    self.clear_drop_box
    HtmlReporter.add_step_passed_with_ss('Set Processing system', processing_system)
    sleep 2
  end

  def select_isin_filter_option(isin)
    BrowserObject.obj.span(text: 'ISIN').click
    sleep 1
    BrowserObject.obj.span(text: /#{isin}/).click
    self.clear_drop_box
    HtmlReporter.add_step_passed_with_ss('Set ISIN', isin)
    sleep 2
  end

  def validate_dropdown_options(dropdown, options)
    passed = true
    BrowserObject.obj.span(text: dropdown).scroll.to :center
    sleep 1
    BrowserObject.obj.span(text: dropdown).click
    sleep 1
    options.each do |status|
      if BrowserObject.obj.span(text: /#{status}/).exist?
        HtmlReporter.add_step_passed_no_ss("#{dropdown} Shows", status)
      else
        HtmlReporter.add_step_failed_no_ss("#{dropdown} does not show", status)
        passed = false
      end
    end

    if passed == true
      HtmlReporter.add_step_passed_with_ss("All #{dropdown} values displayed", 'Pass Test')
    else
      HtmlReporter.add_step_failed_with_ss("#{dropdown} dropdown missing values", 'Fail Test')
      raise Exception.new "#{dropdown} dropdown missing values"
    end
  end



  def select_status_tab (status)
    BrowserObject.obj.a(class: 'tabs', text: /#{status}/).click
    sleep 5
    HtmlReporter.add_step_passed_with_ss('Filter Status by tab', status)
  end

  def select_records_option (count)
    BrowserObject.obj.select.select /#{count}/
    sleep 5
    BrowserObject.obj.select.scroll.to
    HtmlReporter.add_step_passed_with_ss( 'Set Records per page', count)
  end

  def order_ref_field
    BrowserObject.obj.text_field(placeholder: 'Search for Order Reference')
  end

  def clear_filter_link
    BrowserObject.obj.a(text: 'Clear filter')
  end




end