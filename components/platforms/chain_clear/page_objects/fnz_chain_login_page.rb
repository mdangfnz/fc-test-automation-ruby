require_relative '../../../../components/platforms/page_object'

class FnzChainLoginPage < PageObject


  def username_field
    BrowserObject.obj.text_field(id: 'username')
  end

  def password_field
    BrowserObject.obj.text_field(id: 'password')
  end

  def login_button
    BrowserObject.obj.button(id: 'kc-login')
  end

  def error_message
    BrowserObject.obj.span(class: 'kc-feedback-text')
  end

  def fnz_logo
    BrowserObject.obj.div(class: 'kc-logo-text')
  end

  def support_message
    BrowserObject.obj.p(class: 'msg_txt')
  end
end
