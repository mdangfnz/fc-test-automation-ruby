require_relative '../../../../components/platforms/page_object'

class FnzChainChangePwdPage < PageObject

  def change_pwd_span
    BrowserObject.obj.span(text: 'Change Password')
  end

  def password_field
    BrowserObject.obj.text_field(id: 'password')
  end

  def new_password_field
    BrowserObject.obj.text_field(id: 'password-new')
  end

  def confirm_password_field
    BrowserObject.obj.text_field(id: 'password-confirm')
  end

  def submit_button
    BrowserObject.obj.button(name: 'submitAction')
  end

  def error_message
    BrowserObject.obj.span(class: 'pficon pficon-error-exclamation')
  end
end