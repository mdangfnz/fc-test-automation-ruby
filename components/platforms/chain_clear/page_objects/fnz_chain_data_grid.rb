require_relative '../../../../components/platforms/page_object'

class FnzChainDataGrid < PageObject

  def initialize
    @table_data_map = []
  end
  #This method stores the data in the table into an array of hashes
  #ex: @table_data_map[0]['order_ref'], returns the value of the first row's order reference
  def set_table_data_map
    #TODO: Delete this comment after code is completed
    #BrowserObject.obj.divs(class: 'ReactTable')[1].div(class: 'rt-tbody').divs(class: 'rt-tr-group')[1].divs(role: 'gridcell')[4].text


    #Get all rows of the table
    rows_arr = BrowserObject.obj.divs(class: 'ReactTable')[1].div(class: 'rt-tbody').divs(class: 'rt-tr-group')
    rows_arr.each do |column_data_arr|

      #Get all columns of the row and create a new hash
      row_hash = {}
      data_arr = column_data_arr.divs(role: 'gridcell')

      #Return if chain_received_time is empty
      if data_arr[0].nil? or data_arr[0].text== ' ' or data_arr[0] == ''
        return @table_data_map
      end

      #Store the data of each column into row_hash
      data_arr.each_with_index do |column_data, index|

        add_column_to_hash(row_hash, index, column_data)
      end

      #Add hash into @table_data_map
      @table_data_map.push row_hash
    end
  end

  #Return @table_data_map for test validations
  def get_table_data_map
    raise Exception.new 'Data Table is empty' if @table_data_map.empty?
    @table_data_map
  end

  def get_unique_statuses
    status_arr = []
    @table_data_map.each do |row|
      unless status_arr.include? row['status']
        status_arr << row['status']
      end
    end

    return status_arr
  end

  def get_unique_channels
    status_arr = []
    @table_data_map.each do |row|
      unless status_arr.include? row['channel']
        status_arr << row['channel']
      end
    end

    return status_arr
  end

  def get_unique_processing_systems
    status_arr = []
    @table_data_map.each do |row|
      unless status_arr.include? row['processing_system']
        status_arr << row['processing_system']
      end
    end

    return status_arr
  end

  def get_unique_isins
    status_arr = []
    @table_data_map.each do |row|
      unless status_arr.include? row['isin']
        status_arr << row['isin']
      end
    end

    return status_arr
  end

  def get_unique_fund_managers
    status_arr = []
    @table_data_map.each do |row|
      unless status_arr.include? row['fund_manager']
        status_arr << row['fund_manager']
      end
    end

    return status_arr
  end

  def get_unique_trade_types
    status_arr = []
    @table_data_map.each do |row|
      unless status_arr.include? row['trade_type']
        status_arr << row['trade_type']
      end
    end

    return status_arr
  end

  def capture_table_data
    BrowserObject.obj.scroll.to :bottom
    HtmlReporter.add_step_passed_with_ss "Capture Table data", 'Successful'

    #Scroll to the right of the table and capture a screenshot
    BrowserObject.obj.divs(class: 'rt-table', role: 'grid')[1].click
    40.times do
      BrowserObject.obj.send_keys :arrow_right
    end
    HtmlReporter.add_step_passed_with_ss "Capture Table data pt2", 'Successful'
  end

  def table_rows
    BrowserObject.obj.divs(class: 'panel')[4].divs(class: %w['rt-tr', '-odd'])
  end

  def received_time_sort
    BrowserObject.obj.span(text: 'Chain received time')
  end

  def isin_sort
    BrowserObject.obj.span(class: 'text-left show-pointer', text: 'ISIN')
  end

  def total_page_count
    BrowserObject.obj.span(class: '-totalPages')
  end

  def next_page_button
    BrowserObject.obj.button(text: '>')
  end

  def capture_table_data_ss
    BrowserObject.obj.scroll.to :bottom
    HtmlReporter.add_step_passed_with_ss "Capture Table data", 'Successful'

    #Scroll to the right of the table and capture a screenshot
    BrowserObject.obj.divs(class: 'rt-table', role: 'grid')[1].click
    40.times do
      BrowserObject.obj.send_keys :arrow_right
    end
    HtmlReporter.add_step_passed_with_ss "Capture Table data pt2", 'Successful'
  end

  def total_row_count
    BrowserObject.obj.span(class: '-rowCount')
  end

  private

  def add_column_to_hash(hash, key_index, value)
    # Updated as of 17/03/2020 - Update code if table format changes
    case key_index
    when 0
      hash['chain_received_time'] = value.text
    when 1
      hash['ta_accepted_time'] = value.text
    when 2
      hash['trade_date'] = value.text
    when 3
      hash['order_ref'] = value.text
    when 4
      hash['trade_id'] = value.text
    when 5
      hash['channel'] = value.text
    when 6
      hash['account_id'] = value.text
    when 7
      hash['fund_manager'] = value.text
    when 8
      hash['isin'] = value.text
    when 9
      hash['amount_currency'] = value.text
    when 10
      hash['trade_type'] = value.text
    when 11
      hash['value/unit'] = value.text
    when 12
      hash['amount'] = value.text
    when 13
      hash['units'] = value.text
    when 14
      hash['status'] = value.text
    when 15
      hash['processing_system'] = value.text
    when 16
      hash['settlement_date'] = value.text
    end
  end

end