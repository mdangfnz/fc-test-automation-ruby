require_relative '../../../../components/platforms/page_object'

class FnzChainDisplaySections < PageObject

  def see_error_details_link
    BrowserObject.obj.a(text: 'See Details')
  end

  def error_count_display
    BrowserObject.obj.h4(class: 'red_txt')
  end
end