class DatabaseBase

  include ReportHelper

  def initialize
    puts "\n\n############################# Database Validation ####################################\n\n"
  end

  def action
    raise Exception.new 'Need implementation'
  end

  def login
    
  end

  def logout

  end

  def error_close_all(message)
    puts message
    ReportHelper.report_step_failed_no_screenshot('Error', message)
  end
end