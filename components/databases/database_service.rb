require 'java'
require 'jdbc/mysql'
require_relative '../../utilities/config'

# TODO: Import DB Drivers
# java_import 'oracle.jdbc.OracleDriver'
# java_import 'java.sql.DriverManager'
# java_import 'java.sql.ResultSetMetaData'
# java_import 'com.amazon.redshift.jdbc.Driver'

#include ResultSetMetaData

def resultset_to_hash(resultset)
  meta = resultset.meta_data
  rows = []

  while resultset.next
    row = {}

    (1..meta.column_count).each do |i|
      name = meta.getColumnName i
      row[name] = resultset.get_string(i).to_s
    end

    rows << row
  end
  rows
end

module DatabaseService


end