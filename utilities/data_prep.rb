module DataPrep

  def self.initialize
    @data_prep_stores = []
    @index = 0
    data_prep_store = {}
    @data_prep_stores << data_prep_store
  end

  def self.clear
    @data_prep_stores = []
  end

  def self.row(index)
    @index = index
  end

  def self.row_count
    @data_prep_stores.length
  end

  def self.[](key)
    @data_prep_stores[@index][key]
  end

  def self.[]=(key, value)
    @data_prep_stores[@index][key] = value
  end


  def self.store(key, value)
    @data_prep_stores[@index][key] = value
  end

  def self.store_with_default(key, value, default)
    if value.nil? or value.empty?
      @data_prep_stores[@index][key] = default
    else
      @data_prep_stores[@index][key] = value
    end
  end

  def self.hash
    return @data_prep_stores[@index]
  end
end