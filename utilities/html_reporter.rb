require 'fileutils'
require_relative 'browser_object'
require_relative 'config'

#TODO: Implement HTML reporting module

module HtmlReporter

  def self.initialize(scenario_name)

    scenario_name = scenario_name.name
    @scenario_name = scenario_name
    puts @scenario_name

    #:TODO Consider UTC time for html paths
    #HTML directory paths
    base_dir = Config['file_path']['html_dir']
    date_dir = base_dir + "/#{Time.now.strftime('%Y-%m-%d')}"
    env_dir = date_dir + "/#{Config['env']}"
    start_run_time = Time.now.strftime('%H-%M-%S')
    scenario_dir = env_dir + "/#{scenario_name}"
    scenario_start_time_dir = scenario_dir + "/#{start_run_time}"
    @screenshot_dir = scenario_start_time_dir + '/screenshots'
    @archive_dir = scenario_start_time_dir + '/archive'
    Config['file_path']['archives'] = @archive_dir #rebuild archive path based on scenario

    #:TODO Handle window paths
    #Creates HTML directory paths
    FileUtils.mkdir_p base_dir
    FileUtils.mkdir_p date_dir
    FileUtils.mkdir_p env_dir
    FileUtils.mkdir_p scenario_dir
    FileUtils.mkdir_p scenario_start_time_dir
    FileUtils.mkdir_p @screenshot_dir
    FileUtils.mkdir_p @archive_dir
    FileUtils.mkdir_p Config['emx_e2e']['base']
    FileUtils.mkdir_p Config['emx_e2e']['base']

    #Creates HTML file
    html_file_name = 'TestResults.html'
    puts '*************** HTML FILE NAME *******************' + html_file_name
    @html_file_path = scenario_start_time_dir + "/#{html_file_name}"

    build_template
  end

  def self.add_step_header(header)
    begin
      file = File.open(@html_file_path, 'a')
      file.write("<div class=\"datagrid\"><table>"\
                    "<thead><tr><th width=\"16%\">Step</th>"\
                    "<th width=\"68%\">Detail</th><th width=\"8%\">Result</th><th "\
                    "width=\"8%\">Screenshot</th></tr></thead><tr><td colspan=\"5\" bgcolor =\"LightSteelBlue \" style=\"font-weight:bold\">" + "Header: #{header}" + "</tr>")
    rescue IOError => e
      puts e.message
    ensure
      file.close unless file.nil?
    end
  end



  def self.close
    begin
      file = File.open(@html_file_path, 'a')
      file.write("</table></div>")
      file.write("</body>")
      file.write("</html>")
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end

  def self.add_step_passed_with_ss(step_name, step_detail)
    puts "** PASS ** Step Name: #{step_name}  -  Step Detail: #{step_detail}"
    add_step_details_with_ss(step_name,step_detail,'green')
  end

  def self.add_step_failed_with_ss(step_name, step_detail)
    puts "** ERROR ** Step Name: #{step_name}  -  Step Detail: #{step_detail}"
    add_step_details_with_ss(step_name, step_detail, 'red')
  end

  def self.add_step_passed_no_ss(step_name, step_detail)
    puts "** PASS ** Step Name: #{step_name}  -  Step Detail: #{step_detail}"
    add_step_details_no_ss(step_name, step_detail, 'green')
  end

  def self.add_step_failed_no_ss(step_name, step_detail)
    puts "** ERROR ** Step Name: #{step_name}  -  Step Detail: #{step_detail}"
    add_step_details_no_ss(step_name, step_detail,'red')
  end

  def self.add_step_passed_with_ss_bo2(step_name, step_detail)
    puts "** PASS ** Step Name: #{step_name}  -  Step Detail: #{step_detail}"
    add_step_details_with_ss_bo2(step_name,step_detail,'green')
  end

  def self.add_step_failed_with_ss_bo2(step_name, step_detail)
    puts "** ERROR ** Step Name: #{step_name}  -  Step Detail: #{step_detail}"
    add_step_details_with_ss_bo2(step_name, step_detail, 'red')
  end

  def self.add_step_with_file(step_name, file_name)
    link_to_file = 'archive/' + file_name

    begin
      file = File.open(@html_file_path, 'a')
      file.write(" <tr height=20 style='height:15.0pt'><td bgcolor=\"Wheat\">" + step_name + "</td> <td><a href=\"#{link_to_file}\"> Click here </a></td><td bgcolor=\" green  \"</td><td></td></tr>")
      file.write("</body>")
      file.write("</html>")
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end


  private

  def self.build_template
    begin
      file = File.open(@html_file_path, 'w')
      #Create the Report header
      file.write("<html\n")
      file.write("<head><title>TestName</title>\n")
      file.write("<style>\n")
      file.write(".datagrid table { border: 1px solid #8C8C8C; border-collapse: collapse; text-align: left; width: 100%; } \n")
      file.write(".datagrid {font: normal 12px/150% Verdana, Arial, Helvetica, sans-serif; background: #fff; \n")
      file.write("overflow: hidden; border: 1px solid #8C8C8C; -webkit-border-radius: 3px; -moz-border-radius: \n")
      file.write("3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid \n")
      file.write("table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #8C8C8C), \n")
      file.write("color-stop(1, #7D7D7D) );background:-moz-linear-gradient( center top, #8C8C8C 5%, #7D7D7D  100% );\n")
      file.write("filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#8C8C8C', endColorstr='#7D7D7D');\n")
      file.write("background-color:#8C8C8C; color:#FFFFFF; font-size: 15px; font-weight: bold; border-left: 1px \n")
      file.write("solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td \n")
      file.write("{ color: #00496B; border-left: 1px solid #E1EEF4;font-size: 12px;font-weight: normal;border-bottom: 1px solid #8C8C8C }.datagrid \n")
      file.write("table tbody .alt td { background: #E1EEF4; color: #00496B; }.datagrid table tbody td:first-child \n")
      file.write("{ border-left: none; }.datagrid table tbody tr:last-child td { border: 1px solid #8C8C8C; }.datagrid \n")
      file.write("table tfoot td div { border-top: 1px solid #8C8C8C;background: #E1EEF4;} .datagrid table tfoot td \n")
      file.write("{ padding: 0; font-size: 12px } .datagrid table tfoot td div{ padding: 2px; }.datagrid table tfoot \n")
      file.write("td ul { margin: 0; padding:0; list-style: none; text-align: right; }.datagrid table tfoot  \n")
      file.write("li { display: inline; }.datagrid table tfoot li a { text-decoration: none; display: inline-block;  \n")
      file.write("padding: 2px 8px; margin: 1px;color: #FFFFFF;border: 1px solid #8C8C8C;-webkit-border-radius: \n")
      file.write("3px; -moz-border-radius: 3px; border-radius: 3px; background:-webkit-gradient( linear, left top, \n")
      file.write("left bottom, color-stop(0.05, #8C8C8C), color-stop(1, #7D7D7D) );background:-moz-linear-gradient( \n")
      file.write("center top, #8C8C8C 5%, #7D7D7D 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#8C8C8C', \n")
      file.write("endColorstr='#7D7D7D');background-color:#8C8C8C; }.datagrid table tfoot ul.active, .datagrid table tfoot \n")
      file.write("ul a:hover { text-decoration: none;border-color: #8C8C8C; color: #FFFFFF; background: none; \n")
      file.write("background-color:#7D7D7D;}div.dhtmlx_window_active, div.dhx_modal_cover_dv { position: fixed !important; }\n")
      file.write("</style>\n")
      file.write("</head>\n")
      file.write("<body>\n")
      file.write("\n")
      file.write("\n")
      file.write("\n")
      file.write("<div class=\"datagrid\"><table><thead><tr><th>Test Results</th></tr>\n")
      file.write("</thead><tr height=20 style='height:15.0pt'><tr><td>Scenario: " + @scenario_name + "</td></tr><tr><td>Environment: " + Config['env'] + "</td></tr><tr><td>Date/Time: " + Time.now.strftime('%m/%d/%Y %H:%M:%S') + "</td></tr>\n")
      file.write("</table>\n")
      file.write("</div><p></p>\n")
    ensure
      file.close unless file.nil?
    end
  end

  def self.add_step_details_with_ss(step_name, step_detail, cell_color)
    begin
      screenshot_file = Time.now.strftime('%H%M%S') + ".png"
      strFilePath = @screenshot_dir + "/" + screenshot_file
      file = File.open(@html_file_path, 'a')
      BrowserObject.obj.screenshot.save strFilePath
      absoluteFilePath = "file:#{strFilePath}"
      relativeFilePath = "screenshots/#{screenshot_file}"

      file.write(" <tr height=20 style='height:15.0pt'><td bgcolor=\"Wheat\">" + step_name + "</td> <td>" + step_detail + "</td> <td  bgcolor=\" #{cell_color}  \"</td><td><a href=\"#{relativeFilePath}\" target=\"new\"><img src=\"#{relativeFilePath}\" width=48 height=30</td></tr>")
      file.write("</body>")
      file.write("</html>")
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end

  def self.add_step_details_no_ss(step_name, step_detail, cell_color)
    begin
      file = File.open(@html_file_path, 'a')
      file.write(" <tr height=20 style='height:15.0pt'><td bgcolor=\"Wheat\">" + step_name + "</td> <td>" + step_detail + "</td><td bgcolor=\" #{cell_color}  \"</td><td></td></tr>")
      file.write("</body>")
      file.write("</html>")
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end

  def self.add_step_details_with_ss_bo2(step_name, step_detail, cell_color)
    begin
      screenshot_file = Time.now.strftime('%H%M%S') + ".png"
      strFilePath = @screenshot_dir + "/" + screenshot_file
      file = File.open(@html_file_path, 'a')
      BrowserObjectTwo.obj.screenshot.save strFilePath
      absoluteFilePath = "file:#{strFilePath}"
      relativeFilePath = "screenshots/#{screenshot_file}"

      file.write(" <tr height=20 style='height:15.0pt'><td bgcolor=\"Wheat\">" + step_name + "</td> <td>" + step_detail + "</td> <td  bgcolor=\" #{cell_color}  \"</td><td><a href=\"#{relativeFilePath}\" target=\"new\"><img src=\"#{relativeFilePath}\" width=48 height=30</td></tr>")
      file.write("</body>")
      file.write("</html>")
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end

  def self.add_step_details_no_ss(step_name, step_detail, cell_color)
    begin
      file = File.open(@html_file_path, 'a')
      file.write(" <tr height=20 style='height:15.0pt'><td bgcolor=\"Wheat\">" + step_name + "</td> <td>" + step_detail + "</td><td bgcolor=\" #{cell_color}  \"</td><td></td></tr>")
      file.write("</body>")
      file.write("</html>")
    rescue IOError => e
      #some error occur, dir not writable etc.
    ensure
      file.close unless file.nil?
    end
  end


  #<a href=\"#{absoluteFilePath}\"



end