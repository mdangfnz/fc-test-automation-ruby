module BrowserObjectTwo

  #Loads the specified browser, with desired capabilities
  def self.load(browser)

    case browser.upcase
    when 'CHROME'
      prefs = {
          download: {
              default_directory: Config['file_path']['downloads'],
              prompt_for_download: false
          },
          profile: {
              # 1 = Allow popups, 2 = Block popups
              default_content_setting_values: {popups: 1}
          }
      }

      puts RbConfig::CONFIG['host_os']
      if RbConfig::CONFIG['host_os'].include? 'linux'
        args = ['--headless']
      else
        args = []
      end
      client = Selenium::WebDriver::Remote::Http::Default.new
      client.read_timeout = 120 #4mins

      @browser = Watir::Browser.new :chrome, options: {prefs: prefs, args: args, client: client}
    when 'FIREFOX'
      firefox_profile = Selenium::WebDriver::Firefox::Profile.new
      firefox_profile['browser.download.folderList'] = 2 #allows custom path
      firefox_profile['browser.download.dir'] = Config['file_path']['downloads']
      firefox_profile['browser.helperApps.neverAsk.saveToDisk'] = 'text/csv,application/pdf'
      args = ['--always-authorize-plugins']

      #:TODO desired_capabilities depreciated
      # firefox_caps = Selenium::WebDriver::Remote::Capabilities.firefox(
      #     firefoxOptions: {
      #         args: ['--always-authorize-plugins'],
      #         prefs: firefox_profile
      #     }
      # )

      if RbConfig::CONFIG['host_os'].include? 'linux'
        headless_flag = true
      else
        headless_flag = false
      end

      client = Selenium::WebDriver::Remote::Http::Default.new
      client.read_timeout = 120
      @browser = Watir::Browser.new :firefox, profile: firefox_profile, args: args, headless: headless_flag, client: client

    when 'SAFARI'
      @browser = Watir::Browser.new :safari, technology_preview: true

    when 'IE'
      @browser = Watir::Browser.new :ie #, options:
    end

    @browser.cookies.clear
    #@browser.window.resize_to(1600, 1200)
    @browser.window.maximize
    @browser.name
    Watir.default_timeout = 120 #increase to 2 mins
  end

  #return the Browser object
  def self.obj
    attempts = 0
    begin
      @browser
    rescue Net::ReadTimeout => e
      if attempts == 0
        attempts += 1
        retry
      else
        raise
      end
    end
  end

  def self
    @browser
  end

  def self.goto_url(url)
    attempts = 0 # has to be outside the begin/rescue to avoid infinite loop
    begin
      @browser.goto url
      puts 'goto url attempt' + (attempts + 1).to_s
      puts 'browser state = ' + @browser.ready_state
    rescue Net::ReadTimeout => e
      if attempts < 3
        puts 'caught READTIMEOUT - retrying goto URL'
        sleep 5
        attempts += 1
        retry
      else
        raise Exception.new 'Goto URL failed'
      end
    end
  end

  def self.action_timeout_retry(action)
    attempts = 0 # has to be outside the begin/rescue to avoid infinite loop
    action
    begin
      @browser.refresh
      puts 'action attempt' + (attempts + 1).to_s
      puts 'browser state = ' + @browser.ready_state
    rescue Net::ReadTimeout => e
      if attempts < 3
        puts 'caught READTIMEOUT - retrying action'
        sleep 60
        attempts += 1
        retry
      else
        raise Exception.new 'Action Retry Failed'
      end
    end

  end

  def self.chain_ui_login_retry
    attempts = 0 # has to be outside the begin/rescue to avoid infinite loop

    begin
      @browser.button(id: 'kc-login').click
    rescue
    end
    #perform login click
    begin
      puts 'click login attempt' + (attempts + 1).to_s
      puts 'browser state = ' + @browser.ready_state
    rescue
      if attempts < 3
        puts 'caught READTIMEOUT - retrying login click'
        sleep 60
        attempts += 1
        retry
      else
        raise Exception.new 'Click login Retry Failed'
      end
    end

    #check chain UI has loaded
    begin

      puts 'chain UI load attempt' + (attempts + 1).to_s
      @browser.img(alt: 'Logo').wait_until(&:present?)
    rescue
      if attempts < 3
        puts 'caught READTIMEOUT - retrying CHAIN UI load'
        sleep 60
        attempts += 1
        retry
      else
        raise Exception.new 'Login Retry Failed'
      end
    end
  end

end