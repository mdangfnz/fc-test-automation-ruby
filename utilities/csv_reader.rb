require 'csv'
require 'fileutils'
require 'date'

class CsvReader


  def initialize
    #These instance variables get set by the child
    @data_map = []
  end

  def read_file_from_downloads
    raise Exception.new "Method needs to be implemented in the child class"
  end

  def archive_csv
    FileUtils.move("#{Config['file_path']['downloads']}/#{@csv_file_name}", "#{Config['file_path']['archives']}/#{@csv_file_name}")
    HtmlReporter.add_step_with_file('CSV FILE', @csv_file_name)
  end

  def read_and_archive_file
    sleep 5
    self.read_file_from_downloads
    self.archive_csv
  end

  def validate(data1, data2, column)
    if self.data_has_value?(data1, data2, column)
      puts 'wait'
      if data1.to_s.upcase == data2.to_s.upcase
        HtmlReporter.add_step_passed_no_ss(column, "CSV: #{data1} | Chain UI: #{data2}")
      else
        HtmlReporter.add_step_failed_no_ss(column, "CSV: #{data1} | Chain UI: #{data2}")
        @fail_flag = 'Y'
      end
    end

  end

  #data1 = csv, data2=chainUI. Add Offset to data2, and compare time with a threshold of +/- 60seconds to account for latency
  def validate_time_offset(csv_data, table_data, column)
    if self.data_has_value?(csv_data, table_data, column)

      #This if statement is to account that Chain UI does not convert to BST time, it still displays UTC+0
      if Time.now.zone == 'BST'
        csv_data = DateTime.parse(csv_data) #This is GMT time
        table_data_offset = DateTime.parse(table_data)

        #The difference is in days. You need the fractional of 3 minute of a day
        if (csv_data - table_data_offset) >= (-3.0/ 24/60) and (csv_data - table_data_offset) <= (3.0/24/60)
          HtmlReporter.add_step_passed_no_ss("#{column}(+/- 60s)", "CSV: #{csv_data} | Chain UI: #{table_data_offset}")
        else
          HtmlReporter.add_step_failed_no_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data_offset}")
          @fail_flag = 'Y'
        end
      else
        csv_data = DateTime.parse(csv_data) #This is GMT time
        table_data_offset = DateTime.parse(table_data) - (Time.now.gmt_offset.to_f / 3600 / 24) #gmt_offset gives returns seconds, convert it to days (x / 60 / 60 / 24) + (60*60 for DST)

        #The difference is in days. You need the fractional of 3 minute of a day
        if (csv_data - (table_data_offset + (1.0/24))) >= (-3.0 / 24 / 60) and (csv_data - (table_data_offset + (1.0/24))) <= (3.0 / 24 / 60)
          HtmlReporter.add_step_passed_no_ss("#{column}(+/- 60s)", "CSV: #{csv_data} | Chain UI: #{table_data_offset}")
        else
          HtmlReporter.add_step_failed_no_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data_offset}")
          @fail_flag = 'Y'
        end
      end

    end
  end

  #Normalize amount: Extract commas and compare
  def validate_amount(csv_data, table_data, column)
    if self.data_has_value?(csv_data, table_data, column)
      data1_converted = csv_data.gsub(',', '').to_f.to_s

      data2_converted = table_data.gsub(',', '').to_f.to_s

      if (data1_converted == data2_converted)
        HtmlReporter.add_step_passed_no_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data}")
      else
        HtmlReporter.add_step_failed_no_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data}")
        @fail_flag = 'Y'
      end
    end
  end

  #round csv data to the 2nd decimal place
  def validate_units(csv_data, table_data, column)
    if self.data_has_value?(csv_data, table_data, column)
      csv_data_rounded = csv_data.gsub(',', '').to_f.round(2).to_s
      table_data_rounded = table_data.gsub(',', '').to_f.round(2).to_s

      if (csv_data_rounded == table_data_rounded)
        HtmlReporter.add_step_passed_no_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data}")
      else
        HtmlReporter.add_step_failed_no_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data}")
        @fail_flag = 'Y'
      end
    end
  end

  #:TODO Add mapping
  def validate_status(csv_data, table_data, column)
    HtmlReporter.add_step_passed_with_ss(column, "CSV: #{csv_data} | Chain UI: #{table_data}")


    # when 'TRANSACTION_CONFIRMATION_RECEIVED_BY_CHAIN'
    # when 'REJECTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR'
    # when 'REJECTED_STATUS_REPORT_RECEIVED_BY_CHAIN'
    # when 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR'
    # when 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_CHAIN'
    # when 'TRANSACTION_RECEIVED_BY_TA'
    # when 'TRANSACTION_RECEIVED_BY_CHAIN'
    # end
  end

  def validate_fund_manager(csv_data, table_data, column)

  end

  def validate_count(table_data)
    if table_data.size == @data_map.size
      HtmlReporter.add_step_passed_no_ss('Trade counts match', "CSV: #{@data_map.size} | Chain UI: #{table_data.size}")
    else
      HtmlReporter.add_step_passed_no_ss('Trade counts mismatch', "CSV: #{@data_map.size} | Chain UI: #{table_data.size}")
      raise Exception.new 'Trade count mismatch between Chain UI and CSV'
    end
  end


  def data_has_value?(csv_data, table_data, column)
    if csv_data.nil? or table_data.nil?
      if csv_data == table_data or (csv_data.nil? and table_data == '--') or (csv_data.nil? and table_data == '')
        HtmlReporter.add_step_passed_no_ss(column, "CSV: blank  | CHAIN UI: blank")
      elsif csv_data.nil?
        HtmlReporter.add_step_failed_no_ss(column, "CSV: blank  | CHAIN UI: #{table_data}")
        @fail_flag = 'Y'
      elsif table_data.nil?
        HtmlReporter.add_step_failed_no_ss(column, "CSV: #{csv_data} | CHAIN UI: blank")
        @fail_fail = 'Y'
      end
      return false
    end
    return true
  end


  #GETTERS

  def file_name
    @csv_file_name
  end

  def csv_data_map
    @data_map
  end

end