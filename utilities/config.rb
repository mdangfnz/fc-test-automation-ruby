require 'yaml'
require 'java'
require 'jdbc/mysql'

java_import 'java.sql.DriverManager'
java_import 'java.sql.ResultSetMetaData'

include ResultSetMetaData

module Config
  def self.load_env(env)

    case env.upcase
    when 'UAT'
      @yaml_path = '../config/uat_config.yaml'
    when 'BENCHMARK'
      @yaml_path = '../config/benchmark_config.yaml'
    end
    @props = YAML::load_file(File.join(__dir__, @yaml_path))

    #Build File Paths based on Computer username
    @props['file_path']['downloads'] = @props['file_path']['downloads'].sub(/{{.*}}/, ENV['USER'])
    @props['file_path']['archives'] = @props['file_path']['archives'].sub(/{{.*}}/, ENV['USER'])
    @props['file_path']['html_dir'] = @props['file_path']['html_dir'].sub(/{{.*}}/, ENV['USER'])
    @props['emx_e2e']['base'] = @props['emx_e2e']['base'].sub(/{{.*}}/, ENV['USER'])
    @props['auto_data_path'] = @props['auto_data_path'].sub(/{{.*}}/, ENV['USER'])
  end

  def self.[](key)
    @props[key]
  end

  def self.[]=(key, value)
    @props[key] = value
  end

  def self.set_emx_scenario(scenario)
    case scenario.upcase
    when 'BEFORE VALUATION'
      active_file = Config['emx_e2e']['before_valuation']
    when 'AFTER VALUATION'
      active_file = Config['emx_e2e']['after_valuation']
    when 'BEFORE CUTOFF'
      active_file = Config['emx_e2e']['before_cutoff']
    when 'AFTER CUTOFF'
      active_file = Config['emx_e2e']['after_cutoff']
    when 'REJECTED'
      active_file = Config['emx_e2e']['rejected']
    when 'COMPLETE E2E'
      active_file = Config['emx_e2e']['complete_e2e']
    when 'SANTANDAR'
      active_file = Config['emx_e2e']['santandar']
    when 'CLOSE TO VALUATION'
      active_file = Config['emx_e2e']['close_to_valuation']
    else
      raise Exception.new 'SET EMX SCENARIO input is invalid, please check'
    end
    @props['emx_e2e']['active_file'] = active_file
  end
end