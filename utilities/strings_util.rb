module StringsUtil

  def self.get_feature_name(feature)
    begin
      feature_arr = feature_name.split("/")
      feature_name = feature_arr[feature_arr.length-1]
      return feature_name
    end
  end

  def self.generate_code(number)
    charset = Array('A'..'Z')
    Array.new(number) { charset.sample }.join
  end

end