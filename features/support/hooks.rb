Before do |scenario|
  Config.load_env('UAT')
  HtmlReporter.initialize scenario
  HtmlReporter.add_step_header scenario.name
  DataPrep.initialize
  puts 'Starting Scenario'
end

After do |scenario|
  HtmlReporter.close
end

After('@keycloak') do
  BrowserObjectTwo.obj.close
end

Before('@api_test') do
  puts 'Starting API Test'
end

Before('@ui_test')do
  BrowserObject.load'Chrome'
end

Before('@ui_test_firefox') do
  BrowserObject.load 'FireFox'
end

Before('@ui_test_safari') do
  BrowserObject.load 'Safari'
end

After('@ui_test') do
  BrowserObject.obj.close
end

Before('@emx_e2e_1') do
  Config.set_emx_scenario('BEFORE CUTOFF')
end

Before('@emx_e2e_2') do
  Config.set_emx_scenario('AFTER CUTOFF')
end

Before('@emx_e2e_3') do
  Config.set_emx_scenario('BEFORE VALUATION')
end

Before('@emx_e2e_4') do
  Config.set_emx_scenario('AFTER VALUATION')
end

Before('@emx_e2e_5') do
  Config.set_emx_scenario('SANTANDAR')
end

Before('@emx_e2e_0') do
  Config.set_emx_scenario('COMPLETE e2e')
end

Before('@emx_e2e_8') do
  Config.set_emx_scenario('CLOSE TO VALUATION')
end

Before('@emx_e2e_rejected') do
  Config.set_emx_scenario('rejected')
end





