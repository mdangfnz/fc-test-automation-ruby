When(/^I get ws gateway messages by chain ID$/) do
  @gateway_get_message_ws = GetGatewayMessageById.new
  @gateway_get_message_ws.send @chain_id
end

Then(/^the ws gateway shows my retry message as RECONSUMED$/) do
  @gateway_get_message_ws.validate_reconsumed
end