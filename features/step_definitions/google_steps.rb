When(/^I goto google$/)do
  BrowserObject.obj.goto("https://www.google.com/")
end

When(/^I search for speed test$/)do
  @google_page = GoogleSearchPage.new
  @google_page.search_field.set('speed test')
  BrowserObject.obj.send_keys :enter
end

Then(/^I find Speed test by Ookla$/)do
  speed_test_page = SpeedTestPage.new
  speed_test_page.header.wait_until(&:present?)
  expect(speed_test_page.header.exists?).to eql true
  HtmlReporter.add_step_passed_with_ss('Speed Test', 'loaded')

end