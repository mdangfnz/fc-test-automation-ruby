Given(/^User is testing with (.*)$/)do |browser_type|
  BrowserObject.load(browser_type)
end

When(/^I wait '(.*)' seconds$/) do |time|
  sleep time.to_i
end


When(/^I wait '(.*)' seconds and capture time$/) do |time|
  HtmlReporter.add_step_passed_no_ss('Time Now', Time.now.to_s)
  sleep time.to_i
  HtmlReporter.add_step_passed_no_ss('Time Now', Time.now.to_s)
end
