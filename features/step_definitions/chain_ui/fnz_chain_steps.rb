Given(/^User logged into FNZ Chain UI$/) do

  #instantiate all necessary objects
  @login_page = FnzChainLoginPage.new
  @search_grid = FnzChainSearchGrid.new
  @data_grid = FnzChainDataGrid.new
  @ta_bcp_file = TaBcpFormatReader.new
  @chain_clear_file = ChainClearDefaultReader.new
  @display_sections = FnzChainDisplaySections.new
  @error_page = FnzChainErrorPage.new


  BrowserObject.goto_url "https://chainlink.demo.fnzchain.com/#/app/home"
  @login_page.username_field.set Config['chain_ui']['username']
  sleep 1
  @login_page.password_field.set Config['chain_ui']['password']
  sleep 1
  BrowserObject.chain_ui_login_retry
end

Given(/^User logged into FNZ Chain UI to change pwd$/) do

  #instantiate all necessary objects
  @login_page = FnzChainLoginPage.new
  @search_grid = FnzChainSearchGrid.new
  @data_grid = FnzChainDataGrid.new
  @ta_bcp_file = TaBcpFormatReader.new
  @chain_clear_file = ChainClearDefaultReader.new
  @display_sections = FnzChainDisplaySections.new
  @error_page = FnzChainErrorPage.new

  pw = Config['chain_ui']['change_pwd_pwd'] + File.read(Config['auto_data_path'] + Config['chain_ui']['change_pwd_file']).to_s
  BrowserObject.goto_url "https://chainlink.demo.fnzchain.com/#/app/home"
  @login_page.username_field.set Config['chain_ui']['change_pwd_usr']
  sleep 1
  @login_page.password_field.set pw
  sleep 1
  BrowserObject.chain_ui_login_retry
end

When(/^the dates are within a 7 day range$/) do

end

When(/^I goto Chain UI login page$/) do
  #instantiate all necessary objects
  @login_page = FnzChainLoginPage.new
  BrowserObject.goto_url Config['chain_ui']['url']
  HtmlReporter.add_step_passed_with_ss('URL', BrowserObject.obj.url)
end

When(/^I login with keycloak temp password$/) do
  pwd_num = File.read(Config['auto_data_path'] + Config['chain_ui']['reset_pwd_file'])
  pwd = Config['chain_ui']['reset_pwd_pwd'] + pwd_num
  @login_page.username_field.set Config['chain_ui']['reset_pwd_usr']
  @login_page.password_field.set pwd
  @login_page.login_button.click
end

Then(/^Chain UI url redirects to keycloak$/) do
  #instantiate all necessary objects
  if BrowserObject.obj.url.include? 'keycloak'
    HtmlReporter.add_step_passed_with_ss('Redirect URL', BrowserObject.obj.url)
  else
    HtmlReporter.add_step_failed_with_ss('Redirect URL', BrowserObject.obj.url)
    raise Exception.new 'Redirect did not happen'
  end
end

When(/^User is prompt to update password$/) do
  #add 1 to update to a new password
  file_full_path = Config['auto_data_path'] + Config['chain_ui']['reset_pwd_file']
  pwd_num = File.read(file_full_path)
  pwd_num = (pwd_num.to_i + 1).to_s
  pwd = Config['chain_ui']['reset_pwd_pwd'] + pwd_num
  sleep 5

  unless BrowserObject.obj.h1(id: 'kc-page-title').text.include? 'Update'
    HtmlReporter.add_step_failed_with_ss('Update Password Required?', 'Failed')
    raise Exception.new 'not required to update password'
  end
  HtmlReporter.add_step_passed_with_ss('Required to update password', 'Successful')
  BrowserObject.obj.text_field(id: 'password-new').set pwd
  BrowserObject.obj.text_field(id: 'password-confirm').set pwd
  BrowserObject.obj.button(value: 'Submit').click
  HtmlReporter.add_step_passed_no_ss('New Password', pwd)


  #write new password to file
  file_obj = File.open(file_full_path, 'w')
  file_obj.write(pwd_num)
  file_obj.close
end


When(/^User sets the format to (.*) and click download$/) do |csv_format|
  sleep 10
  case csv_format.upcase
  when 'TABCPFORMAT'
    @search_grid.select_ta_bcp_format_option
    @search_grid.download_csv_button.click
    @ta_bcp_file.read_and_archive_file
  when 'CHAINCLEARDEFAULT'
    @search_grid.select_chain_clear_default_option #TODO:map chainclear option
    @search_grid.download_csv_button.click
    @chain_clear_file.read_and_archive_file
  else
    raise Exception.new 'Invalid option, use TABCPFORMAT or CHAINCLEARDEFAULT'
  end
end

Then(/^User can see Chain UI$/) do
  @search_grid = FnzChainSearchGrid.new
  @search_grid.fnz_logo_img.wait_until(&:present?)
  raise Exception.new 'FNZ Chain Clear dashboard did not appear' unless @search_grid.fnz_logo_img.exists?
  sleep 5
  HtmlReporter.add_step_passed_with_ss('Chain UI Dashboard', 'Successful')
end


Then(/^the trade will appear '(.*)' in Chain UI as count '(.*)'$/) do |status, count|
  HtmlReporter.add_step_header('Validate in Chain UI')

  from_date = DateTime.parse(@orders[0]['creation_date']).strftime('%d/%m/%Y') + ' 00:00'
  to_date = DateTime.parse(@orders[0]['creation_date']).strftime('%d/%m/%Y') + ' 23:59'
  step 'User logged into FNZ Chain UI'
  step "User sets the 'from' date to #{from_date}"
  step "User sets the 'to' date to #{to_date}"
  step 'User clicks the search icon'
  step "User searches by order reference '#{@orders[0]['order_ref']}'"
  step "the count of records should match '#{count.to_s}'"
  step 'the trade data will display in the table'
  step "only '#{status}' status orders are displayed"
end

Then(/^the trade will appear '(.*)' in Chain UI as count '(.*)' today$/) do |status, count|
  HtmlReporter.add_step_header('Validate in Chain UI')

  step 'User logged into FNZ Chain UI'
  step "User searches by order reference '#{@orders[0]['order_ref']}'"
  step "the count of records should match '#{count.to_s}'"
  step 'the trade data will display in the table'
  step "only '#{status}' status orders are displayed"
end

Then(/^the default date range is 3 days back$/) do
  if @search_grid.from_date_field.value.include? (Time.now - 60 * 60 * 24 * 2).strftime('%d/%m/%Y')
    if @search_grid.to_date_field.value.include? Time.now.strftime('%d/%m/%Y')
      HtmlReporter.add_step_passed_with_ss('Default Timeframe', '3 days - Test Passed')
    else
      HtmlReporter.add_step_failed_with_ss('Default Timeframe', 'Not 3 days - Test Failed')
      raise Exception.new 'Default timeframe not 3 days'
    end
  end
end

#TODO: refactor with POM
Then(/^the pagination is correct$/) do
  BrowserObject.obj.span(class: '-totalPages').text
  record_count = BrowserObject.obj.span(class: '-rowCount').text.to_i
  BrowserObject.obj.scroll.to :bottom
  page_count = record_count / 50
  if record_count % 50 > 0
    page_count = page_count + 1
  end
  if BrowserObject.obj.span(class: '-totalPages').text == page_count.to_s
    HtmlReporter.add_step_passed_with_ss('Page Count Correct', "Records: #{record_count.to_s} | Pages: #{page_count.to_s}")
  else
    HtmlReporter.add_step_failed_with_ss('Page Count Incorrect', "Records: #{record_count.to_s} | Pages: #{page_count.to_s}")
    raise Exception.new 'Page count incorrect'
  end
end

#TODO: refactor with POM
Then(/^the donut chart will display total orders$/) do
  puts 'wait'
  sleep 5
  if BrowserObject.obj.h3(class: 'h3_heading').text.include? BrowserObject.obj.span(class: '-rowCount').text
    HtmlReporter.add_step_passed_with_ss('Donut Chart matches total trade', BrowserObject.obj.span(class: '-rowCount').text)
    if BrowserObject.obj.g(aria_label: 'Received').exist?
      received_count = BrowserObject.obj.g(aria_label: 'Received').text.split("\n")[1]
      if (BrowserObject.obj.div(class: 'col-lg-6 table_status_wrapper').lis[1].a.text).include? received_count
        HtmlReporter.add_step_passed_with_ss('Received matches', received_count)
      else
        HtmlReporter.add_step_failed_with_ss('Received mismatch', 'Fail Test')
      end
    end
    if BrowserObject.obj.g(aria_label: 'Accepted').exist?
      accepted_count = BrowserObject.obj.g(aria_label: 'Accepted').text.split("\n")[1]
      if (BrowserObject.obj.div(class: 'col-lg-6 table_status_wrapper').lis[2].a.text).include? accepted_count
        HtmlReporter.add_step_passed_with_ss('Accepted matches', accepted_count)
      else
        HtmlReporter.add_step_failed_with_ss('Accepted mismatch', 'Fail Test')
      end
    end
    if BrowserObject.obj.g(aria_label: 'Rejected').exist?
      rejected_count = BrowserObject.obj.g(aria_label: 'Rejected').text.split("\n")[1]
      if (BrowserObject.obj.div(class: 'col-lg-6 table_status_wrapper').lis[3].a.text).include? rejected_count
        HtmlReporter.add_step_passed_with_ss('Rejected matches', rejected_count)
      else
        HtmlReporter.add_step_failed_with_ss('Rejected mismatch', 'Fail Test')
      end
    end
    if BrowserObject.obj.g(aria_label: 'Priced').exist?
      priced_count = BrowserObject.obj.g(aria_label: 'Priced').text.split("\n")[1]
      if (BrowserObject.obj.div(class: 'col-lg-6 table_status_wrapper').lis[4].a.text).include? priced_count
        HtmlReporter.add_step_passed_with_ss('Priced matches', priced_count)
      else
        HtmlReporter.add_step_failed_with_ss('Priced mismatch', 'Fail Test')
      end
    end
    if BrowserObject.obj.g(aria_label: 'Terminated').exist?
      terminated_count = BrowserObject.obj.g(aria_label: 'Terminated').text.split("\n")[1]
      if (BrowserObject.obj.div(class: 'col-lg-6 table_status_wrapper').lis[6].a.text).include? terminated_count
        HtmlReporter.add_step_passed_with_ss('Terminated matches', terminated_count)
      else
        HtmlReporter.add_step_failed_with_ss('Terminated mismatch', 'Fail Test')
      end
    end
  else
    HtmlReporter.add_step_failed_with_ss('Donut Mismatch', 'Fail test')
    raise Exception.new 'Donut mismatched'
  end
end

Then(/^the status tabs are updated$/)do
  sleep 5
  unless BrowserObject.obj.b(text: 'All').parent.text.include? BrowserObject.obj.h3(class: 'h3_heading').text[0..2]
    HtmlReporter.add_step_passed_with_ss('Status Tabs Updated', 'Test Passed')
  else
    HtmlReporter.add_step_failed_with_ss('Status Tabs Updated', 'Test Failed')
    raise Exception.new 'Status Tabs not updated'
  end
end

Then(/^the channel section is not updated$/)do
  puts ''
end



