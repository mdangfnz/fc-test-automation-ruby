#format '15/03/2020 00:00'
When(/^User sets the 'from' date to (.*)$/) do |from_datetime|
  @from_datetime = from_datetime

  18.times do
    @search_grid.from_date_field.send_keys :backspace
  end

  @search_grid.from_date_field.send_keys from_datetime
  @search_grid.from_date_field.send_keys :enter
  @search_grid.clear_drop_box
  HtmlReporter.add_step_passed_with_ss "Set 'from' date", from_datetime
end

#format '15/03/2020 00:00'
When(/^User sets the 'to' date to (.*)$/) do |to_datetime|
  @to_datetime = to_datetime
  18.times do
    @search_grid.to_date_field.send_keys :backspace
  end

  @search_grid.to_date_field.send_keys to_datetime
  @search_grid.from_date_field.send_keys :enter
  @search_grid.clear_drop_box
  HtmlReporter.add_step_passed_with_ss "Set 'to' date", to_datetime
end

#format '15/03/2020 00:00'
When(/^User sets the date to today$/) do
  from_datetime = DateTime.now.strftime("%d/%m/%Y") + " 00:00"
  to_datetime = DateTime.now.strftime("%d/%m/%Y") + " 23:59"

  step "User sets the 'from' date to #{from_datetime}"
  step "User sets the 'to' date to #{to_datetime}"
  step 'User clicks the search icon'
end


When(/^User filters by '(.*)' status$/) do |status|
  #TODO: Capture Filter option dropdown and select by status
  @search_grid.select_status_filter_option status
end

When(/^User clicks the search icon$/) do
  @search_grid.search_icon.click
  sleep 5
end


When(/^User filters by '(.*)' fund manager$/) do |fund_manager|
  @search_grid.select_fund_manager_filter_option fund_manager
end

When(/^User filters by '(.*)' channel$/) do |channel|
  @search_grid.select_channel_filter_option channel
end

When(/^User filters by '(.*)' processing system$/) do |processing_system|
  @search_grid.select_processing_system_filter_option processing_system
end

When(/^User filters by '(.*)' isin$/) do |isin|
  @search_grid.select_isin_filter_option isin
end

When(/^User filters by '(.*)' trade type$/) do |trade_type|
  @search_grid.select_trade_type_filter_option trade_type
end

When(/^User clicks '(.*)' status tab$/) do |status|
  @search_grid.select_status_tab status
end

When(/^User sets records displayed to '(.*)'$/) do |count|
  @search_grid.select_records_option count
end

When(/^User searches by order reference '(.*)'$/)do |order_ref|
  @search_grid.order_ref_field.set order_ref
  sleep 6
  HtmlReporter.add_step_passed_with_ss 'Search Order Ref', order_ref
end

When(/^User searches for emx orders uploaded by file$/)do
  emx_order_string = File.read(Config['emx_e2e']['base'] + Config['emx_e2e']['active_file'])
  step "User searches by order reference '#{emx_order_string}'"
end

When(/^User clicks clear filter$/) do
  @search_grid.clear_filter_link.click
  sleep 2
  @search_grid.clear_filter_link.scroll.to :center
  sleep 8
  HtmlReporter.add_step_passed_with_ss('Clear Filter', 'Clicked')
end



