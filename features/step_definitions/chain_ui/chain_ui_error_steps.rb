Then(/^the error will be displayed in Chain UI$/) do
  step "User logged into FNZ Chain UI"

  HtmlReporter.add_step_header('Display Error in Chain UI')
  @display_sections.see_error_details_link.click
  sleep 10
  error_rows = @error_page.error_rows

  error_rows.each_with_index do |row, count|
    if count < 20
      if row.span(text: @trace_id).exist?
        row.span(text: @trace_id).focus
        HtmlReporter.add_step_passed_with_ss('Trace ID', @trace_id)
        HtmlReporter.add_step_passed_with_ss('Error Found', row.divs[1].text)
        break
      elsif row.span(text: @error_id).exist?
        row.span(text: @error_id).focus
        HtmlReporter.add_step_passed_with_ss('Chain Error ID', @error_id)
        HtmlReporter.add_step_passed_with_ss('Error Found', row.divs[1].text)
        break
      end
    else
      HtmlReporter.add_step_failed_with_ss('Trace ID Not found', @trace_id)
      HtmlReporter.add_step_failed_with_ss('Error not in Chain UI', 'Fail')
      raise Exception.new 'Error not Found in Chain'
    end
  end
end

Then(/^the payload will be displayed in Chain UI$/) do
  error_rows = @error_page.error_rows
  error_rows.each_with_index do |row, count|
    if count < 20
      if row.span(text: @trace_id).exist?
        row.span(text: @trace_id).focus
        row.span(text: "View").click
        sleep 2
        if BrowserObject.obj.div(class: "clearfix").exist?
          HtmlReporter.add_step_passed_with_ss("Display Payload", 'Test Passed')
        else
          HtmlReporter.add_step_failed_with_ss("Display Payload", 'Test Failed')
        end
        break
      end
    end
  end
end


Then(/^the total errors is correctly displayed in Chain UI$/) do
  HtmlReporter.add_step_header('Validate in Chain UI')
  step "User logged into FNZ Chain UI"
  sleep 10
  chain_ui_error_count = @display_sections.error_count_display.text

  if chain_ui_error_count == @error_count.to_s
    HtmlReporter.add_step_passed_with_ss('Chain UI Error Count Passed', "Expected: #{@error_count.to_s} | Actual: #{chain_ui_error_count}")
  else
    HtmlReporter.add_step_failed_with_ss('Chain UI Error Count Failed', "Expected: #{@error_count.to_s} | Actual: #{chain_ui_error_count}")
    raise Exception.new 'Chain UI error count not aligned with API'
  end
end

Then(/^the error is correctly displayed in Chain UI$/) do
  HtmlReporter.add_step_header('Validate in Chain UI')
  step "User logged into FNZ Chain UI"
  sleep 10
  chain_ui_error_count = @display_sections.error_count_display.text

  if chain_ui_error_count == @error_count.to_s
    HtmlReporter.add_step_passed_with_ss('Chain UI Error Count Passed', "Expected: #{@error_count.to_s} | Actual: #{chain_ui_error_count}")
  else
    HtmlReporter.add_step_failed_with_ss('Chain UI Error Count Failed', "Expected: #{@error_count.to_s} | Actual: #{chain_ui_error_count}")
    raise Exception.new 'Chain UI error count not aligned with API'
  end
end