# ******************
# ****** WHEN ******
# ******************

When(/^I put my username$/) do
  @login_page.username_field.set Config['chain_ui']['username']
end

When(/^I login with a user with no role$/) do
  @login_page.username_field.set 'test_user_no_role'
  @login_page.password_field.set 'Tester11'
  @login_page.login_button.click
end

When(/^I fail login 3 times$/) do
  3.times do |count|
    @login_page.username_field.set Config['chain_ui']['username']
    @login_page.password_field.set 'FailTest'
    @login_page.login_button.click
    sleep 3

    error = @login_page.error_message
    if error.exist?
      HtmlReporter.add_step_passed_with_ss("Attempt #{(count+1).to_s}", error.text)
    else
      HtmlReporter.add_step_failed_with_ss("Attempt #{(count+1).to_s}", 'Error not displayed')
    end
  end
end

When(/^I put my password$/) do
  @login_page.password_field.set Config['chain_ui']['password']
end

When(/^I click login$/) do
  @login_page.login_button.click
end

When(/^User logs out of Chain UI$/)do
  BrowserObject.obj.img(alt: 'logout').click
  HtmlReporter.add_step_passed_with_ss('Click Logout', 'Successful')
end

# ******************
# ****** THEN ******
# ******************

Then(/^I see 'fill out this field'$/) do
  sleep 2
  HtmlReporter.add_step_passed_with_ss('Access Denied', 'Fill out this field appears')
end

Then(/^Chain UI will display missing role error$/)do
  if @login_page.error_message.text == 'User requires the web user role to login'
    HtmlReporter.add_step_passed_with_ss('Chain UI - Keycloak error',@login_page.error_message.text )
  else
    HtmlReporter.add_step_failed_with_ss('Missing Chain UI - Keycloak Error', 'Fail Test')
    raise Exception.new 'missing role assigned error is missing'
  end
end

Then(/^my user will be locked$/) do
  error = @login_page.error_message
  if error.text == 'Your account has been locked. Please contact FNZ Chain admin for support.'
    HtmlReporter.add_step_passed_with_ss("User Locked", error.text)
  else
    HtmlReporter.add_step_failed_with_ss("Wrong error displayed", error.text)
    raise Exception.new 'Lock user failed'
  end
end

Then(/^login screen has necessary components$/) do
  @login_page.fnz_logo.wait_until(&:present?)
  if @login_page.fnz_logo.exist? and @login_page.support_message.exist? and @login_page.username_field.exist?
    if @login_page.password_field.exist? and @login_page.login_button.exist?
      HtmlReporter.add_step_passed_with_ss('Chain UI Login Screen', 'All components displayed')
    else
      HtmlReporter.add_step_failed_with_ss('Chain UI Login Screen', 'Missing compoonents')
      raise Exception.new 'Chain UI missing components'
    end
  else
    HtmlReporter.add_step_failed_with_ss('Chain UI Login Screen', 'Missing compoonents')
    raise Exception.new 'Chain UI missing components'
  end
end

Then(/^Chain UI redirected to login$/) do
  step 'login screen has necessary components'
end

