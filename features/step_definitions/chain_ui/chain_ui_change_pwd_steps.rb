# ******************
# ****** WHEN ******
# ******************

When(/^User clicks on change password$/) do
  @change_pwd_page = FnzChainChangePwdPage.new
  @change_pwd_page.change_pwd_span.click
end

When(/^User puts new password without an uppercase$/) do
  puts 'wait'
  new_password = 'test1234'
  @change_pwd_page.password_field.set Config['chain_ui']['password']
  @change_pwd_page.new_password_field.set new_password
  @change_pwd_page.confirm_password_field.set new_password
  HtmlReporter.add_step_passed_with_ss('Set new password', new_password)
  sleep 2
  @change_pwd_page.submit_button.click
end

When(/^User puts valid new password$/) do
  puts 'wait'
  original_pwd_num = File.read(Config['auto_data_path'] + Config['chain_ui']['change_pwd_file']).to_i
  new_pwd_num = original_pwd_num + 1
  new_password = Config['chain_ui']['change_pwd_pwd'] + new_pwd_num.to_s
  original_password = Config['chain_ui']['change_pwd_pwd'] + original_pwd_num.to_s
  @change_pwd_page.password_field.set original_password
  @change_pwd_page.new_password_field.set new_password
  @change_pwd_page.confirm_password_field.set new_password
  HtmlReporter.add_step_passed_with_ss('Set new password', new_password)
  sleep 2
  @change_pwd_page.submit_button.click
  HtmlReporter.add_step_passed_with_ss'Set new password', 'Confirmed'

  #write new password to file
  file_obj = File.open(Config['chain_ui']['change_pwd_file'], 'w')
  file_obj.write(new_pwd_num.to_s)
  storage_file_name = Config['chain_ui']['change_pwd_file']
  FileUtils.move(storage_file_name, Config['auto_data_path'] + Config['chain_ui']['change_pwd_file'])
  file_obj.close
end

When(/^User puts new password without 2 numbers$/) do
  puts 'wait'
  new_password = 'TestTest'
  @change_pwd_page.password_field.set Config['chain_ui']['password']
  @change_pwd_page.new_password_field.set new_password
  @change_pwd_page.confirm_password_field.set new_password
  HtmlReporter.add_step_passed_with_ss('Set new password', new_password)
  sleep 2
  @change_pwd_page.submit_button.click
end

When(/^User puts new password with a special character$/) do
  puts 'wait'
  new_password = 'Test123@'
  @change_pwd_page.password_field.set Config['chain_ui']['password']
  @change_pwd_page.new_password_field.set new_password
  @change_pwd_page.confirm_password_field.set new_password
  HtmlReporter.add_step_passed_with_ss('Set new password', new_password)
  sleep 2
  @change_pwd_page.submit_button.click
end

When(/^User puts new password less than 8 characters$/) do
  puts 'wait'
  new_password = 'Test123'
  sleep 2
  @change_pwd_page.password_field.set Config['chain_ui']['password']
  @change_pwd_page.new_password_field.set new_password
  @change_pwd_page.confirm_password_field.set new_password
  HtmlReporter.add_step_passed_with_ss('Set new password', new_password)
  sleep 2
  @change_pwd_page.submit_button.click
end

When(/^User puts previously used password$/) do
  puts 'wait'
  new_password = Config['chain_ui']['password']
  sleep 2
  @change_pwd_page.password_field.set Config['chain_ui']['password']
  @change_pwd_page.new_password_field.set new_password
  @change_pwd_page.confirm_password_field.set new_password
  #HtmlReporter.add_step_passed_with_ss('Set new password', new_password)
  sleep 2
  @change_pwd_page.submit_button.click
end

# ******************
# ****** THEN ******
# ******************

Then(/^New password will display an error$/) do
  sleep 2
  if @change_pwd_page.error_message.exists?
    HtmlReporter.add_step_passed_with_ss('Password Error Found', @change_pwd_page.error_message.text)
  else
    HtmlReporter.add_step_failed_with_ss('Password Error Not Found', 'Password without a capital was accepted')
    raise Exception.new 'Update password rules are not working'
  end
end