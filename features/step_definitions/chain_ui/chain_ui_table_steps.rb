# ******************
# ****** WHEN ******
# ******************

When(/^User sorts by received time$/) do
  puts 'wait'

  #Get unsorted received time
  unsort_time_arr = []
  table_rows = @data_grid.table_rows
  table_rows.each do |row|
    receive_time = row.divs(class: %w['rt-td', 'text-center'])[0]
    unsort_time_arr << receive_time.span.text
  end

  #click sort asc
  sleep 1
  @data_grid.received_time_sort.scroll.to :center
  @data_grid.received_time_sort.click
  sleep 10
  @data_grid.received_time_sort.scroll.to :top
  sort_asc_time_arr = []

  #get sorted asc receive time
  table_rows = @data_grid.table_rows
  table_rows.each do |row|
    receive_time = row.divs(class: %w['rt-td', 'text-center'])[0]
    sort_asc_time_arr << receive_time.span.text
  end

  if unsort_time_arr.sort == sort_asc_time_arr
    HtmlReporter.add_step_passed_with_ss('Receive Time Sort Asc', 'Successful')
  else
    HtmlReporter.add_step_failed_with_ss('Receive Time Sort Asc', 'Failed')
    raise Exception.new 'Receive Time Sort Asc Failed'
  end

  #click sort desc
  sleep 1
  @data_grid.received_time_sort.scroll.to :center
  @data_grid.received_time_sort.click
  sleep 10
  @data_grid.received_time_sort.scroll.to :top
  sort_desc_time_arr = []

  #get sorted desc receive time
  table_rows = @data_grid.table_rows
  table_rows.each do |row|
    receive_time = row.divs(class: %w['rt-td', 'text-center'])[0]
    sort_desc_time_arr << receive_time.span.text
  end

  if unsort_time_arr.sort.reverse == sort_desc_time_arr
    HtmlReporter.add_step_passed_with_ss('Receive Time Sort Desc', 'Successful')
  else
    HtmlReporter.add_step_failed_with_ss('Receive Time Sort Desc', 'Failed')
    raise Exception.new 'Receive Time Sort Desc Failed'
  end
end

When(/^User sorts by ISIN$/) do

  #Get unsorted isin
  unsort_time_arr = []
  table_rows = @data_grid.table_rows
  table_rows.each do |row|
    isin = row.divs(class: %w['rt-td', 'text-left'])[4]
    unsort_time_arr << isin.span.text
  end

  #click sort asc
  sleep 1
  @data_grid.isin_sort.scroll.to :center
  @data_grid.isin_sort.click
  sleep 10
  @data_grid.isin_sort.scroll.to :top
  sort_asc_time_arr = []

  #get sorted asc isin
  table_rows = @data_grid.table_rows
  table_rows.each do |row|
    isin = row.divs(class: %w['rt-td', 'text-left'])[4]
    sort_asc_time_arr << isin.span.text
  end

  if unsort_time_arr.sort == sort_asc_time_arr
    HtmlReporter.add_step_passed_with_ss('ISIN Sort Asc', 'Successful')
  else
    HtmlReporter.add_step_failed_with_ss('ISIN Sort Asc', 'Failed')
  end

  #click sort desc
  sleep 1
  @data_grid.isin_sort.scroll.to :center
  @data_grid.isin_sort.click
  sleep 10
  @data_grid.isin_sort.scroll.to :top
  sort_desc_time_arr = []

  #get sorted desc isin
  table_rows = @data_grid.table_rows
  table_rows.each do |row|
    isin = row.divs(class: %w['rt-td', 'text-left'])[4]
    sort_desc_time_arr << isin.span.text
  end

  if unsort_time_arr.sort.reverse == sort_desc_time_arr
    HtmlReporter.add_step_passed_with_ss('ISIN Sort Desc', 'Successful')
  else
    HtmlReporter.add_step_failed_with_ss('ISIN Sort Desc', 'Failed')
  end
end

# ******************
# ****** THEN ******
# ******************

Then(/^the trade data will display euros for opbank$/) do
  HtmlReporter.add_step_header('Validating Chain UI table')
  table_data = @data_grid.get_table_data_map
  table_data.each_with_index do |row, row_count|
    HtmlReporter.add_step_passed_no_ss("Row #{row_count+1} - Value or Units", row['value/unit'])
    # EUR will only show for "VALUE" amount type trades, otherwise BLANK.
    if row['value/unit'] == 'value' or row['status'] == 'Priced'
      if row['amount_currency'] == 'EUR'
        HtmlReporter.add_step_passed_no_ss("Amount Currency row #{row_count+1}", row['amount_currency'])
      else
        HtmlReporter.add_step_failed_no_ss("Amount Currency row #{row_count+1}", row['amount_currency'])
        raise Exception.new 'Chain UI not showing euros for opbank'
      end
    else
      if row['amount_currency'] == '' or row['amount_currency'].nil?
        HtmlReporter.add_step_passed_no_ss("Amount Currency row #{row_count+1}", 'BLANK')
      else
        HtmlReporter.add_step_failed_no_ss("Amount Currency row #{row_count+1}", row['amount_currency'])
        raise Exception.new 'Chain UI not showing euros for opbank'
      end
    end
  end
end

Then(/^the trade data will display in the table$/) do
  sleep 30
  page_count = @data_grid.total_page_count.text.to_i
  (page_count).times do |count|
    @data_grid.set_table_data_map
    unless count == (page_count - 1)
      if BrowserObject.obj.div(class: 'rt-noData', text: 'No Orders').exist?
        break
      end
      @data_grid.next_page_button.send_keys :enter

      sleep 10
    end
  end

  @data_grid.get_table_data_map
  @data_grid.capture_table_data_ss
end

Then(/^the trade data displayed is within the date range$/) do
  table_data = @data_grid.get_table_data_map

  @to_date = DateTime.parse(@to_datetime)
  @from_date = DateTime.parse(@from_datetime)
  @failed = 'N'
  table_data.each_with_index do |row, row_count|
    datetime = DateTime.parse(row['chain_received_time'])
    unless datetime.between? @from_date, @to_date
      HtmlReporter.add_step_failed_no_ss "Invalid date-time", row_count + 1
      @failed = 'Y'
    end
  end

  raise Exception.new 'Invalid date found' if @failed == 'Y'

  HtmlReporter.add_step_passed_with_ss "Validate Date Range", 'Successful'
end

Then(/^the (.*) file matches the table's data$/) do |csv_format|
  table_data = @data_grid.get_table_data_map

  case csv_format.upcase
  when 'TABCPFORMAT'
    @ta_bcp_file.validate_against(table_data)
  when 'CHAINCLEARDEFAULT'
    @chain_clear_file.validate_against(table_data)
  else
    raise Exception.new 'INVALID OPTION, use TABCPFORMAT or CHAINCLEARDEFAULT'
  end
end

Then(/^the '(.*)' file count matches Chain UI$/) do |csv_format|
  table_data = @data_grid.get_table_data_map

  case csv_format.upcase
  when 'TABCPFORMAT'
    @ta_bcp_file.validate_count(table_data)
  when 'CHAINCLEARDEFAULT'
    @chain_clear_file.validate_count(table_data)
  else
    raise Exception.new 'INVALID OPTION, use TABCPFORMAT or CHAINCLEARDEFAULT'
  end
end


Then(/^only '(.*)' status orders are displayed$/) do |status|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['status'].upcase == status.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{status}'")
      raise Exception.new "Filter did not set return only '#{status}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{status}")
end

Then(/^User gets the trade count displayed$/) do
  @data_grid.total_row_count.scroll.to :bottom
  @trade_count = @data_grid.total_row_count.text
  HtmlReporter.add_step_passed_with_ss('Trade Count', @trade_count)
end

Then(/^the trade count has been updated$/) do
  @data_grid.total_row_count.scroll.to :bottom
  new_trade_count = @data_grid.total_row_count.text

  if @trade_count == new_trade_count
    HtmlReporter.add_step_failed_with_ss('Trade Count', 'Failed - trade count is the same')
    raise Exception.new 'Clear filter failed'
  end
  HtmlReporter.add_step_passed_with_ss('Trade Count', 'Passed - trade count different')
end

Then(/^only '(.*)' type orders are displayed$/) do |type|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['trade_type'].upcase == type.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{type}'")
      raise Exception.new "Filter did not set return only '#{type}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{type}")
end

Then(/^only '(.*)' channel orders are displayed$/) do |channel|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['channel'].upcase == channel.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{channel}'")
      raise Exception.new "Filter did not set return only '#{channel}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{channel}")
end

Then(/^only '(.*)' processing system orders are displayed$/) do |processing_system|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['processing_system'].upcase == processing_system.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{processing_system}'")
      raise Exception.new "Filter did not set return only '#{processing_system}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{processing_system}")
end

Then(/^only '(.*)' order reference is displayed$/) do |order_ref|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['order_ref'].upcase == order_ref.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{order_ref}'")
      raise Exception.new "Filter did not set return only '#{order_ref}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{order_ref}")
end

Then(/^only '(.*)' ISIN orders are displayed$/) do |isin|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['isin'].upcase == isin.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{isin}'")
      raise Exception.new "Filter did not set return only '#{isin}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{isin}")
end

Then(/^only '(.*)' fund manager orders are displayed$/) do |fund_manager|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['fund_manager'].upcase == fund_manager.upcase
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{fund_manager}'")
      raise Exception.new "Filter did not set return only '#{fund_manager}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{fund_manager}")
end

Then(/^the count of records should match '(.*)'$/) do |count|
  total_row_count = @data_grid.total_row_count.text

  if total_row_count == count.to_s
    HtmlReporter.add_step_passed_with_ss('Data Validation', "Expected: #{count.to_s} orders | Actual: #{total_row_count}")
  else
    HtmlReporter.add_step_failed_with_ss('Data Error', "Expected: #{count.to_s} orders | Actual: #{total_row_count}")
    raise Exception.new "Filter did not return '#{count.to_s}' order(s)"
  end
end

Then(/^the ChainClear file will have rejected reasons$/) do
  @chain_clear_file.validate_rejected_reasons
end

Then(/^the ChainClear file will show euros for all currency$/) do
  @chain_clear_file.validate_opbank_euros
end

Then(/^the TABCP file will show euros for all currency$/) do
  @ta_bcp_file.validate_opbank_euros
end

Then(/^User can validate holdings redemption in CSV$/) do
  @chain_clear_file.validate_redemption_holding @order_ref
end

Then(/^the ChainClear file will have rejected reasons for emx orders/) do
  emx_order_string = File.read(Config['emx_e2e']['base'] + Config['emx_e2e']['active_file'])
  @chain_clear_file.validate_rejected_reasons_emx emx_order_string
end

Then(/^only '(.*)' managed orders are displayed$/) do |fund_manager|
  table_data = @data_grid.get_table_data_map
  table_data.each do |row|
    unless row['fund_manager'] == fund_manager
      HtmlReporter.add_step_failed_with_ss('Data Error', "Not all orders are '#{fund_manager}'")
      raise Exception.new "Filter did not return only '#{fund_manager}' orders"
    end
  end
  HtmlReporter.add_step_passed_with_ss('Data Validation', "All orders are #{fund_manager}")
end

Then(/^orders displayed is '(.*)'$/) do |count|
  if @data_grid.total_row_count.text == count.to_s
    HtmlReporter.add_step_passed_with_ss("#{count} orders expected", 'Successful')
  else
    HtmlReporter.add_step_failed_with_ss("#{count} orders expected", "Failed | Count: #{@data_grid.total_row_count.text}")
    raise Exception.new 'Order Count not correct'
  end
end

Then(/^the status filter will align with the table data$/) do
  @search_grid.validate_dropdown_options 'Status', @data_grid.get_unique_statuses
  @search_grid.clear_drop_box
end

Then(/^the channel filter will align with the table data$/) do
  @search_grid.validate_dropdown_options 'Channel', @data_grid.get_unique_channels
  @search_grid.clear_drop_box
end

Then(/^the trade type filter will align with the table data$/) do
  @search_grid.validate_dropdown_options 'Trade type', @data_grid.get_unique_trade_types
  @search_grid.clear_drop_box
end

Then(/^the processing system filter will align with the table data$/) do
  @search_grid.validate_dropdown_options 'Processing system', @data_grid.get_unique_processing_systems
  @search_grid.clear_drop_box
end

Then(/^the isin filter will align with the table data$/) do
  @search_grid.validate_dropdown_options 'ISIN', @data_grid.get_unique_isins
  @search_grid.clear_drop_box
end

Then(/^the fund manager filter will align with the table data$/) do
  @search_grid.validate_dropdown_options 'Fund manager', @data_grid.get_unique_fund_managers
  @search_grid.clear_drop_box
end
