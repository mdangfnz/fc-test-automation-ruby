Given (/^User logged into Keycloak$/) do
  BrowserObject.obj.goto Config['keycloak']['url']
  @keycloak_login_page = KeycloakLoginPage.new
  @keycloak_login_page.admin_console_link.click
  @keycloak_login_page.username_field.set Config['keycloak']['admin_user']
  @keycloak_login_page.password_field.set Config['keycloak']['admin_pass']
  @keycloak_login_page.login_button.click

  puts 'wait'
end

When(/^User starts a Keycloak instance$/) do
  BrowserObjectTwo.load 'Chrome'
  BrowserObjectTwo.goto_url Config['keycloak']['url']
  @keycloak_login_page = KeycloakLoginPage.new
  @keycloak_login_page.admin_console_link.click
  sleep 5
  @keycloak_login_page.username_field.set Config['keycloak']['admin_user']
  @keycloak_login_page.password_field.set Config['keycloak']['admin_pass']
  @keycloak_login_page.login_button.click
end

When(/^Keycloak revokes the Chain user$/) do
  BrowserObjectTwo.obj.a(href: "#/realms/FNZ-Chainclear/users").click
  BrowserObjectTwo.obj.text_field(placeholder: 'Search...').set Config['chain_ui']['username']
  sleep 3
  BrowserObjectTwo.obj.i(id: 'userSearch').click
  sleep 3
  BrowserObjectTwo.obj.td(text: 'Edit').click
  BrowserObjectTwo.obj.a(text: 'Sessions').click
  HtmlReporter.add_step_passed_with_ss_bo2('Keycloak', 'User Sessions')
  BrowserObjectTwo.obj.a(id: 'logoutAllSessions').click
  sleep 5
  HtmlReporter.add_step_passed_with_ss_bo2('Keycloak', 'Click Logout Chain user')
  puts 'wait'
end

When(/^Keycloak navigates to user credentials$/) do
  BrowserObjectTwo.obj.a(href: "#/realms/FNZ-Chainclear/users").click
  BrowserObjectTwo.obj.text_field(placeholder: 'Search...').set Config['chain_ui']['reset_pwd_usr']
  sleep 3
  BrowserObjectTwo.obj.i(id: 'userSearch').click
  sleep 3
  BrowserObjectTwo.obj.td(text: 'Edit').click
  BrowserObjectTwo.obj.a(text: 'Credentials').click
end

When(/^Keycloak resets user password$/) do
  #add 1 to file to update the password
  file_full_path = Config['auto_data_path'] + Config['chain_ui']['reset_pwd_file']
  pwd_num = File.read(file_full_path)
  pwd_num = (pwd_num.to_i + 1).to_s
  pwd = Config['chain_ui']['reset_pwd_pwd'] + pwd_num

  #Update the Password
  BrowserObjectTwo.obj.text_field(name: 'newPas').set pwd
  BrowserObjectTwo.obj.text_field(name: 'confirmPas').set pwd
  BrowserObjectTwo.obj.button(text: 'Reset Password').click
  sleep 1
  HtmlReporter.add_step_passed_with_ss_bo2('Click Reset Password', 'Successful')
  BrowserObjectTwo.obj.button(text: 'Change password').click
  sleep 1
  HtmlReporter.add_step_passed_with_ss_bo2('Click Change Password', 'Successful')
  HtmlReporter.add_step_passed_no_ss('New Password', pwd)

  #write new password to file
  file_obj = File.open(file_full_path, 'w')
  file_obj.write(pwd_num)
  # Config['chain_ui']['reset_pwd_file']
  # FileUtils.move(Config['chain_ui']['reset_pwd_file'], Config['auto_data_path'])
  file_obj.close
end

When(/^Keycloak resets the Chain user's password$/) do
  step 'Keycloak navigates to user credentials'
  step 'Keycloak resets user password'
end

When(/^Keycloak resets the Chain user's password and disables temporary$/) do
  step 'Keycloak navigates to user credentials'

  sleep 1
  BrowserObjectTwo.obj.span(class: 'onoffswitch-inner').click
  sleep 1
  HtmlReporter.add_step_passed_with_ss_bo2('Disable Temporary', 'Successful')

  step 'Keycloak resets user password'
end
