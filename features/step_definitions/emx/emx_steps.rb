Given(/^User logged into EMX$/) do
  BrowserObject.goto_url Config['emx']['url']
  @emx_login_page = EmxLoginPage.new
  @emx_home_page = EmxHomePage.new
  @emx_writer = EmxFileWriter.new
  @emx_order_page = EmxOrderPage.new

  #Login
  @emx_login_page.company_id_field.set Config['emx']['company_id']
  @emx_login_page.username_field.set Config['emx']['username']
  @emx_login_page.password_field.set Config['emx']['password']
  BrowserObject.action_timeout_retry @emx_login_page.login_button.click
end

When(/^User uploads an '(.*)' file$/) do |message_type|


  # Navigate to Uploads and select message type
  @emx_home_page.upload_link.click
  @emx_home_page.message_type_option.select message_type
  HtmlReporter.add_step_passed_with_ss('Message Type', message_type)


  #THIS ASSUMES ALL USERS ARE FOLLOWING the standards "EMX ORDER-###.txt"
  #Get latest file number used
  #latest_upload_num = BrowserObject.obj.table(summary: 'List of Upload File History')[1][1].text.split("\\")[-1].split('-')[-1][0..-5]

  #Generate a new file and upload it to EMX
  @emx_home_page.upload_button.click
  @emx_writer.write_archive_file
  @emx_home_page.choose_file_path @emx_writer.get_upload_file_name
  @emx_home_page.upload_button.click

  #Confirm Successful upload
  if @emx_home_page.upload_confirmation_button.exists?
    HtmlReporter.add_step_passed_with_ss 'Upload Orders', 'Successful'
    @emx_home_page.upload_confirmation_button.click
  else
    HtmlReporter.add_step_failed_with_ss 'Upload Orders', 'failed'
    raise Exception.new 'Upload failed'
  end
end

When(/^User clicks on Mailbox orders$/) do
  @emx_order_page.mailbox_link.hover
  @emx_order_page.mailbox_orders_link.click
end

When(/^User filters emx on '(.*)' status$/) do |status|
  case status.upcase
  when 'NOT BOOKED'
    @emx_order_page.not_booked_checkbox.click
  when 'BOOKED'
    @emx_order_page.booked_checkbox.click
  when 'COMPLETE'
    @emx_order_page.complete_checkbox.click
  when 'MERRD (ALL)'
    @emx_order_page.merrd_all_checkbox.click
  when 'MERRD (UNREAD)'
    @emx_order_page.merrd_unread_checkbox.click
  else
    raise Exception.new "Filter by #{status} not valid"
  end

  HtmlReporter.add_step_passed_with_ss 'Status selected', status
end

When(/^User sets emx record display to '(.*)'$/) do |count|
  @emx_order_page.records_displayed_option.select /#{count}/
  HtmlReporter.add_step_passed_with_ss 'Records Displayed', count
end

# format '26/03/2020'
When(/^User sets emx 'from date' to '(.*)'$/) do |from_date|
  @emx_order_page.from_date_field.set from_date
  sleep 3
  HtmlReporter.add_step_passed_with_ss 'From Date', from_date
end

# format '30/03/2020'
When(/^User sets emx 'to date' to '(.*)'$/) do |to_date|
  @emx_order_page.to_date_field.set to_date
  sleep 3
  HtmlReporter.add_step_passed_with_ss 'To Date', to_date
end

When(/^User clicks the emx search button$/) do
  @emx_order_page.search_button.click

end

When(/^User filters emx on today's date$/) do
  today = DateTime.now.strftime("%d/%m/%Y")

  step "User sets emx 'from date' to '#{today}'"
  step "User sets emx 'to date' to '#{today}'"

end

When(/^User filters by order reference matching '(.*)'$/)do |order_ref_string|
  @emx_order_page.order_reference_field.set order_ref_string
  HtmlReporter.add_step_passed_with_ss 'Filter by Order Ref', order_ref_string
end

When(/^User filters by orders from uploaded file$/) do
  #EMX ORDER STRING is stored during file upload
  emx_order_string = File.read(Config['emx_e2e']['base'] + Config['emx_e2e']['active_file'])
  step "User filters by order reference matching '#{emx_order_string}'"
end

Then(/^EMX orders displayed is '(.*)'$/) do |count|
  sleep 30 #wait for table to load
  if @emx_order_page.expected_orders count
    HtmlReporter.add_step_passed_with_ss "Expected #{count} orders", 'Successful'
  else
    HtmlReporter.add_step_failed_with_ss "Expected #{count} orders", "Failed"
    raise Exception.new 'EMX order count not correct'
  end

end

Then(/^EMX rejected reasons are all valid$/)do
  @emx_order_page.failed_order_rows.each do |error|
    error.a(text: 'View').click
    HtmlReporter.add_step_passed_with_ss('Error Msg', @emx_order_page.failed_order_text.text)
    BrowserObject.obj.back
  end
end

Then(/^the trade will appear '(.*)' in EMX$/)do |status|
  step 'User logged into EMX'
  step 'User clicks on Mailbox orders'
  step "User filters emx on '#{status}' status"
  step "User filters by order reference matching '#{@orders['order_reference']}'"
  step "EMX orders displayed is '1'"
end

