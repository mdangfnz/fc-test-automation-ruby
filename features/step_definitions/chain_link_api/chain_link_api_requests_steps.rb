When(/^I get requests via Chain API by chain ID$/) do
  @request_ws = GetRequestsById.new
  @request_ws.send @chain_id
end

When(/^I get state update '(.*)' request via Chain API by chain ID$/) do |state|
  @request_ws = GetRequestsById.new
  @request_ws.send @chain_id
  @request_id = @request_ws.get_state_update_request state
end

When(/^I get outbound status report request via Chain API by chain ID$/) do
  @request_ws = GetRequestsById.new
  @request_ws.send @chain_id
  @request_id = @request_ws.get_outbound_status_report_request
end

Then(/^I will see requests details via Chain API by chain ID$/) do
  @request_ws.validate_response_details
  @request_id = @request_ws.get_request_id
end