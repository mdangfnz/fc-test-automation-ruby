Given(/^I am using Chain API as (.*)$/)do |user|

  case user.upcase
  when 'FNZ REGISTER'
    Config['chain_link_api']['active_user'] = Config['chain_link_api']['register_user']
    Config['chain_link_api']['active_pw'] = Config['chain_link_api']['register_pw']
  when 'FNZ CLEAR'
    Config['chain_link_api']['active_user'] = Config['chain_link_api']['clear_user']
    Config['chain_link_api']['active_pw'] = Config['chain_link_api']['clear_pw']
  when 'MDANG'
    Config['chain_link_api']['active_user'] = Config['chain_link_api']['test_user']
    Config['chain_link_api']['active_pw'] = Config['chain_link_api']['test_pw']
  when 'FNZ ONE OPBANK'
    Config['chain_link_api']['active_user'] = Config['chain_link_api']['one_opbank_user']
    Config['chain_link_api']['active_pw'] = Config['chain_link_api']['one_opbank_pass']
  when 'REGISTER FINLAND'
    Config['chain_link_api']['active_user'] = Config['chain_link_api']['register_finland_user']
    Config['chain_link_api']['active_pw'] = Config['chain_link_api']['register_finland_pass']
  else
    raise Exception.new 'Invalid user - please check configs'
  end

end

Given(/^I have a Chain API token$/)do
  @login_ws = PostUserLogin.new
  @login_ws.send
  @login_ws.validate
end

Given(/^I have an invalid Chain API token$/)do
  #using an old token
  Config['chain_link_api']['token'] == 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJRRnAwX3NycW5ZMXRXMmlnR09YZXcycmxBYmZ5Y0JGNFNPQmVGNlhlWlNVIn0.eyJqdGkiOiJlMTE4YzgyNC00Yjg3LTQ2OGEtYTU5NS03OGIwYmM5ZTJkOTUiLCJleHAiOjE1ODkxODAyOTUsIm5iZiI6MCwiaWF0IjoxNTg5MTc5MDk1LCJpc3MiOiJodHRwczovL2tleWNsb2FrLmRlbW8uZm56Y2hhaW4uY29tL2F1dGgvcmVhbG1zL0ZOWi1DaGFpbmNsZWFyIiwic3ViIjoiMjkzMjI2ZjYtODViOC00MGIzLTg5ODQtMWU2MTdhYjdiZWM0IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZm56LWNoYWluY2xlYXItYXBpIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiNWVjYzRhMjAtNzIwNS00ZjJmLWEyNTEtNTRiN2E3M2M3NDA1IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJjaGFpbmNsZWFyLWFwaS11c2VyIl19LCJzY29wZSI6Im9wZW5pZCBtc3BpZCBlbWFpbCBwcm9maWxlIiwibXNwaWQiOiJmbnpwbGF0Zm9ybU1TUCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiZm56X2NsZWFyIiwidXNlcm5hbWUiOiJmbnpfY2xlYXIifQ.go6oKEuIn8TgDCSnikvuua4zHnbY8VCCg_8QE6lyF4Hy_jf_8_4pUU4G39-nJEpY--Emr2NruWk8Doec7XqpxSU5RtvxqCbSkIgWzpqNz1AU3JabINQxl6uRNclAgyh7MrhWqBqLx6OJOY5_vdHeX05yScVQy1fJA7gF96mzR5QBDFAADLKRdJe3DxBshn7ZDie6l5mgojmp0gimLbMhqlYj-I43CSGUopMl5hOCUsXEdWax0R2FMfLfsEThlYxB4Fvyhzzc49qPCFtn2zZg22923Ca6jk_Dwu6OPsPhky6tQts-JSr46ZiUmz--DxEARalv8Irw9SJ3g6hM2JII_Q'
end

Given(/^I don't have a Chain API token$/)do
  #no action required
end

Given(/^I have a Chain API client token$/)do
  @client_login_ws = PostGrantClientLogin.new
  @client_login_ws.send
  @client_login_ws.validate
end

When(/^I send a post request to the Chain API login service$/)do
  @login_ws = PostUserLogin.new
  @login_ws.send
end

When(/^I generate a new token$/)do
  @login_ws = PostUserLogin.new
  @login_ws.send
  @login_ws.validate
end

Then(/^the login service returns a token$/)do
  @login_ws.validate
end

Then(/^the token is new$/)do
  puts 'do nothing'
end

#
When(/^I send a get request to the Chain API health service$/)do
  @health_ws = GetHealth.new
  @health_ws.send
end

Then(/^the health service returns a healthy status$/)do
  @health_ws.validate
end

When(/^I accept emx file orders via FNZ TA endpoint$/) do
  @ta_accepted_ws = PostFnzTaAccepted.new
  @orders.each_with_index do |order, count|
    if count == '100'
      break
    end
    @ta_accepted_ws.send order['chain_id'], order['order_ref'], 'N'
  end
end

When(/^I accept emx perf orders via FNZ TA endpoint$/) do
  @ta_accepted_ws = PostFnzTaAccepted.new
  @orders
  @orders.each do |order|
    @ta_accepted_ws.send order['chain_id'], order['order_ref'], 'PERF'
  end
end

When(/^I price emx file orders via FNZ TA endpoint$/) do
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], 'N'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price all emx perf orders via FNZ TA endpoint$/) do
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], 'PERF'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I accept orders via FNZ TA endpoint$/) do
  @ta_accepted_ws = PostFnzTaAccepted.new
  @orders.each do |order|
    @ta_accepted_ws.send order['chain_id'], order['order_ref']
  end
end

When(/^I price orders via FNZ TA endpoint$/) do
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type']
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without Fund ID$/) do
  DataPrep['fund_id'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without deal reference$/) do
  DataPrep['deal_ref'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without ACCT ID$/) do
  DataPrep['acct_id'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without net amt$/) do
  DataPrep['net_amt'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without sttlm amt$/) do
  DataPrep['sttlm_amt'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without price amt$/) do
  DataPrep['price_amt'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without grss amt$/) do
  DataPrep['grss_amt'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without units$/) do
  DataPrep['unit'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without trade date$/) do
  DataPrep['trad_dt_tm'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without cash sttlm dt$/) do
  DataPrep['csh_sttlm_dt'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without order type$/) do
  DataPrep['ordr_typ'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without grss amt ccy$/) do
  DataPrep['grss_amt_ccy'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without net amt ccy$/) do
  DataPrep['net_amt_ccy'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without price amt ccy$/) do
  DataPrep['price_amt_ccy'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end

When(/^I price orders via FNZ TA endpoint without sttlm amt ccy$/) do
  DataPrep['sttlm_amt_ccy'] = ''
  @ta_priced_ws = PostFnzTAPriced.new
  @orders.each do |order|
    @ta_priced_ws.send order['chain_id'], order['order_ref'], order['order_type'], '400'
  end
  @chain_id = @orders[0]['chain_id']
end



When(/^I get received orders with order_ref '(.*)' via transactions endpoint$/) do |order_ref|
  @state_ws = GetState.new("TRANSACTION_RECEIVED_BY_CHAIN")
  @orders = @state_ws.get_orders_by_order_ref order_ref
end


#TODO: Review if this step is properly using the emx_order file
When(/^I get my received orders via transactions endpoint$/) do
  emx_order_string = File.read(Config['emx_e2e']['base'] + '/' + Config['emx_e2e']['active_file'])
  step "I get received orders with order_ref '#{emx_order_string}' via transactions endpoint"
end

When(/^I get my received EMX file orders via transactions endpoint$/) do
  emx_order_string = File.read(Config['emx_e2e']['base'] + '/' + Config['emx_e2e']['active_file'])
  step "I get received orders with order_ref '#{emx_order_string}' via transactions endpoint"
end

When(/^I get accepted orders via get transactions endpoint$/) do
  @state_ws = GetState.new("ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR")
  @state_ws.send
end

When(/^I get confirmed orders via get transactions endpoint$/) do
  @state_ws = GetState.new("TRANSACTION_CONFIRMATION_RECEIVED_BY_DISTRIBUTOR")
  @state_ws.send
end

When(/^I regenerate the cert$/)do
  @regenerate_ws = PostRegenerateCert.new
  @regenerate_ws.send
end

Then(/^all orders are sourced from '(.*)'$/)do |transaction_source|
  case transaction_source
  when 'FNZ_CLEAR'
    @state_ws.validate_source 'FNZ_CLEAR'
  when 'FNZ_ONE_OPBANK'
    @state_ws.validate_source 'FNZ_ONE_OPBANK'
  when 'EMX'
    @state_ws.validate_source 'EMX'
  end
end

Then(/^no orders are sourced from '(.*)'$/)do |transaction_source|
  case transaction_source
  when 'FNZ_CLEAR'
    @state_ws.validate_source_none 'FNZ_CLEAR'
  when 'FNZ_ONE_OPBANK'
    @state_ws.validate_source_none 'FNZ_ONE_OPBANK'
  when 'EMX'
    @state_ws.validate_source_none 'EMX'
  end
end


Then(/^orders have been accepted$/)do

end

Then(/^orders have been priced$/)do

end

Then(/^my get state request returns a 401 response$/)do
  @state_ws.validate_401_no_token

end

Then(/^the cert has been successfully regenerated$/)do
  @regenerate_ws.validate
end