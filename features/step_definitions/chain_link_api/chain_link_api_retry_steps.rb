When(/^I retry my trace ID$/) do
  @error_retry_ws = PutRetryError.new
  @error_retry_ws.send @trace_id
end

Then(/^my retry error response returns same chain ID$/) do
  @error_retry_ws.validate_chain_id @chain_id
end

When(/^I retry my request ID$/) do
  @request_retry_ws = PutRetryRequest.new
  @request_retry_ws.send @request_id
end

Then(/^my retry request response returns 'processing'$/) do
  @request_retry_ws.validate
end