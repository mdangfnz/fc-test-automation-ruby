When(/^I place a redemption via Chain API$/) do
  DataPrep['default'] = 'Y'
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a redemption with units via Chain API$/) do
  DataPrep['default'] = 'Y'
  DataPrep['units'] = '1'
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a redemption with amt via Chain API$/) do
  DataPrep['default'] = 'Y'
  DataPrep['amt'] = '100'
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a subscription via Chain API$/) do
  DataPrep['default'] = 'Y'
  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription with units via Chain API$/) do
  DataPrep['default'] = 'Y'
  DataPrep['units'] = '1'
  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription with amt via Chain API$/) do
  DataPrep['default'] = 'Y'
  DataPrep['amt'] = '100'
  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a redemption via Chain API with empty fund nm$/) do
  DataPrep['default'] = 'Y'
  DataPrep['fund_nm'] = ''
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a redemption via Chain API with holding redemption rate$/) do
  DataPrep['hldgs_red_rate'] = '100'
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a redemption via Chain API with empty fund manager$/) do
  DataPrep['default'] = 'Y'
  DataPrep['fund_provider'] = ''
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a redemption via Chain API with an invalid fund manager$/) do
  DataPrep['default'] = 'Y'
  DataPrep['fund_provider'] = 'FAIL TEST'
  @redemption_ws = PostRedemption.new
  @redemption_ws.send
end

When(/^I place a subscription via Chain API with 0 units$/) do
  DataPrep['units'] = '0'
  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with 0 amount$/) do
  DataPrep['amt'] = '0'

  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with empty units$/) do
  DataPrep['units'] = ''
  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with empty amount$/) do
  DataPrep['amt'] = ''

  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with empty account id$/) do
  DataPrep['default'] = 'Y'
  DataPrep['acct_id'] = ''

  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with empty order ref$/) do
  DataPrep['default'] = 'Y'
  DataPrep['order_ref'] = ''

  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with empty ISIN$/) do
  DataPrep['default'] = 'Y'
  DataPrep['isin'] = ''

  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with an invalid ISIN$/) do
  DataPrep['default'] = 'Y'
  DataPrep['isin'] = 'AB12345'

  @subscription_ws = PostSubscription.new
  @subscription_ws.send
end

When(/^I place a subscription via Chain API with a duplicate order ref$/) do
  DataPrep['default'] = 'Y'
  DataPrep['order_ref'] = 'Dup_' + StringsUtil.generate_code(5)
  @order_ref = DataPrep['order_ref']
  2.times do |count|
    @subscription_ws = PostSubscription.new
    @subscription_ws.send
    @subscription_ws.validate_202
    HtmlReporter.add_step_passed_no_ss("Order Ref(#{(count + 1).to_s}) Used", DataPrep['order_ref'])
  end

  @chain_id = @subscription_ws.get_chain_id
end

When(/^I place a redemption via Chain API with a duplicate order ref$/) do
  DataPrep['default'] = 'Y'
  DataPrep['order_ref'] = 'Dup_' + StringsUtil.generate_code(5)
  @order_ref = DataPrep['order_ref']

  2.times do |count|
    @redemption_ws = PostRedemption.new
    @redemption_ws.send
    @redemption_ws.validate_202
    HtmlReporter.add_step_passed_no_ss("Order Ref(#{(count + 1).to_s}) Used", DataPrep['order_ref'])
  end

  @chain_id = @redemption_ws.get_chain_id
end


Then(/^my subscription receives a 400 error$/) do
  @subscription_ws.validate_400
end

Then(/^my confirmation receives a 400 error$/) do
  @ta_priced_ws.validate_400
end

Then(/^my redemption receives a 400 error$/) do
  @redemption_ws.validate_400
end

Then(/^my redemption receives a 202 success$/) do
  @redemption_ws.validate_202
  @chain_id = @redemption_ws.get_chain_id
end

Then(/^my subscription receives a 202 success$/) do
  @subscription_ws.validate_202
  @chain_id = @subscription_ws.get_chain_id
end

Then(/^my trade will have an error via Chain API$/) do
  @error_ws = GetErrors.new
  @error_ws.send
  @error_ws.query_error @chain_id
  @trace_id = @error_ws.get_trace_id
  @error_id = @error_ws.get_error_id

end

When(/^I get all error count via Chain API$/) do
  @error_ws = GetErrors.new
  @error_ws.send
  @error_count = @error_ws.get_error_count
end

When(/^I get all error IDs via Chain API$/) do
  @error_ws = GetErrors.new
  @error_ws.send
  @error_id_arr = @error_ws.get_all_error_ids
end

When(/^I update error state for all errors$/) do
  @error_ws.update_error_state @error_id_arr
end

When(/^I wait for my transaction to be 'Accepted'$/) do
  pass = false

  #Wait 2 minutes, before starting
  puts 'wait 120seconds before checking message'
  sleep 120 #TODO: Find a dynamic wait option

  #Check Status every 60seconds, overall 6minutes
  @status_report_ws = GetTransactionStatusReport.new
  4.times do |count|
    @status_report_ws.send @chain_id
    if @status_report_ws.status_report_received?
      pass = true
      HtmlReporter.add_step_passed_no_ss('Status Report Created', 'Continue Test')
      break
    end
    puts 'waiting another 60 seconds'
    sleep 60
  end

  unless pass
    HtmlReporter.add_step_failed_no_ss('Status Report Not Created', 'Waited 6 minutes')
  end
end

Then(/^Chain API returns error count 0$/) do
  if @error_count == '0'
    HtmlReporter.add_step_passed_no_ss('Chain API errors', @error_count)
  else
    HtmlReporter.add_step_failed_no_ss('Chain API errors is not 0', 'Test Fail')
    raise Exception.new 'Errors not resolved'
  end
end