When(/^I get emx order from file with index '(.*)'$/) do |index|

  order_string = File.read(Config['emx_e2e']['base'] + Config['emx_e2e']['active_file'])
  order_string = order_string + index.to_s
  @order_ref = order_string.to_s
end

When(/^I check order ref shows '(.*)'$/) do |listener_status|
  message_ws = GetMessageByOrderReference.new @order_ref

  #Wait 2 minutes, before starting
  puts 'wait 120seconds before checking message'
  pass = false
  sleep 120 #TODO: Find a dynamic wait option
  #Check message status every 60seconds, overall 6minutes
  4.times do |count|
    message_ws.send
    if message_ws.validate_listener listener_status
      pass = true
      break
    end
    puts 'waiting another 60seconds'
    sleep 60
  end

  unless pass
    HtmlReporter.add_step_failed_no_ss('Trade not accepted','Waited 6 minutes')
    raise Exception.new 'Trade was not accepted within 6minutes'
  end

  sleep 90 # Wait another 90 seconds for messages to display on PLATFORM
end

Then(/^I can move to the next scenario$/) do
  HtmlReporter.add_step_passed_no_ss'Status confirmed','Moving to the next scenario'
end