When(/^I send a get request to Chain API state service with '(.*)'$/) do |state|
  @state_ws = GetState.new(state)
  @state_ws.send
end

Then(/^all orders are from FNZ_ONE_OPBANK$/) do
  @state_ws.validate_opbank_source

end


Then(/^then all orders show status '(.*)'$/) do |state|
  @state_ws.validate_state

end

When(/^I get EMX uploaded orders via the transactions endpoint$/) do
  @state_ws = GetState.new("TRANSACTION_RECEIVED_BY_CHAIN")
  @orders = @state_ws.get_emx_file_orders
end

When(/^I get all EMX perf received orders via the transactions endpoint$/) do
  @state_ws = GetState.new("TRANSACTION_RECEIVED_BY_CHAIN")
  @orders = @state_ws.get_all_emx_perf_orders
end

When(/^I get accepted EMX file orders via the transactions endpoint$/) do
  @state_ws = GetState.new("ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR")
  @orders = @state_ws.get_emx_file_orders
end

When(/^I get accepted EMX perf orders via the transactions endpoint$/) do
  @state_ws = GetState.new("ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR")
  @orders = @state_ws.get_all_emx_perf_orders
end

When(/^I get one accepted EMX order via the transactions endpoint$/) do
  @state_ws = GetState.new("ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR")
  @orders = @state_ws.get_latest_by_source 'EMX'
  @chain_id = @orders[0]['chain_id']
end

When(/^I get one accepted Clear order via the transactions endpoint$/) do
  @state_ws = GetState.new("ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR")
  @orders = @state_ws.get_latest_by_source 'FNZ_CLEAR'
end

When(/^I check my trade has been received$/) do
  sleep 10 #wait for trade to be received
  @transaction_ws = GetTransactionByID.new
  @transaction_ws.send @chain_id
  @orders = @transaction_ws.get_received_trade_details
  @order_ref = @orders[0]['order_ref']
end

#TODO: Add validation from API side (not necessary?)
When(/^my orders will show 'Accepted' from the FNZ API$/) do
  @state_ws = GetState.new "TRANSACTION_ACCEPTED_BY_CHAIN"


    #REJECTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR
    #ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR
    #TRANSACTION_CONFIRMATION_RECEIVED_BY_DISTRIBUTOR - priced
end

When(/^my emx orders will show 'Accepted'$/) do

end