When(/^I post state update to '(.*)' via Chain API$/) do |state|
  @state_update_ws = PostStateUpdate.new
  @state_update_ws.send @chain_id, state
  @state_update_ws.validate_202
end