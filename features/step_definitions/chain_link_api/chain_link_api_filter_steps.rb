When(/^I send a post request to Chain API filter service from '(.*)' to '(.*)'$/) do |from_date, to_date|
  @filter_ws = PostFilter.new
  @filter_ws.send(from_date,to_date)
end

Then(/^Chain API filter response only returns blockchains within date range$/) do
  @filter_order_count = @filter_ws.validate_count
end


Then(/^I get one transaction from each source$/) do
  @filter_ws.validate_all_sources_appear
  @emx_chain = @filter_ws.get_emx_chain
  @clear_chain = @filter_ws.get_clear_chain
  @opbank_chain = @filter_ws.get_opbank_chain
end


Then(/^admin user can get each chain$/) do
  @transaction_ws = GetTransactionByID.new

  @transaction_ws.send @emx_chain
  @transaction_ws.validate_response_details

  @transaction_ws.send @clear_chain
  @transaction_ws.validate_response_details

  @transaction_ws.send @opbank_chain
  @transaction_ws.validate_response_details
end
