Feature: Keycloak can disable temp password upon password reset

  @ui_test @keycloak
  Scenario: fc2744 - Keycloak disable temp password
    Given User starts a Keycloak instance
    When Keycloak resets the Chain user's password and disables temporary
    And I goto Chain UI login page
    And I login with keycloak temp password
    And User can see Chain UI