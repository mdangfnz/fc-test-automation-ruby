Feature: Chain UI will disconnect a user after 20minutes of inactivity

  @ui_test
  Scenario: fc2780 - Chain UI session timeout
    Given User logged into FNZ Chain UI
    And User can see Chain UI
    When I wait '1260' seconds and capture time
    Then Chain UI redirected to login