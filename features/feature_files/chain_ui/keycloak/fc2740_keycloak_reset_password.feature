Feature: Keycloak can update Chain UI password

  @ui_test @keycloak
  Scenario: fc2740 - Keycloak reset Chain user's pw
    Given User starts a Keycloak instance
    When Keycloak resets the Chain user's password
    And I goto Chain UI login page
    And I login with keycloak temp password
    Then User is prompt to update password
    And User can see Chain UI
