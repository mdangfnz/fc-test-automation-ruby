Feature: Accessing Chain UI redirects to keycloak to handle authentication

  @ui_test
  Scenario: fc2811 - Chain UI login redirect
    Given I goto Chain UI login page
    When login screen has necessary components
    Then Chain UI url redirects to keycloak