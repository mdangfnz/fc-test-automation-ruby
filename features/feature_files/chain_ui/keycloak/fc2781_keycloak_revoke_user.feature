Feature: Keycloak provides functionality to revoke a user

  @ui_test @keycloak
  Scenario: fc2781 - Keycloak revoke Chain UI user
    Given User logged into FNZ Chain UI
    And User can see Chain UI
    And User starts a Keycloak instance
    And Keycloak revokes the Chain user
    Then Chain UI redirected to login
