Feature: OPBank and TA Finland can only see OPBANK trades
  
  @api_test
  Scenario: fc3340 - Opbank user only shows OPBANK trades
    Given I am using Chain API as FNZ ONE OPBANK
    And I have a Chain API client token
    When I send a get request to Chain API state service with 'CONFIRMATION_RECEIVED_BY_DISTRIBUTOR'
    Then all orders are from FNZ_ONE_OPBANK

  @api_test
  Scenario: fc3340 - Register Finland user only shows OPBANK trades
    Given I am using Chain API as REGISTER FINLAND
    And I have a Chain API client token
    When I send a get request to Chain API state service with 'CONFIRMATION_RECEIVED_BY_DISTRIBUTOR'
    Then all orders are from FNZ_ONE_OPBANK