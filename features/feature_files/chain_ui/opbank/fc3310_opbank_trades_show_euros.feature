Feature: OP Bank trades show EUROS for currency
  
  @ui_test
  Scenario: fc3310 - Opbank Trades shows Euros
    Given User logged into FNZ Chain UI
    When User filters by 'Fnz One Opbank' channel
    And User sets the format to TABCPFORMAT and click download
    And I wait '5' seconds
    And User sets the format to ChainClearDefault and click download
    And the trade data will display in the table
    Then the ChainClear file will show euros for all currency
    And the TABCP file will show euros for all currency
    And the trade data will display euros for opbank