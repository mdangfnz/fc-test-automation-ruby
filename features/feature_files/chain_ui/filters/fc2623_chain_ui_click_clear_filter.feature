Feature: Chain UI allows filter on orders by status

  @ui_test
  Scenario: fc2623 - Chain UI clear filters
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 21/04/2020 00:00
#    And User sets the 'to' date to 21/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Emx' channel
    And User filters by 'Priced' status
    And User filters by 'EQTFT' fund manager
    And User filters by 'Buy' trade type
    And User filters by 'Platform' processing system
    And User filters by 'GB00B4RL9738' isin
    And the trade data will display in the table
    And User gets the trade count displayed
    And User clicks clear filter
    Then the trade count has been updated