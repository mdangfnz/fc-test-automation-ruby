Feature: Chain UI allows filter on orders by status


  @ui_test
  Scenario: fc2409 - User filters orders by 'SAMUK' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'SAMUK' fund manager
    Then the trade data will display in the table
    And only 'SAMUK' fund manager orders are displayed


  @ui_test
  Scenario: fc2409 - User filters orders by 'PFSHUETCAP' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'PFSHUETCAP' fund manager
    Then the trade data will display in the table
    And only 'PFSHUETCAP' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'MERIANIM' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'MERIANIM' fund manager
    Then the trade data will display in the table
    And only 'MERIANIM' fund manager orders are displayed


  @ui_test
  Scenario: fc2409 - User filters orders by 'OPFMC' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'OPFMC' fund manager
    Then the trade data will display in the table
    And only 'OPFMC' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'TTMNT' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 23/04/2020 00:00
#    And User sets the 'to' date to 24/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'TTMNT' fund manager
    Then the trade data will display in the table
    And only 'TTMNT' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'SHAREFUNDS' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'SHAREFUNDS' fund manager
    Then the trade data will display in the table
    And only 'SHAREFUNDS' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'SANAT' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'SANAT' fund manager
    Then the trade data will display in the table
    And only 'SANAT' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'MERIT' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'MERIT' fund manager
    Then the trade data will display in the table
    And only 'MERIT' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'HOSTT' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'HOSTT' fund manager
    Then the trade data will display in the table
    And only 'HOSTT' fund manager orders are displayed

  @ui_test
  Scenario: fc2409 - User filters orders by 'EQTFT' fund manager in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'EQTFT' fund manager
    Then the trade data will display in the table
    And only 'EQTFT' fund manager orders are displayed
