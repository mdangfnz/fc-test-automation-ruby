Feature: Chain UI filter on orders by fund manager, which is displayed in Chain UI and ChainClear


  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'EQTFT' in ChainUI
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'EQTFT' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'EQTFT' managed orders are displayed
    And the ChainClearDefault file matches the table's data


  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'HOSTT' in ChainUI 
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'HOSTT' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'HOSTT' managed orders are displayed
    And the ChainClearDefault file matches the table's data


  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'MERIT' in ChainUI 
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'MERIT' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'MERIT' managed orders are displayed
    And the ChainClearDefault file matches the table's data


  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'SANAT' in ChainUI 
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'SANAT' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'SANAT' managed orders are displayed
    And the ChainClearDefault file matches the table's data

  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'SHAREFUNDS' in ChainUI 
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'SHAREFUNDS' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'SHAREFUNDS' managed orders are displayed
    And the ChainClearDefault file matches the table's data


  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'TTMNT' in ChainUI 
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'TTMNT' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'TTMNT' managed orders are displayed
    And the ChainClearDefault file matches the table's data


#  @ui_test
#  Scenario: fc3342 - (ChainClear) Filter orders by 'OPFMC' in ChainUI 
#    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
#    And User filters by 'OPFMC' fund manager
#    And User sets the format to ChainClearDefault and click download
#    Then the trade data will display in the table
#    And only 'OPFMC' managed orders are displayed
#    And the ChainClearDefault file matches the table's data

#  @ui_test
#  Scenario: fc3342 - (ChainClear) Filter orders by 'MERIANIM' in ChainUI 
#    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
#    And User filters by 'MERIANIM' fund manager
#    And User sets the format to ChainClearDefault and click download
#    Then the trade data will display in the table
#    And only 'MERIANIM' managed orders are displayed
#    And the ChainClearDefault file matches the table's data

  @ui_test
  Scenario: fc3342 - (ChainClear) Filter orders by 'PFSHUETCAP' in ChainUI 
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User filters by 'PFSHUETCAP' fund manager
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And only 'PFSHUETCAP' managed orders are displayed
    And the ChainClearDefault file matches the table's data

# TODO: Add SAMUK at a later date. This was not included in the test case
#  @ui_test
#  Scenario: fc3342 - (ChainClear) Filter orders by 'SAMUK' in ChainUI 
#    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 10/02/2020 00:00
#    And User sets the 'to' date to 16/02/2020 23:59
#    And User clicks the search icon
#    And User filters by 'SAMUK' fund manager
#    And User sets the format to ChainClearDefault and click download
#    Then the trade data will display in the table
#    And only 'SAMUK' managed orders are displayed
#    And the ChainClearDefault file matches the table's data

