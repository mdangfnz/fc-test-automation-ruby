Feature: Chain UI allows filter on orders by status


  @ui_test
  Scenario: fc2411 - User filters orders by 'Accepted' status in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 27/05/2020 00:00
#    And User sets the 'to' date to 27/05/2020 23:59
#    And User clicks the search icon
    When User filters by 'Accepted' status
    Then the trade data will display in the table
    And only 'Accepted' status orders are displayed


  @ui_test
  Scenario: fc2411 - User filters orders by 'Rejected' status in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 27/05/2020 00:00
#    And User sets the 'to' date to 27/05/2020 23:59
#    And User clicks the search icon
    When User filters by 'Rejected' status
    Then the trade data will display in the table
    And only 'Rejected' status orders are displayed

  @ui_test
  Scenario: fc2411 - User filters orders by 'Received' status in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 27/05/2020 00:00
#    And User sets the 'to' date to 27/05/2020 23:59
#    And User clicks the search icon
    When User filters by 'Received' status
    Then the trade data will display in the table
    And only 'Received' status orders are displayed


  @ui_test
  Scenario: fc2411 - User filters orders by 'Priced' status in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 27/05/2020 00:00
#    And User sets the 'to' date to 27/05/2020 23:59
#    And User clicks the search icon
    When User filters by 'Priced' status
    Then the trade data will display in the table
    And only 'Priced' status orders are displayed

#  @ui_test TODO: Revisit later, determined unnecessary April 9 2020
#  Scenario: fc2411-User filters orders by 'Investigation' status in Chain UI
#    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 15/03/2020 00:00
#    And User sets the 'to' date to 18/03/2020 23:59
#    And User filters by 'Investigation' status
#    Then the trade data will display in the table
#    And only 'Investigation' orders are displayed

  @ui_test
  Scenario: fc2411 - User filters orders by 'All' status in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 27/05/2020 00:00
#    And User sets the 'to' date to 27/05/2020 23:59
#    And User clicks the search icon
    When User filters by 'Select All' status
    Then the trade data will display in the table

  @ui_test
  Scenario: fc2411 - User filters orders by 'Terminated' status in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 27/05/2020 00:00
#    And User sets the 'to' date to 27/05/2020 23:59
#    And User clicks the search icon
    When User filters by 'Terminated' status
    Then the trade data will display in the table
    And only 'Terminated' status orders are displayed
