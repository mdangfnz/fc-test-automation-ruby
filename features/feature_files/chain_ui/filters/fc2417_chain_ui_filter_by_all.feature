Feature: Filter by all available status types

  @ui_test
  Scenario: fc2417 - Chain UI filter by all filter types
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 21/04/2020 00:00
#    And User sets the 'to' date to 21/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Emx' channel
    And User filters by 'Priced' status
    And User filters by 'EQTFT' fund manager
    And User filters by 'Buy' trade type
    And User filters by 'Platform' processing system
    And User filters by 'GB00B4RL9738' isin
    Then the trade data will display in the table
    And only 'Buy' type orders are displayed
    And only 'Platform' processing system orders are displayed
    And only 'Emx' channel orders are displayed
    And only 'GB00B4RL9738' ISIN orders are displayed
    And only 'Priced' status orders are displayed
    And only 'EQTFT' managed orders are displayed