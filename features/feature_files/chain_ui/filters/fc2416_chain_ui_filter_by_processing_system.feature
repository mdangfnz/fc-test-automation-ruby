Feature: Chain UI allows filter on orders by processing system

  @ui_test
  Scenario: fc2416 - User filters orders by 'Platform' system in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Platform' processing system
    Then the trade data will display in the table
    And only 'Platform' processing system orders are displayed

  @ui_test
  Scenario: fc2416 - User filters orders by 'Chain' system in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Chain' processing system
    Then the trade data will display in the table
    And only 'Chain' processing system orders are displayed



