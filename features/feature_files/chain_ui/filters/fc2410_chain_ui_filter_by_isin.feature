Feature: Chain UI allows filter on orders by ISIN

  @ui_test
  Scenario: fc2410 - User filters orders by ISIN in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'GB00B2NLM962' isin
    Then the trade data will display in the table
    And only 'GB00B2NLM962' ISIN orders are displayed


