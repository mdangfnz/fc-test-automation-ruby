Feature: Chain UI allows filter on orders by trade type


  @ui_test
  Scenario: fc2414 - User filters orders by 'Buy' type in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Buy' trade type
    Then the trade data will display in the table
    And only 'Buy' type orders are displayed


  @ui_test
  Scenario: fc2414 - User filters orders by 'Sell' type in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Sell' trade type
    Then the trade data will display in the table
    And only 'Sell' type orders are displayed

