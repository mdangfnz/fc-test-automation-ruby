Feature: Chain UI user can search for trades within a 7 day range


  @ui_test
  Scenario: fc2627-Chain user filters on a date range within 7 days
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 15/03/2020 00:00
    And User sets the 'to' date to 21/03/2020 23:59
    And the dates are within a 7 day range
    And User clicks the search icon
    Then the trade data will display in the table
    And the trade data displayed is within the date range


##Negative Scenario
#  @ui_test
#  Scenario: Chain user filters on a date range greater than 7 days
#    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 01/03/2020 00:00
#    And User sets the 'to' date to 18/03/2020 23:59
#    Then date search will show an error