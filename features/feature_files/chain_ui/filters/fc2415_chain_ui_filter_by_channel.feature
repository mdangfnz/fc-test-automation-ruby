Feature: Chain UI allows filter on orders by trade type


  @ui_test
  Scenario: fc2415 - User filters orders by 'Fnz clear' channel in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Fnz clear' channel
    Then the trade data will display in the table
    And only 'Fnz clear' channel orders are displayed

  @ui_test
  Scenario: fc2415 - User filters orders by 'Emx' channel in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Emx' channel
    Then the trade data will display in the table
    And only 'Emx' channel orders are displayed

  @ui_test
  Scenario: fc2415 - User filters orders by 'Fnz one opbank' channel in Chain UI
    Given User logged into FNZ Chain UI
#    When User sets the 'from' date to 20/04/2020 00:00
#    And User sets the 'to' date to 22/04/2020 23:59
#    And User clicks the search icon
    And User filters by 'Fnz one opbank' channel
    Then the trade data will display in the table
    And only 'Fnz one opbank' channel orders are displayed

