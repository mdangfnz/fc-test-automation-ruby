Feature: Chain UI provides a search filter for order reference

  @ui_test
  Scenario: fc2637 - Chain UI search with order reference
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 20/04/2020 00:00
    And User sets the 'to' date to 22/04/2020 23:59
    And User clicks the search icon
    And User searches by order reference 'ATEST_UKXBL36'
    Then the trade data will display in the table
    And only 'ATEST_UKXBL36' order reference is displayed