Feature: Chain UI provides login functionality

  @ui_test
  Scenario: fc2770 - Chain UI password rules - requires 1 uppercase
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User clicks on change password
    And User puts new password without an uppercase
    Then New password will display an error

  @ui_test
  Scenario: fc2770 - Chain UI password rules - requires 2 numbers
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User clicks on change password
    And User puts new password without 2 numbers
    Then New password will display an error

  @ui_test
  Scenario: fc2770 - Chain UI password rules - no special characters
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User clicks on change password
    And User puts new password with a special character
    Then New password will display an error

  @ui_test
  Scenario: fc2770 - Chain UI password rules - 8 characters minimum
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User clicks on change password
    And User puts new password less than 8 characters
    Then New password will display an error

  @ui_test
  Scenario: fc2770 - Chain UI password rules - no previous passwords
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User clicks on change password
    And User puts previously used password
    Then New password will display an error