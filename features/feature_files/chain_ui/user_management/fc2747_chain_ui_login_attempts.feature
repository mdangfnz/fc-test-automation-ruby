Feature: Chain UI requires username and password

  @ui_test
  Scenario: fc2747 - Login without username
    When I goto Chain UI login page
    And I put my password
    And I click login
    Then I see 'fill out this field'

  @ui_test
  Scenario: fc2747 - Login without password
    When I goto Chain UI login page
    And I put my username
    And I click login
    Then I see 'fill out this field'
