Feature: Chain UI provides login functionality

  @ui_test
  Scenario: fc2760 - Chain UI change password
    Given User logged into FNZ Chain UI to change pwd
    When User can see Chain UI
    And User clicks on change password
    And User puts valid new password
    Then User can see Chain UI