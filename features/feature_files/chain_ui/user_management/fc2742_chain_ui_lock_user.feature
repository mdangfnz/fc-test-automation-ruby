Feature: Chain will lock user after 3 attempts

  @ui_test
  Scenario: fc2742 - Chain UI lock user after 3 attempts
    Given I goto Chain UI login page
    When I fail login 3 times
    And I put my username
    And I put my password
    And I click login
    Then my user will be locked