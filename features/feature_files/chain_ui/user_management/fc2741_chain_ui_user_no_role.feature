Feature: Chain will lock user after 3 attempts

  @ui_test
  Scenario: fc2741 - Chain UI user logs in with no role
    Given I goto Chain UI login page
    When I login with a user with no role
    Then Chain UI will display missing role error