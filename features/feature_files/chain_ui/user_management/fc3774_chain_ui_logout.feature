Feature: Chain UI provides user to logout and clear session

  @ui_test
  Scenario: fc3774 - Logout of Chain UI
    Given User logged into FNZ Chain UI
    And User can see Chain UI
    When User logs out of Chain UI
    Then Chain UI redirected to login