Feature: Chain UI will automatically refresh with new data

  @ui_test @api_test
  Scenario: fc2643 - Chain UI will auto update displays
    Given User logged into FNZ Chain UI
    And User gets the trade count displayed
    And I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And I wait '30' seconds
    Then the trade count has been updated