Feature: Chain UI provides a donut chart to display orders by status

  @ui_test
  Scenario: fc3280 - Chain UI donut chart displays total orders
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    Then the donut chart will display total orders