Feature: Chain UI login screen has display template

  @ui_test
  Scenario: fc3331 - Chain UI login screen components with Chrome
    When I goto Chain UI login page
    Then login screen has necessary components

  @ui_test_firefox
  Scenario: fc3331 - Chain UI login screen components with Firefox
    When I goto Chain UI login page
    Then login screen has necessary components

  @ui_test_safari
  Scenario: fc3331 - Chain UI login screen components with Safari
    When I goto Chain UI login page
    Then login screen has necessary components
