Feature: FNZ Chain API provides end point to return all errors

  @ui_test @api_test
  Scenario: fc2407 - Chain UI display error
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption via Chain API with an invalid fund manager
    Then my redemption receives a 202 success
    And my trade will have an error via Chain API
    And the error will be displayed in Chain UI
    And the payload will be displayed in Chain UI