Feature: Chain UI status tab updates after filter

  @ui_test
  Scenario: fc3293 - Chain UI status tab updates after filter by channel
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Emx' channel
    Then the status tabs are updated

  @ui_test
  Scenario: fc3293 - Chain UI status tab updates after filter by fund manager
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'EQTFT' fund manager
    Then the status tabs are updated

  @ui_test
  Scenario: fc3293 - Chain UI status tab updates after filter by Status
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Accepted' status
    Then the status tabs are updated

  @ui_test
  Scenario: fc3293 - Chain UI status tab updates after filter by ISIN
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'GB00B4RL9738' isin
    Then the status tabs are updated

  @ui_test
  Scenario: fc3293 - Chain UI status tab updates after filter by trade type
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Buy' trade type
    Then the status tabs are updated

  @ui_test
  Scenario: fc3293 - Chain UI status tab updates after filter by processing system
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Platform' processing system
    Then the status tabs are updated