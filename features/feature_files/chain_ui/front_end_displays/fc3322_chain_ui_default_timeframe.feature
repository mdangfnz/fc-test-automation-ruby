Feature: Chain UI defaults order timeframe 3 days back

  @ui_test
  Scenario: fc3322 - Chain UI defaults timeframe to 3 days
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    Then the default date range is 3 days back