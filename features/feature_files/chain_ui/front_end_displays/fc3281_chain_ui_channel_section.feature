Feature: Chain UI provides a donut chart to display orders by status

  @ui_test
  Scenario: fc3281 - Chain UI channel section static after filter by channel
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And I capture channel section
    And User filters by 'Emx' channel
    Then the channel section is not updated

  @ui_test
  Scenario: fc3281 - Chain UI channel section static after filter by fund manager
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'EQTFT' fund manager
    Then the channel section is not updated

  @ui_test
  Scenario: fc3281 - Chain UI channel section static after filter by Status
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Accepted' status
    Then the channel section is not updated

  @ui_test
  Scenario: fc3281 - Chain UI channel section static after filter by ISIN
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'GB00B4RL9738' isin
    Then the channel section is not updated

  @ui_test
  Scenario: fc3281 - Chain UI channel section static after filter by trade type
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Buy' trade type
    Then the channel section is not updated

  @ui_test
  Scenario: fc3281 - Chain UI channel section static after filter by processing system
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User filters by 'Platform' channel
    Then the channel section is not updated