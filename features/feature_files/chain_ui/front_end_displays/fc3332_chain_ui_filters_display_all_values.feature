Feature: Chain UI's drop down filters will show values based on currently available trades displayed

  @ui_test
  Scenario: fc3332 - Chain UI display all values for each dropdown filter
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And the trade data will display in the table
    Then the status filter will align with the table data
    And the channel filter will align with the table data
    And the processing system filter will align with the table data
    And the trade type filter will align with the table data
    And the fund manager filter will align with the table data
    And the isin filter will align with the table data