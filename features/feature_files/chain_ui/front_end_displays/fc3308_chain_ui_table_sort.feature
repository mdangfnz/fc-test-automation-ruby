Feature: Chain UI provides a donut chart to display orders by status

  @ui_test
  Scenario: fc3308 - Chain UI allows sort on received time
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    And User sorts by received time

  @ui_test
  Scenario: fc3308 - Chain UI allows sort on ISIN
    Given User logged into FNZ Chain UI
    When User can see Chain UI
    Then User sorts by ISIN