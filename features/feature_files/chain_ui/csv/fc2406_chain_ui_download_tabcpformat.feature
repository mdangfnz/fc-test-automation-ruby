Feature: Chain UI user can download TaBcpFormat csv in any browser


  @ui_test
  Scenario: fc2405-Chain user can download TaBcpFormat csv with Chrome
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 11/05/2020 00:00
    And User sets the 'to' date to 12/05/2020 23:59
    And User clicks the search icon
    And User sets the format to TaBcpFormat and click download
    Then the trade data will display in the table
    And the TaBcpFormat file matches the table's data

  @ui_test_firefox
  Scenario: fc2406-Chain user can download TaBcpFormat csv with Firefox
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 11/05/2020 00:00
    And User sets the 'to' date to 12/05/2020 23:59
    And User clicks the search icon
    And User sets the format to TaBcpFormat and click download
    Then the trade data will display in the table
    And the TaBcpFormat file matches the table's data

  @ui_test_safari
  Scenario: fc2406-Chain user can download TaBcpFormat csv with Safari
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 11/05/2020 00:00
    And User sets the 'to' date to 12/05/2020 23:59
    And User clicks the search icon
    And User sets the format to TaBcpFormat and click download
    Then the trade data will display in the table
    And the TaBcpFormat file matches the table's data
