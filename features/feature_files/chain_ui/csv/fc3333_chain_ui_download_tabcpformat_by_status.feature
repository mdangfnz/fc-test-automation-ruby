Feature: Chain UI user can filter by status and download TA BCP Format CSV

  @ui_test
  Scenario: fc3333 - User can filter by 'Terminated' orders and download TA BCP Format CSV
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 26/05/2020 00:00
    And User sets the 'to' date to 26/05/2020 23:59
    And User clicks the search icon
    And User filters by 'Terminated' status
    And User sets the format to TaBcpFormat and click download
    Then the trade data will display in the table
    And only 'Terminated' status orders are displayed
    And the 'TABCPFORMAT' file count matches Chain UI