Feature: Users can see Rejected orders in Chain Clear CSV with "Reasons"

  @ui_test
  Scenario: fc2620-ChainClear CSV shows rejected reasons
    Given User logged into FNZ Chain UI
    When User sets the 'from' date to 24/05/2020 00:00
    And User sets the 'to' date to 26/05/2020 23:59
    And User clicks the search icon
    And User filters by 'Rejected' status
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And the ChainClear file will have rejected reasons