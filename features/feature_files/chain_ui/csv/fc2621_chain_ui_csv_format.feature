Feature: Users can see Rejected orders in Chain Clear CSV with "Reasons"

  @ui_test
  Scenario: fc2621-ChainClear CSV format
    Given User logged into FNZ Chain UI
    When User sets the format to ChainClearDefault and click download

  @ui_test
  Scenario: fc2621-TABCP CSV format
    Given User logged into FNZ Chain UI
    When User sets the format to TABCPFORMAT and click download