Feature: This is EMX E2E scenario 3. Place subscription and redemption before TA valuation

  @ui_test @emx_e2e_3
  Scenario: EMX E2E - 3.1 Upload trades to EMX
    Given User logged into EMX
    When User uploads an 'OINP' file
    And User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User clicks the emx search button
    Then EMX orders displayed is '4'

  @api_test @emx_e2e_3
  Scenario: EMX E2E - 3.0.1 - Wait for trade to be accepted
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I get emx order from file with index '4'
    And I check order ref shows 'statusReportComplete'
    Then I can move to the next scenario


  @ui_test @emx_e2e_3
  Scenario: EMX E2E - 3.2 Validate trades are accepted in Chain UI
    Given User logged into FNZ Chain UI
    When User filters by 'Emx' channel
    And User filters by 'Accepted' status
    And User searches for emx orders uploaded by file
    And User sets records displayed to '100'
    Then the trade data will display in the table
    And orders displayed is '3'


  @ui_test @emx_e2e_3
  Scenario: EMX E2E - 3.3 Validate trades are accepted in EMX
    Given User logged into EMX
    When User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User filters emx on 'Booked' status
    And User clicks the emx search button
    Then EMX orders displayed is '3'