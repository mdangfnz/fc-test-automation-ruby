Feature: This is EMX E2E scenario 2. Place subscription and redemption After EMX cutoff (11:15AM GMT)


@ui_test @emx_e2e_2
Scenario: EMX E2E - 2.4 Validate trades are priced in Chain UI
Given User logged into FNZ Chain UI
When User filters by 'Emx' channel
And User filters by 'Priced' status
And User searches for emx orders uploaded by file
Then the trade data will display in the table
And orders displayed is '3'


@ui_test @emx_e2e_2
Scenario: EMX E2E - 2.5 Validate trades are priced in EMX
Given User logged into EMX
When User clicks on Mailbox orders
And User filters by orders from uploaded file
And User filters emx on 'Complete' status
And User clicks the emx search button
Then EMX orders displayed is '3'