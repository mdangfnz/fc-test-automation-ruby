Feature: This is EMX E2E scenario 1. Place subscription and redemption before TA cutoff (11:15AM GMT)

  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.1 Upload trades to EMX
    Given User logged into EMX
    When User uploads an 'OINP' file
    And User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User clicks the emx search button
    Then EMX orders displayed is '100'

  @api_test @emx_e2e_1
  Scenario: EMX E2E - 1.0.1 - Wait for trade to be accepted
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I get emx order from file with index '100'
    And I check order ref shows 'statusReportComplete'
    Then I can move to the next scenario


  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.2 Validate trades are accepted in Chain UI
    Given User logged into FNZ Chain UI
    When User filters by 'Emx' channel
    And User filters by 'Accepted' status
    And User searches for emx orders uploaded by file
    And User sets records displayed to '100'
    Then the trade data will display in the table
    And orders displayed is '90'


  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.3 Validate trades are accepted in EMX
    Given User logged into EMX
    When User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User filters emx on 'Booked' status
    And User clicks the emx search button
    Then EMX orders displayed is '90'

  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.4 - Validate trades are rejected in Chain UI
    Given User logged into FNZ Chain UI
    When User filters by 'Emx' channel
    And User filters by 'Rejected' status
    And User searches for emx orders uploaded by file
    And User sets the format to ChainClearDefault and click download
    Then the trade data will display in the table
    And orders displayed is '10'
    And the ChainClear file will have rejected reasons for emx orders

  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.5 - Validate trades are rejected in EMX
    Given User logged into EMX
    When User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User filters emx on 'MERRd (all)' status
    And User clicks the emx search button
    Then EMX orders displayed is '10'
    And EMX rejected reasons are all valid