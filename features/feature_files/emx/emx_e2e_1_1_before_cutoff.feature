Feature: This is EMX E2E scenario 1. Place subscription and redemption before TA cutoff (11:15AM GMT)


  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.6 Validate trades are priced in Chain UI
    Given User logged into FNZ Chain UI
    When User filters by 'Emx' channel
    And User filters by 'Priced' status
    And User searches for emx orders uploaded by file
    And User sets records displayed to '100'
    Then the trade data will display in the table
    And orders displayed is '90'


  @ui_test @emx_e2e_1
  Scenario: EMX E2E - 1.7 Validate trades are priced in EMX
    Given User logged into EMX
    When User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User filters emx on 'Complete' status
    And User clicks the emx search button
    Then EMX orders displayed is '90'

