Feature: This is EMX E2E scenario 0. Use FNZ TA Api to test the full e2e flow, (Received -> Confirmed by Dist)

  @ui_test @emx_e2e_0
  Scenario: EMX E2E - 0.1 Upload trades to EMX
    Given User logged into EMX
    When User uploads an 'OINP' file
    And User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User clicks the emx search button
    Then EMX orders displayed is '100'

  @api_test @emx_e2e_0
  Scenario: EMX E2E - 0.2 Accept trades via FNZ TA endpoint
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    And I wait '180' seconds
    When I get EMX uploaded orders via the transactions endpoint
    And I accept emx file orders via FNZ TA endpoint
    #Then my orders will show 'Accepted' from the FNZ API


  @ui_test @emx_e2e_0
  Scenario: EMX E2E - 0.3 Validate trades are accepted in Chain UI
    Given User logged into FNZ Chain UI
    When User filters by 'Emx' channel
    And User filters by 'Accepted' status
    And User searches for emx orders uploaded by file
    And User sets records displayed to '100'
    Then the trade data will display in the table
    And orders displayed is '100'


  @ui_test @emx_e2e_0
  Scenario: EMX E2E - 0.4 Validate trades are accepted in EMX
    Given User logged into EMX
    When User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User filters emx on 'Booked' status
    And User clicks the emx search button
    Then EMX orders displayed is '100'


  @api_test @emx_e2e_0
  Scenario: EMX E2E - 0.5 Price trades via FNZ TA endpoint
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    And I wait '180' seconds
    When I get accepted EMX file orders via the transactions endpoint
    And I price emx file orders via FNZ TA endpoint
    #Then my orders will show 'Confirmed'


  @ui_test @emx_e2e_0
  Scenario: EMX E2E - 0.6 Validate trades are priced in Chain UI
    Given User logged into FNZ Chain UI
    When User filters by 'Emx' channel
    And User filters by 'Priced' status
    And User searches for emx orders uploaded by file
    And User sets records displayed to '100'
    Then the trade data will display in the table
    And orders displayed is '100'


  @ui_test @emx_e2e_0
  Scenario: EMX E2E - 0.7 Validate trades are priced in EMX
    Given User logged into EMX
    When User clicks on Mailbox orders
    And User filters by orders from uploaded file
    And User filters emx on 'Complete' status
    And User clicks the emx search button
    Then EMX orders displayed is '100'


