Feature: Chain Admin will see all trade sources from GET calls

  @api_test
  Scenario: fc3513 - Chain Admin views all sources
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I send a post request to Chain API filter service from '2020-04-20 00:00:00' to '2020-04-22 23:59:00'
    And I get one transaction from each source
    Then admin user can get each chain