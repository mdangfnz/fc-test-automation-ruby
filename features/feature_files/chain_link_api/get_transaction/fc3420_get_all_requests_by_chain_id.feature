Feature: Chain API provides a feature to get all requests related to a chain ID

  @api_test
  Scenario: fc3420 - Get all requests by Chain ID
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I accept orders via FNZ TA endpoint
    And I get requests via Chain API by chain ID
    Then I will see requests details via Chain API by chain ID

