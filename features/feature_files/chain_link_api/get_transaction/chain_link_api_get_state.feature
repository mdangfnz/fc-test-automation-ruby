Feature: Chain Link API provides an end point to returns order on state

  @api_test
  Scenario: fc2205-Chain API-Check state with parameter "TRANSACTION_RECEIVED_BY_CHAIN"
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I send a get request to Chain API state service with 'TRANSACTION_RECEIVED_BY_CHAIN'
    Then then all orders show status 'TRANSACTION_RECEIVED_BY_CHAIN'

  @api_test
  Scenario: fc2214-Chain API-Check state with parameter "ACCEPTED_STATUS_REPORT_RECEIVED_BY_CHAIN"
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I send a get request to Chain API state service with 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_CHAIN'
    Then then all orders show status 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_CHAIN'

  @api_test
  Scenario: fc2215-Chain API-Check state with parameter "ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR"
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I send a get request to Chain API state service with 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR'
    Then then all orders show status 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR'

  @api_test
  Scenario: fc2216-Chain API-Check state with parameter "REJECTED_STATUS_REPORT_RECEIVED_BY_CHAIN"
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I send a get request to Chain API state service with 'REJECTED_STATUS_REPORT_RECEIVED_BY_CHAIN'
    Then then all orders show status 'REJECTED_STATUS_REPORT_RECEIVED_BY_CHAIN'

  @api_test
  Scenario: fc2217-Chain API-Check state with parameter "TRANSACTION_CONFIRMATION_RECEIVED_BY_CHAIN"
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I send a get request to Chain API state service with 'TRANSACTION_CONFIRMATION_RECEIVED_BY_CHAIN'
    Then then all orders show status 'TRANSACTION_CONFIRMATION_RECEIVED_BY_CHAIN'