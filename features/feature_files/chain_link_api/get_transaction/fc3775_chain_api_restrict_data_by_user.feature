Feature: Chain API restricts access to data based on USER/ROLE

  @api_test
  Scenario: fc3775 - Chain API - FNZ Clear data access
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I get accepted orders via get transactions endpoint
    Then all orders are sourced from 'FNZ_CLEAR'

  @api_test
  Scenario: fc3775 - Chain API - FNZ ONE OPBANK data access
    Given I am using Chain API as FNZ ONE OPBANK
    And I have a Chain API client token
    When I get confirmed orders via get transactions endpoint
    Then all orders are sourced from 'FNZ_ONE_OPBANK'

  @api_test
  Scenario: fc3775 - Chain API - TA Finland data access
    Given I am using Chain API as REGISTER FINLAND
    And I have a Chain API client token
    When I get confirmed orders via get transactions endpoint
    Then all orders are sourced from 'FNZ_ONE_OPBANK'

  @api_test
  Scenario: fc3775 - Chain API - TA data access
    Given I am using Chain API as FNZ REGISTER
    And I have a Chain API token
    When I get confirmed orders via get transactions endpoint
    Then no orders are sourced from 'FNZ_ONE_OPBANK'