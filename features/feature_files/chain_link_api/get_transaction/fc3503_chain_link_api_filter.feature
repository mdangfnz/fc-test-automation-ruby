Feature: Chain API filter service returns block chains within a given date range

  @api_test
  Scenario: fc3503-Chain API filter returns data within date range
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I send a post request to Chain API filter service from '2020-04-15 00:00:00' to '2020-04-21 23:59:00'
    Then Chain API filter response only returns blockchains within date range

