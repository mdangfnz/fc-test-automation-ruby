Feature: Chain API should return a 401 if an invalid TOKEN is used

  @api_test
  Scenario: fc3778 - Chain API - use empty token
    Given I am using Chain API as mdang
    And I don't have a Chain API token
    When I get accepted orders via get transactions endpoint
    Then my get state request returns a 401 response


  @api_test
  Scenario: fc3778 - Chain API - use invalid token
    Given I am using Chain API as mdang
    And I have an invalid Chain API token
    When I get accepted orders via get transactions endpoint
    Then my get state request returns a 401 response
