Feature: Chain API can regenerate cert

  @api_test
  Scenario: fc2763 - Regenerate Cert
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I regenerate the cert
    Then the cert has been successfully regenerated
