Feature: Chain API's token endpoint will always generate a new token

  @api_test
  Scenario: fc2641 - Token endpoint generates new token
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I wait '5' seconds
    And I generate a new token
    Then the token is new
