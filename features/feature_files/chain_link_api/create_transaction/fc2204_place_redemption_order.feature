Feature: Placing a redemption order via Chain API will reflect in Chain UI
  
  @ui_test @api_test
  Scenario: fc2204 - Place a redemption order with units via Chain API
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption with units via Chain API
    And my redemption receives a 202 success
    And I check my trade has been received
    Then the trade will appear 'Received' in Chain UI as count '1' today

  @ui_test @api_test
  Scenario: fc2204 - Place a redemption order with amt via Chain API
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption with amt via Chain API
    And my redemption receives a 202 success
    And I check my trade has been received
    Then the trade will appear 'Received' in Chain UI as count '1' today