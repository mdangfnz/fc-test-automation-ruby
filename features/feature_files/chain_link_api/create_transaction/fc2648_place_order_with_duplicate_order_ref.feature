Feature: FNZ Chain and FNZ TA should accept orders with duplicate order reference

  @ui_test @api_test
  Scenario: fc2648 - Place subscription with duplicate order reference
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with a duplicate order ref
    And I check my trade has been received
    And I wait '10' seconds
    Then the trade will appear 'Received' in Chain UI as count '2' today

  @ui_test @api_test
  Scenario: fc2648 - Place redemption with duplicate order reference
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption via Chain API with a duplicate order ref
    And I check my trade has been received
    And I wait '10' seconds
    Then the trade will appear 'Received' in Chain UI as count '2' today
