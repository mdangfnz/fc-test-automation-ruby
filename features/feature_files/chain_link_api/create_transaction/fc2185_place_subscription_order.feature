Feature: Chain API can place a subscription

  @ui_test @api_test
  Scenario: fc2185 - Place Subscription Order with units
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription with units via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    Then the trade will appear 'Received' in Chain UI as count '1' today

  @ui_test @api_test
  Scenario: fc2185 - Place Subscription Order with amt
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription with amt via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    Then the trade will appear 'Received' in Chain UI as count '1' today