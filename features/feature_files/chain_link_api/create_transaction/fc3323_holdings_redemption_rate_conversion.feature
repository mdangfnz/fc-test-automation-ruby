Feature: Chain API provides input for holdings redemption rate conversion

  @ui_test @api_test
  Scenario: fc3323 - Holdings redemption rate conversion
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption via Chain API with holding redemption rate
    And my redemption receives a 202 success
    And I check my trade has been received
    Then the trade will appear 'Received' in Chain UI as count '1'
    And User sets the format to ChainClearDefault and click download
    And User can validate holdings redemption in CSV