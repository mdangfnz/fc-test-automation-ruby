Feature: FNZ TA confirms an order, then the trade will appear PRICED in Chain UI and CONFIRMED in PLATFORM

  @ui_test @api_test
  Scenario: fc2196 - Price order via FNZ TA endpoint
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    #And I wait for my transaction to be 'Accepted'
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I wait '10' seconds
    And I price orders via FNZ TA endpoint
    And I wait '30' seconds
    Then the trade will appear 'Priced' in Chain UI as count '1' today
