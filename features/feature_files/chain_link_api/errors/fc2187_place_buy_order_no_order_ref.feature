Feature: Chain API buy order must have an order reference

  @api_test
  Scenario: fc2187 - Place buy order with empty order ref
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with empty order ref
    Then my subscription receives a 400 error
