Feature: Chain API buy order must have an account id

  @api_test
  Scenario: fc2191 - Place buy order with empty account id
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with empty account id
    Then my subscription receives a 400 error
