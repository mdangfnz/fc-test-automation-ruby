Feature: FNZ Chain API provides end point to return all errors

  @ui_test @api_test
  Scenario: fc3398 - Get all errors
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with 0 units
    And I wait '30' seconds
    And I get all error count via Chain API
    Then the total errors is correctly displayed in Chain UI