Feature: Chain API sell order must have a valid fund manager

  @ui_test @api_test
  Scenario: fc2189 - Place sell order with invalid fund manager
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption via Chain API with an invalid fund manager
    And my redemption receives a 202 success
    And my trade will have an error via Chain API
    And the error will be displayed in Chain UI