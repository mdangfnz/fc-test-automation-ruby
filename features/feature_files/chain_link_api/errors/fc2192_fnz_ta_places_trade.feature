Feature: When FNZ TA places a trade, an error should be created as their role should be restricted

  @ui_test @api_test
  Scenario: fc2192-FNZ TA places a trade
    Given I am using Chain API as FNZ Register
    And I have a Chain API token
    When I place a redemption via Chain API
    And my redemption receives a 202 success
    Then my trade will have an error via Chain API
    And the error will be displayed in Chain UI