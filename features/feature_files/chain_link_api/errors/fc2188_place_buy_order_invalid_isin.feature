Feature: Chain API buy order must have a valid ISIN

  @api_test
  Scenario: fc2188 - Place buy order with empty ISIN
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with empty ISIN
    Then my subscription receives a 400 error

  @api_test
  Scenario: fc2188 - Place buy order with invalid ISIN element
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with an invalid ISIN
    Then my subscription receives a 400 error