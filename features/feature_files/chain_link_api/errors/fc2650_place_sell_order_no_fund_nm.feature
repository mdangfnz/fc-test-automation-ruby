Feature: Chain API sell order must have a fund manager

  @api_test
  Scenario: fc2650 - Place sell order with empty fund manager
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a redemption via Chain API with empty fund manager
    Then my redemption receives a 400 error