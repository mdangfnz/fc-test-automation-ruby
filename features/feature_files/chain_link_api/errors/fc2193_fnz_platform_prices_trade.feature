Feature: Fnz Platform should not be able to confirm trades

  @ui_test @api_test
  Scenario: fc2193 - FNZ Platform prices trade
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I get one accepted Clear order via the transactions endpoint
    And I price orders via FNZ TA endpoint
    Then my trade will have an error via Chain API
    And the error will be displayed in Chain UI