Feature: FNZ TA's confirmation message have required fields

  @api_test
  Scenario: fc2654 - FNZ TA sends confirmation without FUND ID
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without Fund ID
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2655 - FNZ TA sends confirmation without deal ref
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without deal reference
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2656 - FNZ TA sends confirmation without ACCT ID
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without ACCT ID
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2657 - FNZ TA sends confirmation without net amt
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without net amt
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2658 - FNZ TA sends confirmation without sttlm amt
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without sttlm amt
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2659 - FNZ TA sends confirmation without price amt
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without price amt
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2660 - FNZ TA sends confirmation without grss amt
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without grss amt
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2661 - FNZ TA sends confirmation without units
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without units
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2662 - FNZ TA sends confirmation without trade date
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without trade date
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2664 - FNZ TA sends confirmation without settlement date
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without cash sttlm dt
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2665 - FNZ TA sends confirmation without order type
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without order type
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2666 - FNZ TA sends confirmation without grss amt ccy
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without grss amt ccy
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2666 - FNZ TA sends confirmation without net amt ccy
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without net amt ccy
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2666 - FNZ TA sends confirmation without price amt ccy
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without price amt ccy
    Then my confirmation receives a 400 error

  @api_test
  Scenario: fc2666 - FNZ TA sends confirmation without sttlm amt ccy
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    And I am using Chain API as FNZ Register
    And I have a Chain API token
    And I accept orders via FNZ TA endpoint
    And I price orders via FNZ TA endpoint without sttlm amt ccy
    Then my confirmation receives a 400 error