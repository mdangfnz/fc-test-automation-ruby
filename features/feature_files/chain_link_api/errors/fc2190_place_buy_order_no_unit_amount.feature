Feature: Chain API buy order must have unit or amount

  @ui_test @api_test
  Scenario: fc2190 - Place buy order with 0 units
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with 0 units
    Then my subscription receives a 202 success
    And my trade will have an error via Chain API
    And the error will be displayed in Chain UI

#  TODO: defect fc-3713 created
  @ui_test @api_test
  Scenario: fc2190 - Place buy order with 0 amount
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with 0 amount
    Then my subscription receives a 202 success
    And my trade will have an error via Chain API
    And the error will be displayed in Chain UI

  @api_test
  Scenario: fc2190 - Place buy order with empty amount
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with empty amount
    Then my subscription receives a 400 error

  @api_test
  Scenario: fc2190 - Place buy order with empty units
    Given I am using Chain API as FNZ Clear
    And I have a Chain API token
    When I place a subscription via Chain API with empty units
    Then my subscription receives a 400 error
