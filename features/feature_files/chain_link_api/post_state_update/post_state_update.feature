Feature: Chain API provides an endpoint to update the state of any trade directly

  @ui_test @api_test
  Scenario: Post State Update Received directly to Terminate
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I wait '30' seconds
    And I check my trade has been received
    And I post state update to 'TRANSACTION_TERMINATED' via Chain API
    Then the trade will appear 'TERMINATED' in Chain UI as count '1' today

  @ui_test @api_test
  Scenario: Post State Update Received directly to Accepted by Dist
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I wait '30' seconds
    And I check my trade has been received
    And I post state update to 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR' via Chain API
    And I wait '30' seconds
    Then my trade will have an error via Chain API
    And the error will be displayed in Chain UI

  @ui_test @api_test
  Scenario: Post State Update Received directly to Rejected by Dist
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I wait '30' seconds
    And I check my trade has been received
    And I post state update to 'REJECTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR' via Chain API
    And I wait '30' seconds
    Then my trade will have an error via Chain API
    And the error will be displayed in Chain UI

  @ui_test @api_test
  Scenario: Post State Update Received directly to Confirmed by Dist
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I wait '30' seconds
    And I check my trade has been received
    And I post state update to 'TRANSACTION_CONFIRMATION_RECEIVED_BY_DISTRIBUTOR' via Chain API
    And I wait '30' seconds
    Then my trade will have an error via Chain API
    And the error will be displayed in Chain UI

