Feature: Chain API provides an endpoint to retry messages using Trace ID.

  @ui_test @api_test
  Scenario: fc3339 - Retry error message returns original Chain ID
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API with 0 units
    And my subscription receives a 202 success
    And my trade will have an error via Chain API
    And I retry my trace ID
    Then the error will be displayed in Chain UI
    And my retry error response returns same chain ID
