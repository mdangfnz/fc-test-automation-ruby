Feature: Chain API provides an endpoint to resolve/remove an error from history

  @ui_test @api_test
  Scenario: fc3396 - Resolve error message
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I get all error IDs via Chain API
    And I update error state for all errors
    And I wait '30' seconds
    And I get all error count via Chain API
    Then Chain API returns error count 0
    And the total errors is correctly displayed in Chain UI
