Feature: Chain API provides endpoint to retry chain request

  @api_test
  Scenario: fc3437 - Retry request message
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I get requests via Chain API by chain ID
    And I will see requests details via Chain API by chain ID
    And I retry my request ID
    Then my retry request response returns 'processing'