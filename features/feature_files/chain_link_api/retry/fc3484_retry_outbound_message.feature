Feature: Chain API provides an endpoint to retry outbound messages

  @api_test
  Scenario: fc3484 - Retry Outbound Message
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I get one accepted EMX order via the transactions endpoint
    And I have a Chain API token
    And I get outbound status report request via Chain API by chain ID
    And I retry my request ID
    And my retry request response returns 'processing'
    Then I get ws gateway messages by chain ID
    And the ws gateway shows my retry message as RECONSUMED