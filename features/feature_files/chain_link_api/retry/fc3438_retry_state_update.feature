Feature: Chain API provides endpoint to retry state update

  @ui_test @api_test
  Scenario: fc3438 - Retry State Update
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I wait '10' seconds
    And I post state update to 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR' via Chain API
    And my trade will have an error via Chain API
    And I wait '30' seconds
    And the error will be displayed in Chain UI
    And I get state update 'ACCEPTED_STATUS_REPORT_RECEIVED_BY_DISTRIBUTOR' request via Chain API by chain ID
    And I retry my request ID
    Then my retry request response returns 'processing'
