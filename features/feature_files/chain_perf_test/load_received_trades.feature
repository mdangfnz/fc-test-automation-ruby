Feature: Chain API can place a subscription

  @ui_test @api_test
  Scenario: PERF TEST - load transactions
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I place a subscription via Chain API
    And my subscription receives a 202 success
    And I check my trade has been received
    #Then the trade will appear 'Received' in Chain UI as count '1' today


  @api_test @emx_e2e_0
  Scenario: PERF TEST - create status report
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I get all EMX perf received orders via the transactions endpoint
    And I accept emx perf orders via FNZ TA endpoint

  @api_test @emx_e2e_0
  Scenario: PERF TEST - 0.5 Price trades via FNZ TA endpoint
    Given I am using Chain API as mdang
    And I have a Chain API token
    When I get accepted EMX perf orders via the transactions endpoint
    And I price all emx perf orders via FNZ TA endpoint
