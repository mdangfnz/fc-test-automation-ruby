require 'rake'
require 'cucumber/rake/task'



Cucumber::Rake::Task.new(:features) do |t|
  t.cucumber_opts = "--format pretty "
    #cucumber features/feature_files/chrome_smoke_test.feature
    sh('cucumber features/feature_files/watir_smoke_test.feature')
end

Cucumber::Rake::Task.new(:chain_link_api_smoke_test) do |t|
  t.cucumber_opts = "--format pretty "
  #cucumber features/feature_files/chrome_smoke_test.feature
  sh('cucumber features/feature_files/chain_link_api_smoke_test.feature')
end
